//
//  Created by Backbase R&D B.V. on 09/11/20.
//

import XCTest
import RxSwift
@testable import IdentityAuthenticationJourney

final class LoadingViewTests: XCTestCase {
    func testLoadingBinding() {
        let view = LoadingView()

        XCTAssertFalse(view.loadingIndicator.isAnimating)

        view.rx.isAnimating.onNext(true)
        XCTAssert(view.loadingIndicator.isAnimating)

        view.rx.isAnimating.onNext(false)
        XCTAssertFalse(view.loadingIndicator.isAnimating)
    }
}
