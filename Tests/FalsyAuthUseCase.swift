//
//  Created by Backbase R&D B.V. on 03/04/2020.
//

import Foundation
import IdentityAuthenticationJourney

internal final class FalsyAuthUseCase: NSObject, AuthenticationUseCase {
    var lastSession: Session?

    func authenticate(user: User, preCompletion: (() -> Void)?, postCompletion: AuthenticationHandler?) {
        postCompletion?(.failure(.invalidCredentials))
    }

    func authenticateWithBiometrics(callback: AuthenticationHandler?) {
        callback?(.failure(.invalidCredentials))
    }

    func authenticateWithPasscode(callback: AuthenticationHandler?) {
        callback?(.failure(.invalidCredentials))
    }

    func forgotUsername(callback: ForgottenCredentialsHandler?) {
        callback?(.failure(.invalidCredentials))
    }

    func forgotPassword(callback: ForgottenCredentialsHandler?) {
        callback?(.failure(.invalidCredentials))
    }

    func changePasscode(callback: PasscodeChangeHandler?) {
        callback?(.failure(.invalidCredentials))
    }

    func completeRegistration(callback: AuthenticationHandler?) {
        callback?(.failure(.invalidCredentials))
    }

    func endSession(callback: SessionHandler?) {
        callback?(.none)
    }

    func logOut(callback: SessionHandler?) {
        callback?(.none)
    }

    func validateSession(callback: SessionHandler?) {
        callback?(.none)
    }

    func expireSession(callback: SessionHandler?) {
        callback?(.expired)
    }

    func lockAccount(callback: SessionHandler?) {
        callback?(.locked)
    }

    var cachedUsername: String? {
        return nil
    }

    var isEnrolled: Bool {
        return false
    }

    var isBiometricEnrolled: Bool {
        return false
    }

    var isPasscodeEnrolled: Bool {
        return false
    }
}
