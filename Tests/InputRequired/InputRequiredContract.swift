//
//  Created by Backbase R&D B.V. on 20/04/2020.
//

import Backbase
import BackbaseIdentity

internal final class InputRequiredContract: NSObject, NativeContract, BBIDInputRequiredAuthenticatorContract {
    var wasExplictlyDismissed: Bool = false
    var renderable: Renderable = FakeRenderable()

    var fields: [String: String]?
    func submit(_ fields: [String: String]) {
        self.fields = fields
    }

    var didCallCancel = false
    func cancel() {
        didCallCancel = true
    }

    var didCallFinish = false
    func finish() {
        didCallFinish = true
    }
}
