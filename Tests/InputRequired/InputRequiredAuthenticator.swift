//
//  Created by Backbase R&D B.V. on 20/04/2020.
//

import BackbaseIdentity

internal final class InputRequiredAuthenticator: BBIDInputRequiredAuthenticator {
    var didCallFinish = false
    override func finish() {
        didCallFinish = true
    }

    var didCallCancel = false
    override func cancel() {
        didCallCancel = true
    }

    var fields: [String: String]?
    override func submit(_ fields: [String: String]) {
        self.fields = fields
    }
}
