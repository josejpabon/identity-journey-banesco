//
//  Created by Backbase R&D B.V. on 20/04/2020.
//

import XCTest
import Backbase
@testable import IdentityAuthenticationJourney

internal final class InputRequiredTests: XCTestCase {
    func testAuthenticator() {
        let authenticator = InputRequired.authenticator
        XCTAssertFalse(authenticator.wasExplictlyDismissed)
    }
}
