//
//  Created by Backbase R&D B.V. on 20/04/2020.
//

import XCTest
import RxSwift
import RxTest
import Resolver
@testable import IdentityAuthenticationJourney

internal final class InputRequiredViewModelTests: XCTestCase {
    private var authenticator = InputRequiredAuthenticator()
    private lazy var viewModel = InputRequiredViewModel(fields: [.email], contract: authenticator)

    override func setUp() {
        Resolver.register { Authentication.Configuration() }

        authenticator = InputRequiredAuthenticator()
        viewModel = InputRequiredViewModel(fields: [.email], contract: authenticator)
    }

    func testInitialization() {
        let viewModel = InputRequiredViewModel(fields: [.email], contract: authenticator)
        XCTAssertNotNil(viewModel.contract)
    }

    func testBinding() {
        var config: Authentication.Configuration = Resolver.resolve()

        let disposeBag = DisposeBag()
        let scheduler = TestScheduler(initialClock: 0)

        let textField = UITextField()
        let output = viewModel.bind(
            values: [textField.rx.text],
            tap: .just(()),
            cancel: .just(()),
            submit: .just(())
        )

        let title = scheduler.createObserver(String.self)
        output.title.drive(title).disposed(by: disposeBag)
        XCTAssertEqual(title.events, [.next(0, config.inputRequired.strings.emailTitle()), .completed(0)])

        let subtitle = scheduler.createObserver(String.self)
        output.subtitle.drive(subtitle).disposed(by: disposeBag)
        XCTAssertEqual(subtitle.events, [.next(0, config.inputRequired.strings.emailSubtitle()), .completed(0)])

        let cancelButtonIcon = scheduler.createObserver(UIImage?.self)
        output.cancelButtonIcon.drive(cancelButtonIcon).disposed(by: disposeBag)
        XCTAssertEqual(cancelButtonIcon.events, [.next(0, config.inputRequired.cancelButtonIcon), .completed(0)])
    }

    func testTitle() {
        var config: Authentication.Configuration = Resolver.resolve()

        let fields1 = [InputField.email]
        XCTAssertEqual(viewModel.title(for: fields1), config.inputRequired.strings.emailTitle())

        let fields2 = [InputField.email, .unknown("username")]
        XCTAssertEqual(viewModel.title(for: fields2), config.inputRequired.strings.unknownTitle())
    }

    func testSubtitle() {
        var config: Authentication.Configuration = Resolver.resolve()

        let fields1 = [InputField.email]
        XCTAssertEqual(viewModel.subtitle(for: fields1), config.inputRequired.strings.emailSubtitle())

        let fields2 = [InputField.email, .unknown("username")]
        XCTAssertEqual(viewModel.subtitle(for: fields2), config.inputRequired.strings.unknownSubtitle())
    }

    func testPlaceholders() {
        var config: Authentication.Configuration = Resolver.resolve()

        let fields1 = [InputField.email]
        XCTAssertEqual(viewModel.placeholders(for: fields1), [config.inputRequired.strings.emailPlaceholder()])

        let fields2 = [InputField.email, .unknown("username")]
        XCTAssertEqual(viewModel.placeholders(for: fields2), [config.inputRequired.strings.emailPlaceholder(), "username"])
    }

    func testSubmitButtonTitle() {
        var config: Authentication.Configuration = Resolver.resolve()

        let fields1 = [InputField.email]
        XCTAssertEqual(viewModel.submitButtonTitle(for: fields1), config.inputRequired.strings.emailSubmitButtonTitle())

        let fields2 = [InputField.email, .unknown("username")]
        XCTAssertEqual(viewModel.submitButtonTitle(for: fields2), config.inputRequired.strings.unknownSubmitButtonTitle())
    }

    func testErrorAlertController() {
        let config: Authentication.Configuration = Resolver.resolve()

        let error = Authentication.Error.cancelledByUser
        let alert = viewModel.alertController(for: error)
        XCTAssertEqual(alert.title, config.strings.alertTitle(error)())
        XCTAssertEqual(alert.message, config.strings.alertMessage(error)())
        XCTAssertEqual(alert.actions.count, 1)
        XCTAssertEqual(alert.actions[0].title, config.strings.alertConfirmOption())
    }

    func testDidSucceed() {
        XCTAssertFalse(authenticator.didCallFinish)
        viewModel.didSucceed()
        XCTAssertTrue(authenticator.didCallFinish)
    }

    func testDidFail() {
        let disposeBag = DisposeBag()
        let scheduler = TestScheduler(initialClock: 0)

        let textField = UITextField()
        let output = viewModel.bind(
            values: [textField.rx.text],
            tap: .just(()),
            cancel: .just(()),
            submit: .just(())
        )

        let alert = scheduler.createObserver(UIAlertController.self)
        output.errorAlert.bind(to: alert).disposed(by: disposeBag)
        XCTAssert(alert.events.isEmpty)

        viewModel.didFail(with: .cancelledByUser)

        XCTAssertFalse(alert.events.isEmpty)
    }
}
