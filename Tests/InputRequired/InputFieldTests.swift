//
//  Created by Backbase R&D B.V. on 20/04/2020.
//

import XCTest
import Backbase
@testable import IdentityAuthenticationJourney

internal final class InputFieldTests: XCTestCase {
    func testKey() {
        let field1 = InputField("email")
        XCTAssertEqual(field1.key, "email")

        let field2 = InputField("other")
        XCTAssertEqual(field2.key, "other")
    }

    func testEquality() {
        XCTAssertEqual(InputField("name"), InputField("name"))
    }
}
