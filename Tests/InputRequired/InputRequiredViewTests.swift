//
//  Created by Backbase R&D B.V. on 20/04/2020.
//

import XCTest
import UIKit
@testable import IdentityAuthenticationJourney

internal final class InputRequiredViewTests: XCTestCase {
    func testViewHasSomeLayout() {
        let view = InputRequiredView()
        XCTAssertFalse(view.constraints.isEmpty, "The view should have some constraints applied")
    }
}
