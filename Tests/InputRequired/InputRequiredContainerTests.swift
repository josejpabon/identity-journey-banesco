//
//  Created by Backbase R&D B.V. on 20/04/2020.
//

import XCTest
import UIKit
import Resolver
@testable import IdentityAuthenticationJourney

internal final class InputRequiredContainerTests: XCTestCase {
    override func setUp() {
        super.setUp()
        Resolver.register { Authentication.Configuration() }
    }

    func testContainer() {
        let view = InputRequiredContainer()

        view.promptUser(forInput: ["email"])
        let inputRequiredView = view.visibleView as? InputRequiredView
        XCTAssertNotNil(inputRequiredView)
        XCTAssertEqual(view.visibleView, inputRequiredView)
    }

    func testRendering() {
        let contract = InputRequiredContract()
        let view = InputRequiredContainer.initialize(with: contract, container: UIView())
        XCTAssertNotNil(view)
        let container = view as? InputRequiredContainer
        XCTAssertNotNil(container)

        container?.promptUser(forInput: ["email"])

        container?.authenticatorDidSucceed()
        XCTAssert(contract.didCallFinish)
    }
}
