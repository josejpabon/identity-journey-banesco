//
//  Created by Backbase R&D B.V. on 24/07/2020.
//

import XCTest
@testable import IdentityAuthenticationJourney

private enum TestError: LocalizedError {
    case error

    var errorDescription: String? {
        return "test"
    }
}

internal final class LocalizableStringsTests: XCTestCase {
    func testAlertTitle() {
        let config = Authentication.Configuration()

        XCTAssertEqual(config.strings.alertTitle(.notConnected).value, "No internet connection")
        XCTAssertEqual(config.strings.alertTitle(.invalidCredentials).value, "Incorrect username or password")
        XCTAssertEqual(config.strings.alertTitle(.invalidPasscode).value, "Incorrect passcode")
        XCTAssertEqual(config.strings.alertTitle(.sessionExpired).value, "Session expired")
        XCTAssertEqual(config.strings.alertTitle(.accountLocked).value, "You’re temporarily blocked")
        XCTAssertEqual(config.strings.alertTitle(.cancelledByUser).value, "")
        XCTAssertEqual(config.strings.alertTitle(.incompleteEnrollment).value, "Setup incomplete")
        XCTAssertEqual(config.strings.alertTitle(.passcodeMismatch).value, "Passcodes do not match")
        XCTAssertEqual(config.strings.alertTitle(.biometricUsageDenied).value, "Biometrics access required.")
        XCTAssertEqual(config.strings.alertTitle(.biometricLockout).value, "Biometric sensor locked out")
        XCTAssertEqual(config.strings.alertTitle(.invalidEmail).value, "Invalid e-mail")
        XCTAssertEqual(config.strings.alertTitle(.sdk(TestError.error)).value, "Error")
    }

    func testAlertMessage() {
        let config = Authentication.Configuration()

        XCTAssertEqual(config.strings.alertMessage(.notConnected).value, "Please check your internet connection and try again.")
        XCTAssertEqual(config.strings.alertMessage(.invalidCredentials).value, "Please try again.")
        XCTAssertEqual(config.strings.alertMessage(.invalidPasscode).value, "You entered an invalid passcode, Please try again.")
        XCTAssertEqual(config.strings.alertMessage(.sessionExpired).value, "Please log in again.")
        XCTAssertEqual(config.strings.alertMessage(.accountLocked).value, "It looks like you reached the maximum attemtps and for security reasons you’ve been temporarily blocked. Please try again.")
        XCTAssertEqual(config.strings.alertMessage(.cancelledByUser).value, "")
        XCTAssertEqual(config.strings.alertMessage(.incompleteEnrollment).value, "Something went wrong during registration. Please try again.")
        XCTAssertEqual(config.strings.alertMessage(.passcodeMismatch).value, "Please try again.")
        XCTAssertEqual(config.strings.alertMessage(.biometricUsageDenied).value, "Your privacy settings are preventing us from accessing your biometrics. Fix this from Settings and try again.")
        XCTAssertEqual(config.strings.alertMessage(.biometricLockout).value, "Biometric sensor was locked out due to several wrong attempts. Lock the device, unlock it, and try again.")
        XCTAssertEqual(config.strings.alertMessage(.invalidEmail).value, "Please enter a valid address and try again.")
        XCTAssertEqual(config.strings.alertMessage(.sdk(TestError.error)).value, "test")
    }

    func testStrings() {
        XCTAssertEqual(localized(key: "alerts.notConnected.title")(), "No internet connection")
        XCTAssertEqual(localized(key: "alerts.notConnected.message")(), "Please check your internet connection and try again.")
        XCTAssertEqual(localized(key: "alerts.invalidCredentials.title")(), "Incorrect username or password")
        XCTAssertEqual(localized(key: "alerts.invalidCredentials.message")(), "Please try again.")
        XCTAssertEqual(localized(key: "alerts.invalidPasscode.title")(), "Incorrect passcode")
        XCTAssertEqual(localized(key: "alerts.invalidPasscode.message")(), "You entered an invalid passcode, Please try again.")
        XCTAssertEqual(localized(key: "alerts.sessionExpired.title")(), "Session expired")
        XCTAssertEqual(localized(key: "alerts.sessionExpired.message")(), "Please log in again.")
        XCTAssertEqual(localized(key: "alerts.accountLocked.title")(), "You’re temporarily blocked")
        XCTAssertEqual(localized(key: "alerts.accountLocked.message")(), "It looks like you reached the maximum attemtps and for security reasons you’ve been temporarily blocked. Please try again.")
        XCTAssertEqual(localized(key: "alerts.cancelledByUser.title")(), "")
        XCTAssertEqual(localized(key: "alerts.cancelledByUser.message")(), "")
        XCTAssertEqual(localized(key: "alerts.incompleteEnrollment.title")(), "Setup incomplete")
        XCTAssertEqual(localized(key: "alerts.incompleteEnrollment.message")(), "Something went wrong during registration. Please try again.")
        XCTAssertEqual(localized(key: "alerts.passcodeMismatch.title")(), "Passcodes do not match")
        XCTAssertEqual(localized(key: "alerts.passcodeMismatch.message")(), "Please try again.")
        XCTAssertEqual(localized(key: "alerts.biometricUsageDenied.title")(), "Biometrics access required.")
        XCTAssertEqual(localized(key: "alerts.biometricUsageDenied.message")(), "Your privacy settings are preventing us from accessing your biometrics. Fix this from Settings and try again.")
        XCTAssertEqual(localized(key: "alerts.biometricLockout.title")(), "Biometric sensor locked out")
        XCTAssertEqual(localized(key: "alerts.biometricLockout.message")(), "Biometric sensor was locked out due to several wrong attempts. Lock the device, unlock it, and try again.")
        XCTAssertEqual(localized(key: "alerts.unknown.title")(), "Error")
        XCTAssertEqual(localized(key: "alerts.unknown.message")(), "Something went wrong.")
        XCTAssertEqual(localized(key: "alerts.options.confirm")(), "OK")
        XCTAssertEqual(localized(key: "alerts.options.retry")(), "Retry")
        XCTAssertEqual(localized(key: "alerts.options.cancel")(), "Cancel")
        XCTAssertEqual(localized(key: "alerts.options.settings")(), "Settings")
        XCTAssertEqual(localized(key: "login.labels.title")(), "Welcome back")
        XCTAssertEqual(localized(key: "login.labels.subtitle.faceID")(), "Log in with Face ID")
        XCTAssertEqual(localized(key: "login.labels.subtitle.touchID")(), "Log in with Touch ID")
        XCTAssertEqual(localized(key: "login.labels.subtitle.lockedOut")(), "Enable Biometrics")
        XCTAssertEqual(localized(key: "login.labels.subtitle.passcode")(), "Log in with Passcode")
        XCTAssertEqual(localized(key: "login.buttons.biometric.faceID")(), "Log in with Face ID")
        XCTAssertEqual(localized(key: "login.buttons.biometric.touchID")(), "Log in with Touch ID")
        XCTAssertEqual(localized(key: "login.buttons.biometric.touchID")(), "Log in with Touch ID")
        XCTAssertEqual(localized(key: "login.buttons.biometric.lockedOut")(), "Enable Biometrics")
        XCTAssertEqual(localized(key: "login.buttons.passcode")(), "Log in with Passcode")
        XCTAssertEqual(localized(key: "register.labels.title")(), "Let’s get started")
        XCTAssertEqual(localized(key: "register.labels.subtitle")(), "Enter your personal banking credentials to get started")
        XCTAssertEqual(localized(key: "register.inputs.username.placeholder")(), "Username")
        XCTAssertEqual(localized(key: "register.inputs.username.emptyError")(), "*Username is required")
        XCTAssertEqual(localized(key: "register.inputs.password.placeholder")(), "Password")
        XCTAssertEqual(localized(key: "register.inputs.password.emptyError")(), "*Password is required")
        XCTAssertEqual(localized(key: "register.buttons.forgotUsername")(), "Forgot username?")
        XCTAssertEqual(localized(key: "register.buttons.login")(), "Log in")
        XCTAssertEqual(localized(key: "emailSent.labels.title")(), "On its way")
        XCTAssertEqual(localized(key: "emailSent.labels.subtitle")(), "If there is an account associated with this e-mail address, you will shortly receive an e-mail with your username")
        XCTAssertEqual(localized(key: "passcode.registration.labels.step1Title")(), "Create passcode")
        XCTAssertEqual(localized(key: "passcode.registration.labels.step1Subtitle")(), "Create a %d-digit code to use for logging in to your banking app")
        XCTAssertEqual(localized(key: "passcode.registration.labels.step2Title")(), "Confirm passcode")
        XCTAssertEqual(localized(key: "passcode.registration.labels.step2Subtitle")(), "Confirm your newly created %d-digit code")
        XCTAssertEqual(localized(key: "passcode.registration.buttons.reset")(), "Reset")
        XCTAssertEqual(localized(key: "passcode.change.labels.step1Title")(), "Change passcode")
        XCTAssertEqual(localized(key: "passcode.change.labels.step1Subtitle")(), "Please enter your current passcode")
        XCTAssertEqual(localized(key: "passcode.change.labels.step2Title")(), "New passcode")
        XCTAssertEqual(localized(key: "passcode.change.labels.step2Subtitle")(), "Please enter your new passcode")
        XCTAssertEqual(localized(key: "passcode.change.labels.step3Title")(), "Confirm new passcode")
        XCTAssertEqual(localized(key: "passcode.change.labels.step3Subtitle")(), "Please re-enter your new passcode for verification")
        XCTAssertEqual(localized(key: "passcode.login.labels.title")(), "Welcome back")
        XCTAssertEqual(localized(key: "passcode.login.labels.subtitle")(), "Login with passcode")
        XCTAssertEqual(localized(key: "passcode.login.buttons.troubleshoot")(), "Forgot passcode?")
        XCTAssertEqual(localized(key: "passcode.login.alerts.troubleshoot.title")(), "Forgot passcode?")
        XCTAssertEqual(localized(key: "passcode.login.alerts.troubleshoot.message")(), "This action will reset your application. You will be able to define a new passcode. Would you like to continue?")
        XCTAssertEqual(localized(key: "passcode.login.alerts.troubleshoot.options.confirm")(), "Confirm")
        XCTAssertEqual(localized(key: "biometrics.registration.labels.title.faceID")(), "Enable Face ID")
        XCTAssertEqual(localized(key: "biometrics.registration.labels.title.touchID")(), "Enable Touch ID")
        XCTAssertEqual(localized(key: "biometrics.registration.labels.subtitle.faceID")(), "Log in using Face ID instead of your password for easy account access")
        XCTAssertEqual(localized(key: "biometrics.registration.labels.subtitle.touchID")(), "Log in using Touch ID instead of your password for easy account access")
        XCTAssertEqual(localized(key: "biometrics.registration.buttons.prompt.faceID")(),  "Enable Face ID")
        XCTAssertEqual(localized(key: "biometrics.registration.buttons.prompt.touchID")(), "Enable Touch ID")
        XCTAssertEqual(localized(key: "biometrics.registration.buttons.cancel")(), "Maybe later")
        XCTAssertEqual(localized(key: "inputRequired.labels.emailTitle")(), "Forgot username")
        XCTAssertEqual(localized(key: "inputRequired.labels.emailSubtitle")(), "Please enter your e-mail address to receive instructions")
        XCTAssertEqual(localized(key: "inputRequired.inputs.email.placeholder")(), "e-mail address")
        XCTAssertEqual(localized(key: "inputRequired.labels.unknownTitle")(), "Input required")
        XCTAssertEqual(localized(key: "inputRequired.labels.unknownSubtitle")(), "Please provide the required input")
        XCTAssertEqual(localized(key: "inputRequired.buttons.email.submit")(), "Confirm e-mail address")
        XCTAssertEqual(localized(key: "inputRequired.buttons.unknown.submit")(), "Submit")
        XCTAssertEqual(localized(key: "emailSent.buttons.dismiss")(), "Done")
    }
    
    func testInitWithValue() {
        let loc = LocalizedString(value: "value")
        XCTAssertEqual(loc.value, "value")
    }
    
    func testInitWithLiteral() {
        let loc = LocalizedString(stringLiteral: "literal")
        XCTAssertEqual(loc.value, "literal")
    }

    private func localized(key: String) -> LocalizedString {
        let prefix = "authentication."
        return LocalizedString(key: prefix + key, in: .authentication)
    }
}
