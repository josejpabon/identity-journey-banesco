//
//  Created by Backbase R&D B.V. on 06/11/2020.
//

import XCTest
import BackbaseDesignSystem
@testable import IdentityAuthenticationJourney

internal final class AuthenticationButtonTypeTests: XCTestCase {
    func testBackgroundColor() {
        let colors = DesignSystem.shared.colors
        for type in Authentication.Design.ButtonType.allCases {
            switch type {
            case .primary:
                XCTAssertEqual(type.backgroundColor, colors.primary.default)
            case .secondary:
                XCTAssertEqual(type.backgroundColor, colors.secondary.default)
            case .success:
                XCTAssertEqual(type.backgroundColor, colors.success.default)
            case .warning:
                XCTAssertEqual(type.backgroundColor, colors.warning.default)
            case .danger:
                XCTAssertEqual(type.backgroundColor, colors.danger.default)
            case .info:
                XCTAssertEqual(type.backgroundColor, colors.info.default)
            case .plain:
                XCTAssertEqual(type.backgroundColor, .clear)
            }
        }
    }

    func testTintColor() {
        let colors = DesignSystem.shared.colors
        for type in Authentication.Design.ButtonType.allCases {
            switch type {
            case .primary:
                XCTAssertEqual(type.tintColor, colors.surfacePrimary.default)
            case .secondary:
                XCTAssertEqual(type.tintColor, colors.surfacePrimary.default)
            case .success:
                XCTAssertEqual(type.tintColor, colors.surfacePrimary.default)
            case .warning:
                XCTAssertEqual(type.tintColor, colors.surfacePrimary.default)
            case .danger:
                XCTAssertEqual(type.tintColor, colors.surfacePrimary.default)
            case .info:
                XCTAssertEqual(type.tintColor, colors.surfacePrimary.default)
            case .plain:
                XCTAssertEqual(type.tintColor, colors.text.default)
            }
        }
    }

    func testCornerRaduis() {
        for type in Authentication.Design.ButtonType.allCases {
            switch type {
            case .primary:
                XCTAssertEqual(type.cornerRadius, 8)
            case .secondary:
                XCTAssertEqual(type.cornerRadius, 8)
            case .success:
                XCTAssertEqual(type.cornerRadius, 8)
            case .warning:
                XCTAssertEqual(type.cornerRadius, 8)
            case .danger:
                XCTAssertEqual(type.cornerRadius, 8)
            case .info:
                XCTAssertEqual(type.cornerRadius, 8)
            case .plain:
                XCTAssertEqual(type.cornerRadius, 0)
            }
        }
    }

    func testContentEdgeInsets() {
        let insets = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
        for type in Authentication.Design.ButtonType.allCases {
            switch type {
            case .primary:
                XCTAssertEqual(type.contentEdgeInsets, insets)
            case .secondary:
                XCTAssertEqual(type.contentEdgeInsets, insets)
            case .success:
                XCTAssertEqual(type.contentEdgeInsets, insets)
            case .warning:
                XCTAssertEqual(type.contentEdgeInsets, insets)
            case .danger:
                XCTAssertEqual(type.contentEdgeInsets, insets)
            case .info:
                XCTAssertEqual(type.contentEdgeInsets, insets)
            case .plain:
                XCTAssertEqual(type.contentEdgeInsets, .zero)
            }
        }
    }
}
