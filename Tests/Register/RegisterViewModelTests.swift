//
//  Created by Backbase R&D B.V. on 3/25/20.
//

import XCTest
import RxSwift
import RxTest
import Resolver
@testable import IdentityAuthenticationJourney

internal final class RegisterViewModelTests: XCTestCase {
    override func setUp() {
        super.setUp()
        Resolver.register { Authentication.Configuration() }
        Resolver.register { FalsyAuthUseCase() as AuthenticationUseCase }
    }

    func testBinding() {
        var config: Authentication.Configuration = Resolver.resolve()

        let disposeBag = DisposeBag()
        let scheduler = TestScheduler(initialClock: 0)

        let viewModel = RegisterViewModel(session: .none)

        let output = viewModel.bind(
            username: .just("john"),
            password: .just("123456"),
            forgotUsername: .just(()),
            login: .just(()),
            tap: .just(()),
            willAppear: .just(()),
            didAppear: .just(())
        )

        let title = scheduler.createObserver(String.self)
        output.title.drive(title).disposed(by: disposeBag)
        XCTAssertEqual(title.events, [.next(0, config.register.strings.title()), .completed(0)])

        let subtitle = scheduler.createObserver(String.self)
        output.subtitle.drive(subtitle).disposed(by: disposeBag)
        XCTAssertEqual(subtitle.events, [.next(0, config.register.strings.subtitle()), .completed(0)])

        let usernamePlaceholder = scheduler.createObserver(String?.self)
        output.usernameInputState.map({ $0?.label }).drive(usernamePlaceholder).disposed(by: disposeBag)
        XCTAssertEqual(usernamePlaceholder.events, [.next(0, config.register.strings.usernamePlaceholder())])

        let passwordPlaceholder = scheduler.createObserver(String?.self)
        output.passwordInputState.map({ $0?.label }).drive(passwordPlaceholder).disposed(by: disposeBag)
        XCTAssertEqual(passwordPlaceholder.events, [.next(0, config.register.strings.passwordPlaceholder())])

        let forgotUsernameButtonTitle = scheduler.createObserver(String.self)
        output.forgotUsernameButtonTitle.drive(forgotUsernameButtonTitle).disposed(by: disposeBag)
        XCTAssertEqual(forgotUsernameButtonTitle.events, [.next(0, config.register.strings.forgotUsernameButtonTitle()), .completed(0)])

        let loginButtonTitle = scheduler.createObserver(String.self)
        output.loginButtonTitle.drive(loginButtonTitle).disposed(by: disposeBag)
        XCTAssertEqual(loginButtonTitle.events, [.next(0, config.register.strings.loginButtonTitle()), .completed(0)])

        let authenticateAlert = scheduler.createObserver(UIAlertController.self)
        output.errorAlert.emit(to: authenticateAlert).disposed(by: disposeBag)
        viewModel.authenticate(user: User(username: "john", password: "123456"))
        XCTAssertFalse(authenticateAlert.events.isEmpty)
    }

    func testAlertController() {
        let config: Authentication.Configuration = Resolver.resolve()

        let viewModel = RegisterViewModel(session: .none)
        let error = Authentication.Error.notConnected
        let alert = viewModel.alertController(for: error)
        XCTAssertEqual(alert.title, config.strings.alertTitle(error)())
        XCTAssertEqual(alert.message, config.strings.alertMessage(error)())
        XCTAssertEqual(alert.preferredStyle, .alert)

        let dismiss = alert.actions.first
        XCTAssertEqual(dismiss?.title, config.strings.alertConfirmOption())
    }

    func testAccountLockedAlertController() {
        let config: Authentication.Configuration = Resolver.resolve()

        let viewModel = RegisterViewModel(session: .none)
        let alert = viewModel.accountLockedAlertController()
        XCTAssertEqual(alert.title, config.strings.alertTitle(.accountLocked)())
        XCTAssertEqual(alert.message, config.strings.alertMessage(.accountLocked)())
        XCTAssertEqual(alert.preferredStyle, .alert)

        let dismiss = alert.actions.first
        XCTAssertEqual(dismiss?.title, config.strings.alertConfirmOption())
    }
}
