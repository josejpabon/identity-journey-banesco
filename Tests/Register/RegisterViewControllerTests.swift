//
//  Created by Backbase R&D B.V. on 03/04/2020.
//

import XCTest
import UIKit
@testable import IdentityAuthenticationJourney

internal final class RegisterViewControllerTests: XCTestCase {
    func testViewControllerHasSomeLayout() {
        let viewController = RegisterViewController()
        XCTAssertFalse(viewController.view.constraints.isEmpty, "The view should have some constraints applied")
    }
}
