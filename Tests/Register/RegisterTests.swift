//
//  Created by Backbase R&D B.V. on 17/4/20.
//

import XCTest
import Resolver
@testable import IdentityAuthenticationJourney

internal final class RegisterTests: XCTestCase {
    override func setUp() {
        super.setUp()
        Resolver.register { Authentication.Configuration() }
        Resolver.register { FalsyAuthUseCase() as AuthenticationUseCase }
    }

    func testScreenIsReturningCorrectViewController() {
        let navigationController = UINavigationController()
        let controller = Register.build(session: .none)(navigationController)
        XCTAssert(controller is RegisterViewController)
    }

    func testRouter() {
        var configuration = Authentication.Configuration()
        XCTAssertNotNil(configuration.register.router.didTapForgotUsername)
        XCTAssertNotNil(configuration.register.router.preCompletion)
    }

    func testAnimations() {
        var configuration = Authentication.Configuration()
        XCTAssertNotNil(configuration.register.animation?.onLoading)
    }
}
