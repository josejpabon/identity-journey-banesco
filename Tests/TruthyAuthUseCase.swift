//
//  Created by Backbase R&D B.V. on 03/04/2020.
//

import Foundation
import IdentityAuthenticationJourney

internal final class TruthyAuthUseCase: NSObject, AuthenticationUseCase {
    func authenticate(user: User, preCompletion: (() -> Void)?, postCompletion: AuthenticationHandler?) {
        postCompletion?(.success([:]))
    }

    var lastSession: Session?

    func authenticate(user: User, callback: AuthenticationHandler?) {
        callback?(.success([:]))
    }

    func authenticateWithBiometrics(callback: AuthenticationHandler?) {
        callback?(.success([:]))
    }

    func authenticateWithPasscode(callback: AuthenticationHandler?) {
        callback?(.success([:]))
    }

    func forgotUsername(callback: ForgottenCredentialsHandler?) {
        callback?(.success(Data()))
    }

    func forgotPassword(callback: ForgottenCredentialsHandler?) {
        callback?(.success(Data()))
    }

    func changePasscode(callback: PasscodeChangeHandler?) {
        callback?(.success(()))
    }

    func completeRegistration(callback: AuthenticationHandler?) {
        callback?(.success([:]))
    }

    func endSession(callback: SessionHandler?) {
        callback?(.valid)
    }

    func logOut(callback: SessionHandler?) {
        callback?(.valid)
    }

    func expireSession(callback: SessionHandler?) {
        callback?(.expired)
    }

    func lockAccount(callback: SessionHandler?) {
        callback?(.locked)
    }

    func validateSession(callback: SessionHandler?) {
        callback?(.valid)
    }

    var cachedUsername: String? {
        return "john"
    }

    var isEnrolled: Bool {
        return true
    }

    var isBiometricEnrolled: Bool {
        return true
    }

    var isPasscodeEnrolled: Bool {
        return true
    }
}
