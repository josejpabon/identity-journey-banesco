//
//  Created by Backbase R&D B.V. on 17/04/2020.
//

import XCTest
import Backbase
@testable import IdentityAuthenticationJourney

internal final class SessionStateTests: XCTestCase {
    func testValidSession() {
        let state = SessionState.valid
        let session = state.session
        XCTAssertEqual(session, .valid)
    }

    func testNoneSession() {
        let state = SessionState.none
        let session = state.session
        XCTAssertEqual(session, .none)
    }
}
