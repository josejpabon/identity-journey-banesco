//
//  Created by Backbase R&D B.V. on 17/04/2020.
//

import XCTest
@testable import IdentityAuthenticationJourney

internal final class BiometricAuthenticationViewModelTests: XCTestCase {
    func testDidSucceed() {
        let contract = BiometricContract()
        let viewModel = BiometricAuthenticationViewModel(contract: contract)
        viewModel.didSucceed()
        XCTAssert(contract.didCallFinish)
    }

    func testDidFail() {
        let contract = BiometricContract()
        let viewModel = BiometricAuthenticationViewModel(contract: contract)
        viewModel.didFail(with: .cancelledByUser)
        XCTAssert(contract.didCallCancel)
    }
}
