//
//  Created by Backbase R&D B.V. on 03/04/2020.
//

import BackbaseIdentity

internal final class BiometricAuthenticator: BBIDBiometricAuthenticator {
    var didCallFinish = false
    override func finish() {
        didCallFinish = true
    }

    var didCallCancel = false
    override func cancel() {
        didCallCancel = true
    }

    var didCallRetry = false
    override func retry() {
        didCallRetry = true
    }

    var didCallAuthorize = false
    override func authorize() -> Bool {
        didCallAuthorize = true
        return true
    }
}
