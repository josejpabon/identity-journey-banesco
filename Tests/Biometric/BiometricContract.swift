//
//  Created by Backbase R&D B.V. on 03/04/2020.
//

import Backbase
import BackbaseIdentity

internal final class BiometricContract: NSObject, NativeContract, BBIDBiometricAuthenticatorContract {
    var wasExplictlyDismissed: Bool = false
    var renderable: Renderable = FakeRenderable()

    var didCallAuthorize = false
    func authorize() -> Bool {
        didCallAuthorize = true
        return didCallAuthorize
    }

    var didCallRetry = false
    func retry() {
        didCallRetry = true
    }

    var didCallCancel = false
    func cancel() {
        didCallCancel = true
    }

    var didCallFinish = false
    func finish() {
        didCallFinish = true
    }

    var didCallUserDidDenyBiometricUsage = false
    func userDidDenyBiometricUsage() {
        didCallUserDidDenyBiometricUsage = true
    }

    var didCallUserDidGrantBiometricUsage = false
    func userDidGrantBiometricUsage() {
        didCallUserDidGrantBiometricUsage = true
    }

    func supportedUserVerification() -> [AnyHashable: Any] {
        return [:]
    }

    func aaid() -> String {
        return "test"
    }

    func version() -> UInt16 {
        return 0
    }

    func signCounter() -> UInt32 {
        return 0
    }

    func registrationCounter() -> UInt32 {
        return 0
    }
}
