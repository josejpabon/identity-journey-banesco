//
//  Created by Backbase R&D B.V. on 17/04/2020.
//

import XCTest
import RxTest
import RxSwift
import Resolver
import BackbaseIdentity
@testable import IdentityAuthenticationJourney

internal final class BiometricRegistrationViewModelTests: XCTestCase {
    private var authenticator = BiometricAuthenticator()
    private lazy var viewModel = BiometricRegistrationViewModel(contract: authenticator)

    override func setUp() {
        Resolver.register { Authentication.Configuration() }

        authenticator = BiometricAuthenticator()
        viewModel = BiometricRegistrationViewModel(contract: authenticator)
    }

    func testInitialization() {
        let viewModel = BiometricRegistrationViewModel(contract: authenticator)
        XCTAssertNotNil(viewModel.contract)
    }

    func testBinding() {
        let disposeBag = DisposeBag()
        let scheduler = TestScheduler(initialClock: 0)
        var config: Authentication.Configuration = Resolver.resolve()

        let output = viewModel.bind(allow: .just(()), deny: .just(()), cancel: .just(()))

        let title = scheduler.createObserver(String.self)
        output.title.drive(title).disposed(by: disposeBag)
        XCTAssertEqual(title.events, [.next(0, viewModel.promptTitle()), .completed(0)])

        let subtitle = scheduler.createObserver(String.self)
        output.subtitle.drive(subtitle).disposed(by: disposeBag)
        XCTAssertEqual(subtitle.events, [.next(0, viewModel.promptSubtitle()), .completed(0)])

        let allowButtonTitle = scheduler.createObserver(String.self)
        output.allowButtonTitle.drive(allowButtonTitle).disposed(by: disposeBag)
        XCTAssertEqual(allowButtonTitle.events, [.next(0, viewModel.allowUsageButtonTitle()), .completed(0)])

        let denyButtonTitle = scheduler.createObserver(String.self)
        output.denyButtonTitle.drive(denyButtonTitle).disposed(by: disposeBag)
        XCTAssertEqual(denyButtonTitle.events, [.next(0, config.biometric.strings.denyUsageButtonTitle()), .completed(0)])
    }

    func testPromptImage() {
        var config: Authentication.Configuration = Resolver.resolve()
        XCTAssertEqual(viewModel.promptImage(for: .faceID), config.biometric.promptImage(.faceID))
        XCTAssertEqual(viewModel.promptImage(for: .touchID), config.biometric.promptImage(.touchID))
        XCTAssertEqual(viewModel.promptImage(for: .none), nil)
    }

    func testPromptTitle() {
        var config: Authentication.Configuration = Resolver.resolve()
        XCTAssertEqual(viewModel.promptTitle(for: .faceID), config.biometric.strings.title(.faceID)?())
        XCTAssertEqual(viewModel.promptTitle(for: .touchID), config.biometric.strings.title(.touchID)?())
        XCTAssertEqual(viewModel.promptTitle(for: .none), "")
    }

    func testPromptMessage() {
        var config: Authentication.Configuration = Resolver.resolve()
        XCTAssertEqual(viewModel.promptSubtitle(for: .faceID), config.biometric.strings.subtitle(.faceID)?())
        XCTAssertEqual(viewModel.promptSubtitle(for: .touchID), config.biometric.strings.subtitle(.touchID)?())
        XCTAssertEqual(viewModel.promptSubtitle(for: .none), "")
    }

    func testAllowUsageTitle() {
        var config: Authentication.Configuration = Resolver.resolve()
        XCTAssertEqual(viewModel.allowUsageButtonTitle(for: .faceID), config.biometric.strings.promptButtonTitle(.faceID)?())
        XCTAssertEqual(viewModel.allowUsageButtonTitle(for: .touchID), config.biometric.strings.promptButtonTitle(.touchID)?())
        XCTAssertEqual(viewModel.allowUsageButtonTitle(for: .none), "")
    }

    func testFinish() {
        XCTAssertFalse(authenticator.didCallFinish)
        viewModel.finish()
        XCTAssertTrue(authenticator.didCallFinish)
    }

    func testCancel() {
        XCTAssertFalse(authenticator.didCallCancel)
        viewModel.cancel()
        XCTAssertTrue(authenticator.didCallCancel)
    }

    func testFailureWithCancelledByUser() {
        viewModel.didFail(with: .cancelledByUser)
        XCTAssertTrue(authenticator.didCallCancel)
    }

    func testFailureWithError() {
        viewModel.didFail(with: .accountLocked)
        XCTAssertFalse(authenticator.didCallCancel)
    }

    func testRetry() {
        XCTAssertFalse(authenticator.didCallRetry)
        viewModel.retry()
        XCTAssertTrue(authenticator.didCallRetry)
    }

    func testDeniedBySystemAlert() {
        let config: Authentication.Configuration = Resolver.resolve()

        let error = Authentication.Error.biometricUsageDenied
        let alert = viewModel.errorAlertController(for: error)
        XCTAssertEqual(alert.title, config.strings.alertTitle(error)())
        XCTAssertEqual(alert.message, config.strings.alertMessage(error)())

        let actions = alert.actions
        XCTAssertEqual(actions.count, 2)
        XCTAssertEqual(actions[0].title, config.strings.alertSettingsOption())
        XCTAssertEqual(actions[1].title, config.strings.alertCancelOption())
    }

    func testErrorAlert() {
        let config: Authentication.Configuration = Resolver.resolve()

        let error = Authentication.Error.biometricLockout
        let alert = viewModel.errorAlertController(for: error)
        XCTAssertEqual(alert.title, config.strings.alertTitle(error)())
        XCTAssertEqual(alert.message, config.strings.alertMessage(error)())

        let actions = alert.actions
        XCTAssertEqual(actions.count, 2)
        XCTAssertEqual(actions[0].title, config.strings.alertRetryOption())
        XCTAssertEqual(actions[1].title, config.strings.alertCancelOption())
    }
}
