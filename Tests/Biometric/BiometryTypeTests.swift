//
//  Created by Backbase R&D B.V. on 17/04/2020.
//

import XCTest
import LocalAuthentication
@testable import IdentityAuthenticationJourney

internal final class BiometryTypeTests: XCTestCase {
    func testInitialization() {
        XCTAssertEqual(BiometryType(.faceID), .faceID)
        XCTAssertEqual(BiometryType(.touchID), .touchID)
        XCTAssertEqual(BiometryType(.none), .none)
    }

    func testSupported() {
        let context = LAContext()
        _ = context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
        switch context.biometryType {
        case .faceID:
            XCTAssertEqual(BiometryType.supported, .faceID)
        case .touchID:
            XCTAssertEqual(BiometryType.supported, .touchID)
        case .none:
            XCTAssertEqual(BiometryType.supported, .none)
        @unknown default:
            XCTAssertEqual(BiometryType.supported, .none)
        }
    }

    func testAllowed() {
        XCTAssertEqual(BiometryType.allowed, .denied)
    }
}
