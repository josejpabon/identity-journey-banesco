//
//  Created by Backbase R&D B.V. on 17/04/2020.
//

import XCTest
import UIKit
import Resolver
@testable import IdentityAuthenticationJourney

internal final class BiometricContainerTests: XCTestCase {
    override func setUp() {
        super.setUp()
        Resolver.register { Authentication.Configuration() }
    }

    func testContainer() {
        let view = BiometricContainer()

        view.promptForBiometricAuthentication()
        XCTAssertNil(view.visibleView)

        view.promptForBiometricUsage()
        let registrationView = view.visibleView as? BiometricRegistrationView
        XCTAssertNotNil(registrationView)
        XCTAssertEqual(view.visibleView, registrationView)
    }

    func testRendering() {
        let contract = BiometricContract()
        let view = BiometricContainer.initialize(with: contract, container: UIView())
        XCTAssertNotNil(view)
        let container = view as? BiometricContainer
        XCTAssertNotNil(container)

        container?.promptForBiometricUsage()

        container?.authenticatorDidSucceed()
        XCTAssert(contract.didCallFinish)
    }
}
