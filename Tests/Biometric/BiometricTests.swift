//
//  Created by Backbase R&D B.V. on 3/29/20.
//

import XCTest
import Backbase
@testable import IdentityAuthenticationJourney

internal final class BiometricTests: XCTestCase {
    func testAuthenticator() {
        let authenticator = Biometric.authenticator
        XCTAssertFalse(authenticator.authorize())
    }
}
