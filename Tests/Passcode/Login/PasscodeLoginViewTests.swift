//
//  Created by Backbase R&D B.V. on 17/04/2020.
//

import XCTest
import UIKit
@testable import IdentityAuthenticationJourney

internal final class PasscodeLoginViewTests: XCTestCase {
    func testViewHasSomeLayout() {
        let view = PasscodeLoginView()
        XCTAssertFalse(view.constraints.isEmpty, "The view should have some constraints applied")
    }
}
