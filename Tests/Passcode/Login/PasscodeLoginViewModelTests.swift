//
//  Created by Backbase R&D B.V. on 17/04/2020.
//

import XCTest
import RxSwift
import RxTest
import Resolver

@testable import IdentityAuthenticationJourney

internal final class PasscodeLoginViewModelTests: XCTestCase {
    private var authenticator = PasscodeAuthenticator()
    private lazy var viewModel = PasscodeLoginViewModel(contract: authenticator)

    override func setUp() {
        Resolver.register { Authentication.Configuration() }

        authenticator = PasscodeAuthenticator()
        viewModel = PasscodeLoginViewModel(contract: authenticator)
    }

    func testInitialization() {
        let viewModel = PasscodeLoginViewModel(contract: authenticator)
        XCTAssertNotNil(viewModel.contract)
    }

    func testBinding() {
        var config: Authentication.Configuration = Resolver.resolve()

        let disposeBag = DisposeBag()
        let scheduler = TestScheduler(initialClock: 0)

        let view = Authentication.Design.OTPInput(length: 5)
        let output = viewModel.bind(passcode: view.rx.text, cancel: .just(()), troubleshoot: .just(()))

        let title = scheduler.createObserver(String.self)
        output.title.drive(title).disposed(by: disposeBag)
        XCTAssertEqual(title.events, [.next(0, config.passcode.strings.loginTitle()), .completed(0)])

        let subtitle = scheduler.createObserver(String.self)
        output.subtitle.drive(subtitle).disposed(by: disposeBag)
        XCTAssertEqual(subtitle.events, [.next(0, config.passcode.strings.loginSubtitle()), .completed(0)])

        let cancelButtonIcon = scheduler.createObserver(UIImage?.self)
        output.cancelButtonIcon.drive(cancelButtonIcon).disposed(by: disposeBag)
        XCTAssertEqual(cancelButtonIcon.events, [.next(0, config.passcode.cancelButtonIcon), .completed(0)])

        view.text = "00000"
        viewModel.resetFlowSubject.onNext(())
        XCTAssertNil(view.text)
    }

    func testErrorAlertController() {
        let config: Authentication.Configuration = Resolver.resolve()

        let error = Authentication.Error.cancelledByUser
        let alert = viewModel.errorAlertController(for: error)
        XCTAssertEqual(alert.title, config.strings.alertTitle(error)())
        XCTAssertEqual(alert.message, config.strings.alertMessage(error)())
        XCTAssertEqual(alert.actions.count, 2)
        XCTAssertEqual(alert.actions[0].title, config.strings.alertRetryOption())
        XCTAssertEqual(alert.actions[1].title, config.strings.alertCancelOption())
    }

    func testTroubleshootPasscode() {
        var config: Authentication.Configuration = Resolver.resolve()
        let navVC = UINavigationController(rootViewController: UIViewController())
        let troubleshootRoute = config.passcode.router.didTapTroubleshoot(navVC)

        // We have to setup a window, otherwise we won't be able to present the alert view.
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = navVC
        window.makeKeyAndVisible()
        
        troubleshootRoute {}
        
        let alert = navVC.presentedViewController as! UIAlertController
        
        XCTAssertEqual(alert.title, config.passcode.strings.loginTroubleshootAlertTitle())
        XCTAssertEqual(alert.message, config.passcode.strings.loginTroubleshootAlertMessage())
        XCTAssertEqual(alert.actions.count, 2)
        XCTAssertEqual(alert.actions[0].title, config.passcode.strings.loginTroubleshootAlertConfirmOption())
        XCTAssertEqual(alert.actions[1].title, config.strings.alertCancelOption())
    }

    func testDidSucceed() {
        XCTAssertFalse(authenticator.didCallFinish)
        viewModel.didSucceed()
        XCTAssertTrue(authenticator.didCallFinish)
    }

    func testDidFail() {
        let disposeBag = DisposeBag()
        let scheduler = TestScheduler(initialClock: 0)

        let view = Authentication.Design.OTPInput(length: 5)
        let output = viewModel.bind(passcode: view.rx.text, cancel: .just(()), troubleshoot: .just(()))

        let alert = scheduler.createObserver(UIAlertController.self)
        output.alert.emit(to: alert).disposed(by: disposeBag)
        XCTAssert(alert.events.isEmpty)

        viewModel.didFail(with: .cancelledByUser)

        XCTAssertFalse(alert.events.isEmpty)
    }
}
