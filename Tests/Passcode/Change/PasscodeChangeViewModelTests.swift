//
//  Created by Backbase R&D B.V. on 17/04/2020.
//

import XCTest
import RxSwift
import RxTest
import Resolver

@testable import IdentityAuthenticationJourney

internal final class PasscodeChangeViewModelTests: XCTestCase {
    private enum Error: Swift.Error {
        case error
    }

    private var authenticator = PasscodeAuthenticator()
    private lazy var viewModel = PasscodeChangeViewModel(contract: authenticator)

    override func setUp() {
        Resolver.register { Authentication.Configuration() }

        authenticator = PasscodeAuthenticator()
        viewModel = PasscodeChangeViewModel(contract: authenticator)
    }

    func testInitialization() {
        let viewModel = PasscodeLoginViewModel(contract: authenticator)
        XCTAssertNotNil(viewModel.contract)
    }

    func testBinding() {
        var config: Authentication.Configuration = Resolver.resolve()

        let disposeBag = DisposeBag()
        let scheduler = TestScheduler(initialClock: 0)

        let view = Authentication.Design.OTPInput(length: 5)
        let output = viewModel.bind(passcode: view.rx.text, cancel: .just(()))

        let title = scheduler.createObserver(String.self)
        output.title.drive(title).disposed(by: disposeBag)
        XCTAssertEqual(title.events, [.next(0, config.passcode.strings.changeStep1Title())])

        let subtitle = scheduler.createObserver(String.self)
        output.subtitle.drive(subtitle).disposed(by: disposeBag)
        XCTAssertEqual(subtitle.events, [.next(0, config.passcode.strings.changeStep1Subtitle())])

        let cancelButtonIcon = scheduler.createObserver(UIImage?.self)
        output.cancelButtonIcon.drive(cancelButtonIcon).disposed(by: disposeBag)
        XCTAssertEqual(cancelButtonIcon.events, [.next(0, config.passcode.cancelButtonIcon), .completed(0)])

        view.text = "00000"
        viewModel.resetFlowSubject.onNext(())
        XCTAssertNil(view.text)
    }

    func testTitleAndMessageChangeOnEntry() {
        var config: Authentication.Configuration = Resolver.resolve()

        let disposeBag = DisposeBag()
        let scheduler = TestScheduler(initialClock: 0)

        let view = Authentication.Design.OTPInput(length: 5)
        let output = viewModel.bind(passcode: view.rx.text, cancel: .just(()))

        viewModel.processPasscode(current: "00000", control: view.rx.text)

        let title = scheduler.createObserver(String.self)
        output.title.drive(title).disposed(by: disposeBag)
        XCTAssertEqual(title.events, [.next(0, config.passcode.strings.changeStep2Title())])

        let subtitle = scheduler.createObserver(String.self)
        output.subtitle.drive(subtitle).disposed(by: disposeBag)
        XCTAssertEqual(subtitle.events, [.next(0, config.passcode.strings.changeStep2Subtitle())])

        viewModel.processPasscode(current: "11111", control: view.rx.text)

        XCTAssertEqual(title.events, [
            .next(0, config.passcode.strings.changeStep2Title()),
            .next(0, config.passcode.strings.changeStep3Title())
        ])
        XCTAssertEqual(subtitle.events, [
            .next(0, config.passcode.strings.changeStep2Subtitle()),
            .next(0, config.passcode.strings.changeStep3Subtitle())
        ])

        viewModel.processPasscode(current: "11111", control: view.rx.text)
        XCTAssertEqual(authenticator.oldPasscode, "00000")
        XCTAssertEqual(authenticator.newPasscode, "11111")
    }

    func testAttemptChange() {
        viewModel.attemptChange(old: "00000", new: "11111", confirmed: "11111")
        XCTAssertEqual(authenticator.oldPasscode, "00000")
        XCTAssertEqual(authenticator.newPasscode, "11111")
    }

    func testAttemptChangeWithMismatch() {
        let disposeBag = DisposeBag()
        let scheduler = TestScheduler(initialClock: 0)

        let view = Authentication.Design.OTPInput(length: 5)
        let output = viewModel.bind(passcode: view.rx.text, cancel: .just(()))

        let alert = scheduler.createObserver(UIAlertController.self)
        output.errorAlert.bind(to: alert).disposed(by: disposeBag)
        XCTAssert(alert.events.isEmpty)

        viewModel.attemptChange(old: "00000", new: "11111", confirmed: "22222")

        XCTAssertFalse(alert.events.isEmpty)
    }

    func testErrorAlertController() {
        let config: Authentication.Configuration = Resolver.resolve()

        let error = Authentication.Error.cancelledByUser
        let alert = viewModel.errorAlertController(for: error)
        XCTAssertEqual(alert.title, config.strings.alertTitle(error)())
        XCTAssertEqual(alert.message, config.strings.alertMessage(error)())
        XCTAssertEqual(alert.actions.count, 1)
        XCTAssertEqual(alert.actions[0].title, config.strings.alertRetryOption())
    }

    func testDidSucceed() {
        XCTAssertFalse(authenticator.didCallFinish)
        viewModel.didSucceed()
        XCTAssertTrue(authenticator.didCallFinish)
    }

    func testDidFail() {
        let disposeBag = DisposeBag()
        let scheduler = TestScheduler(initialClock: 0)

        let view = Authentication.Design.OTPInput(length: 5)
        let output = viewModel.bind(passcode: view.rx.text, cancel: .just(()))

        let alert = scheduler.createObserver(UIAlertController.self)
        output.errorAlert.bind(to: alert).disposed(by: disposeBag)
        XCTAssert(alert.events.isEmpty)

        viewModel.didFail(with: .cancelledByUser)

        XCTAssertFalse(alert.events.isEmpty)
    }
}
