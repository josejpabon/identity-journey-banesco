//
//  Created by Backbase R&D B.V. on 03/04/2020.
//

import BackbaseIdentity

internal final class PasscodeAuthenticator: BBIDPasscodeAuthenticator {
    var didCallFinish = false
    override func finish() {
        didCallFinish = true
    }

    var didCallCancel = false
    override func cancel() {
        didCallCancel = true
    }

    var oldPasscode: String?
    var newPasscode: String?
    override func changePasscode(_ oldPasscode: String, newPasscode: String) {
        self.oldPasscode = oldPasscode
        self.newPasscode = newPasscode
    }

    var passcode: String?
    override func passcodeDidEnter(_ passcode: String) {
        self.passcode = passcode
    }
}
