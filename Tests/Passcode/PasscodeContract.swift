//
//  Created by Backbase R&D B.V. on 17/4/20.
//

import Backbase
import BackbaseIdentity

internal final class PasscodeContract: NSObject, NativeContract, BBIDPasscodeAuthenticatorContract {
    var wasExplictlyDismissed: Bool = false
    var renderable: Renderable = FakeRenderable()

    var passcode: String?
    func passcodeDidEnter(_ passcode: String) {
        self.passcode = passcode
    }

    var oldPasscord: String?
    var newPasscode: String?
    func changePasscode(_ oldPasscode: String, newPasscode: String) {
        self.oldPasscord = oldPasscode
        self.newPasscode = newPasscode
    }

    var didCallCancel = false
    func cancel() {
        didCallCancel = true
    }

    var didCallFinish = false
    func finish() {
        didCallFinish = true
    }

    func supportedUserVerification() -> [AnyHashable: Any] {
        return [:]
    }

    func aaid() -> String {
        return "test"
    }

    func version() -> UInt16 {
        return 0
    }

    func signCounter() -> UInt32 {
        return 0
    }

    func registrationCounter() -> UInt32 {
        return 0
    }
}
