//
//  Created by Backbase R&D B.V. on 17/04/2020.
//

import XCTest
import UIKit
import Resolver
@testable import IdentityAuthenticationJourney

internal final class PasscodeContainerTests: XCTestCase {
    override func setUp() {
        super.setUp()
        Resolver.register { Authentication.Configuration() }
    }

    func testContainer() {
        let view = PasscodeContainer()

        view.promptForPasscode()
        let loginView = view.visibleView as? PasscodeLoginView
        XCTAssertNotNil(loginView)
        XCTAssertEqual(view.visibleView, loginView)

        view.promptForPasscodeSelection()
        let registrationView = view.visibleView as? PasscodeRegistrationView
        XCTAssertNotNil(registrationView)
        XCTAssertEqual(view.visibleView, registrationView)

        view.promptForPasscodeChange()
        let changeView = view.visibleView as? PasscodeChangeView
        XCTAssertNotNil(changeView)
        XCTAssertEqual(view.visibleView, changeView)
    }

    func testRendering() {
        let contract = PasscodeContract()
        let view = PasscodeContainer.initialize(with: contract, container: UIView())
        XCTAssertNotNil(view)
        let container = view as? PasscodeContainer
        XCTAssertNotNil(container)

        container?.promptForPasscode()

        container?.authenticatorDidSucceed()
        XCTAssert(contract.didCallFinish)
    }
}
