//
//  Created by Backbase R&D B.V. on 17/04/2020.
//

import XCTest
import RxSwift
import RxTest
import Resolver

@testable import IdentityAuthenticationJourney

internal final class PasscodeRegistrationViewModelTests: XCTestCase {
    private var authenticator = PasscodeAuthenticator()
    private lazy var viewModel = PasscodeRegistrationViewModel(contract: authenticator)

    override func setUp() {
        Resolver.register { Authentication.Configuration() }

        authenticator = PasscodeAuthenticator()
        viewModel = PasscodeRegistrationViewModel(contract: authenticator)
    }

    func testInitialization() {
        let viewModel = PasscodeLoginViewModel(contract: authenticator)
        XCTAssertNotNil(viewModel.contract)
    }

    func testBinding() {
        var config: Authentication.Configuration = Resolver.resolve()

        let disposeBag = DisposeBag()
        let scheduler = TestScheduler(initialClock: 0)

        let view = Authentication.Design.OTPInput(length: 5)
        let output = viewModel.bind(passcode: view.rx.text, cancel: .just(()), resetFlow: .just(()))

        let title = scheduler.createObserver(String.self)
        output.title.drive(title).disposed(by: disposeBag)
        XCTAssertEqual(title.events, [.next(0, config.passcode.strings.registrationStep1Title())])

        let subtitle = scheduler.createObserver(String.self)
        output.subtitle.drive(subtitle).disposed(by: disposeBag)
        XCTAssertEqual(subtitle.events, [.next(0, viewModel.step1Subtitle)])

        view.text = "00000"
        viewModel.retryConfirmationSubject.onNext(())
        XCTAssertNil(view.text)
    }

    func testAttemptRegistration() {
        let disposeBag = DisposeBag()
        let scheduler = TestScheduler(initialClock: 0)

        let passcode = scheduler.createObserver(String.self)
        viewModel.passcodeSubject.bind(to: passcode).disposed(by: disposeBag)

        viewModel.attemptRegistration(initial: "11111", confirmed: "11111")

        XCTAssertEqual(passcode.events, [.next(0, "11111")])
    }

    func testAttemptRegistrationWithMismatch() {
        let disposeBag = DisposeBag()
        let scheduler = TestScheduler(initialClock: 0)

        let view = Authentication.Design.OTPInput(length: 5)
        let output = viewModel.bind(passcode: view.rx.text, cancel: .just(()), resetFlow: .just(()))

        let alert = scheduler.createObserver(UIAlertController.self)
        output.errorAlert.bind(to: alert).disposed(by: disposeBag)
        XCTAssert(alert.events.isEmpty)

        viewModel.attemptRegistration(initial: "11111", confirmed: "22222")

        XCTAssertFalse(alert.events.isEmpty)
    }

    func testAlertController() {
        let config: Authentication.Configuration = Resolver.resolve()

        let error = Authentication.Error.cancelledByUser
        let alert = viewModel.errorAlertController(for: error)
        XCTAssertEqual(alert.title, config.strings.alertTitle(error)())
        XCTAssertEqual(alert.message, config.strings.alertMessage(error)())
        XCTAssertEqual(alert.actions.count, 1)
        XCTAssertEqual(alert.actions[0].title, config.strings.alertRetryOption())
    }

    func testDidSucceed() {
        XCTAssertFalse(authenticator.didCallFinish)
        viewModel.didSucceed()
        XCTAssertTrue(authenticator.didCallFinish)
    }

    func testDidFail() {
        let disposeBag = DisposeBag()
        let scheduler = TestScheduler(initialClock: 0)

        let view = Authentication.Design.OTPInput(length: 5)
        let output = viewModel.bind(passcode: view.rx.text, cancel: .just(()), resetFlow: .just(()))

        let alert = scheduler.createObserver(UIAlertController.self)
        output.errorAlert.bind(to: alert).disposed(by: disposeBag)
        XCTAssert(alert.events.isEmpty)

        viewModel.didFail(with: .cancelledByUser)

        XCTAssertFalse(alert.events.isEmpty)
    }
}
