//
//  Created by Backbase R&D B.V. on 17/04/2020.
//

import XCTest
import Backbase
@testable import IdentityAuthenticationJourney

internal final class PasscodeTests: XCTestCase {
    func testAuthenticator() {
        let authenticator = Passcode.authenticator
        XCTAssertFalse(authenticator.isRegistered(forUsername: "invalid"))
    }
}
