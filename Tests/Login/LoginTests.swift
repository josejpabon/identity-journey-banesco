//
//  Created by Backbase R&D B.V. on 17/4/20.
//

import XCTest
import Resolver
@testable import IdentityAuthenticationJourney

internal final class LoginTests: XCTestCase {
    override func setUp() {
        super.setUp()
        Resolver.register { Authentication.Configuration() }
        Resolver.register { FalsyAuthUseCase() as AuthenticationUseCase }
    }

    func testScreenIsReturningCorrectViewController() {
        let navigationController = UINavigationController()
        let controller = Login.build(session: .none)(navigationController)
        XCTAssert(controller is LoginViewController)
    }
}
