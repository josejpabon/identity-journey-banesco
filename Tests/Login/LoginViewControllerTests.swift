//
//  Created by Backbase R&D B.V. on 3/25/20.
//

import XCTest
import UIKit
@testable import IdentityAuthenticationJourney

internal final class LoginViewControllerTests: XCTestCase {
    func testViewControllerHasSomeLayout() {
        let viewController = LoginViewController()
        XCTAssertFalse(viewController.view.constraints.isEmpty, "The view should have some constraints applied")
    }
}
