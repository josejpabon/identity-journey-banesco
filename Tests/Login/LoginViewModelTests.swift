//
//  Created by Backbase R&D B.V. on 03/04/2020.
//

import XCTest
import RxSwift
import RxTest
import Resolver
@testable import IdentityAuthenticationJourney

internal final class LoginViewModelTests: XCTestCase {
    override func setUp() {
        super.setUp()
        Resolver.register { Authentication.Configuration() }
        Resolver.register { FalsyAuthUseCase() as AuthenticationUseCase }
    }

    func testBinding() {
        var config: Authentication.Configuration = Resolver.resolve()

        let disposeBag = DisposeBag()
        let scheduler = TestScheduler(initialClock: 0)

        let viewModel = LoginViewModel(session: .none)

        let output = viewModel.bind(
            authenticateWithBiometrics: .just(()),
            authenticateWithPasscode: .just(()),
            canShowSessionAlert: .just(true)
        )

        let title = scheduler.createObserver(String.self)
        output.title.drive(title).disposed(by: disposeBag)
        XCTAssertEqual(title.events, [.next(0, config.login.strings.title()), .completed(0)])

        let biometricButtonTitle = scheduler.createObserver(String.self)
        output.biometricButtonTitle.drive(biometricButtonTitle).disposed(by: disposeBag)
        XCTAssertEqual(biometricButtonTitle.events, [.next(0, viewModel.biometricButtonTitle(for: .allowed)), .completed(0)])

        let subtitle = scheduler.createObserver(String.self)
        output.subtitle.drive(subtitle).disposed(by: disposeBag)
        XCTAssertEqual(subtitle.events, [.next(0, config.login.strings.subtitle(.none)()), .completed(0)])

        let passcodeButtonTitle = scheduler.createObserver(String.self)
        output.passcodeButtonTitle.drive(passcodeButtonTitle).disposed(by: disposeBag)
        XCTAssertEqual(passcodeButtonTitle.events, [.next(0, config.login.strings.passcodeButtonTitle()), .completed(0)])

        let biometricEnabled = scheduler.createObserver(Bool.self)
        output.biometricEnabled.drive(biometricEnabled).disposed(by: disposeBag)
        XCTAssertEqual(biometricEnabled.events, [.next(0, false), .completed(0)])

        let authenticateWithBiometricsAlert = scheduler.createObserver(UIAlertController.self)
        output.errorAlert.emit(to: authenticateWithBiometricsAlert).disposed(by: disposeBag)
        viewModel.startBiometricsFlow()
        XCTAssertFalse(authenticateWithBiometricsAlert.events.isEmpty)

        let authenticateWithPasscodeAlert = scheduler.createObserver(UIAlertController.self)
        output.errorAlert.emit(to: authenticateWithPasscodeAlert).disposed(by: disposeBag)
        viewModel.startPasscodeFlow()
        XCTAssertFalse(authenticateWithPasscodeAlert.events.isEmpty)
    }

    func testAlertController() {
        let config: Authentication.Configuration = Resolver.resolve()

        let viewModel = LoginViewModel(session: .none)
        let error = Authentication.Error.cancelledByUser
        let alert = viewModel.alertController(for: error)
        XCTAssertEqual(alert.title, config.strings.alertTitle(error)())
        XCTAssertEqual(alert.message, config.strings.alertMessage(error)())
        XCTAssertEqual(alert.preferredStyle, .alert)

        let dismiss = alert.actions.first
        XCTAssertEqual(dismiss?.title, config.strings.alertConfirmOption())
    }

    func testSessionExpiredAlertController() {
        let config: Authentication.Configuration = Resolver.resolve()

        let viewModel = LoginViewModel(session: .none)
        let alert = viewModel.sessionExpiredAlertController
        XCTAssertEqual(alert.title, config.strings.alertTitle(.sessionExpired)())
        XCTAssertEqual(alert.message, config.strings.alertMessage(.sessionExpired)())
        XCTAssertEqual(alert.preferredStyle, .alert)

        let dismiss = alert.actions.first
        XCTAssertEqual(dismiss?.title, config.strings.alertConfirmOption())
    }

    func testSubtitle() {
        var config: Authentication.Configuration = Resolver.resolve()

        let viewModel = LoginViewModel(session: .none)
        XCTAssertEqual(viewModel.subtitle(for: .touchID, forcePasscode: false), config.login.strings.subtitle(.touchID)())
        XCTAssertEqual(viewModel.subtitle(for: .faceID, forcePasscode: false), config.login.strings.subtitle(.faceID)())
        XCTAssertEqual(viewModel.subtitle(for: .lockedOut, forcePasscode: false), config.login.strings.subtitle(.lockedOut)())
        XCTAssertEqual(viewModel.subtitle(for: .none, forcePasscode: false), config.login.strings.subtitle(.none)())
    }

    func testBiometricButtonTitle() {
        var config: Authentication.Configuration = Resolver.resolve()

        let viewModel = LoginViewModel(session: .none)
        XCTAssertEqual(viewModel.biometricButtonTitle(for: .none), "")
        XCTAssertEqual(viewModel.biometricButtonTitle(for: .faceID), config.login.strings.biometricButtonTitle(.faceID)?())
        XCTAssertEqual(viewModel.biometricButtonTitle(for: .touchID), config.login.strings.biometricButtonTitle(.touchID)?())
        XCTAssertEqual(viewModel.biometricButtonTitle(for: .lockedOut), config.login.strings.biometricButtonTitle(.lockedOut)?())
    }
}
