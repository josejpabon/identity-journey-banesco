//
//  Created by Backbase R&D B.V. on 17/04/2020.
//

import XCTest
import Backbase
@testable import IdentityAuthenticationJourney

internal final class BackbaseRenderingTests: XCTestCase {
    func testRenderingViewControllerView() {
        let renderable = FakeRenderable()
        let viewController = RenderingViewController(withRenderable: renderable)
        viewController.loadView()
        XCTAssert(viewController.view is RenderableContainerView)
    }

    func testNavigationBarIsHiddenInWillAppear() {
        let renderable = FakeRenderable()
        let viewController = RenderingViewController(withRenderable: renderable)
        let navigationController = UINavigationController(rootViewController: viewController)
        viewController.loadView()
        viewController.viewWillAppear(false)
        XCTAssert(navigationController.isNavigationBarHidden)
    }

    func testNavigationBarIsHiddenInWillDisappear() {
        let renderable = FakeRenderable()
        let viewController = RenderingViewController(withRenderable: renderable)
        let navigationController = UINavigationController(rootViewController: viewController)

        navigationController.setNavigationBarHidden(true, animated: false)
        XCTAssert(navigationController.isNavigationBarHidden)

        viewController.loadView()
        viewController.viewWillDisappear(false)

        XCTAssert(navigationController.isNavigationBarHidden)
    }
}
