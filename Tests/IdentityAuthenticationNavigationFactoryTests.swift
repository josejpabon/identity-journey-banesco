//
//  Created by Backbase R&D B.V. on 3/28/20.
//

import XCTest
import BackbaseIdentity
@testable import IdentityAuthenticationJourney

private final class Presenter: BBIDAuthenticatorPresenter {
    override class func isAuthenticatorShowEvent(_ notification: Notification) -> Bool {
        return notification.name.rawValue == "show"
    }

    override class func isAuthenticatorDismissEvent(_ notification: Notification) -> Bool {
        return notification.name.rawValue == "dismiss"
    }
}

// swiftlint:disable:next type_name
internal final class IdentityAuthenticationNavigationFactoryTests: XCTestCase {
    private typealias Client = BBIDAuthClient
    private typealias Factory = IdentityAuthenticationNavigationFactory
    private typealias Target = Factory.Target

    func testTargetCases() {
        XCTAssertEqual(Target.allCases.count, 3)
        XCTAssertEqual(Target.biometric.rawValue, "BBIDBiometricAuthenticator")
        XCTAssertEqual(Target.passcode.rawValue, "BBIDPasscodeAuthenticator")
        XCTAssertEqual(Target.inputRequired.rawValue, "BBIDInputRequiredAuthenticator")

        let name = Notification.Name(rawValue: "navigation.request")

        let biometricTarget = ["target": "BBIDBiometricAuthenticator"]
        let biometricNotification = Notification(name: name, object: nil, userInfo: biometricTarget)
        let biometricEvent = Target(from: biometricNotification)
        XCTAssertNotNil(biometricEvent)
        XCTAssertEqual(biometricEvent, .biometric)

        let passcodeTarget = ["target": "BBIDPasscodeAuthenticator"]
        let passcodeNotification = Notification(name: name, object: nil, userInfo: passcodeTarget)
        let passcodeEvent = Target(from: passcodeNotification)
        XCTAssertNotNil(passcodeEvent)
        XCTAssertEqual(passcodeEvent, .passcode)

        let invalidNotification = Notification(name: name, object: nil, userInfo: nil)
        XCTAssertNil(Target(from: invalidNotification))
    }

    func testDismissCurrentNavigation() {
        let client = Client(clientSecret: "")
        let factory = Factory(client: client)

        let notification = Notification(name: .init("dismiss"), object: nil, userInfo: nil)
        let navigation = factory.navigation(for: notification, using: Presenter.self)
        guard let aNavigation = navigation else {
            XCTFail("Navigation should not be nil")
            return
        }
        switch aNavigation {
        case .dismissCurrent:
            break
        default:
            XCTFail("Event should be a dismiss event")
        }
    }

    func testPasscodePresentEvent () {
        let authenticator = BBIDPasscodeAuthenticator()
        let client = Client(clientSecret: "")
        try? client.add(authenticator)

        let factory = Factory(client: client)

        let notification = Notification(name: .init("show"), object: nil, userInfo: ["target": "BBIDPasscodeAuthenticator"])
        let navigation = factory.navigation(for: notification, using: Presenter.self)
        guard let aNavigation = navigation else {
            XCTFail("Navigation should not be nil")
            return
        }
        switch aNavigation {
        case .present(let screen):
            guard let item = client.authenticatorRenderable("BBIDPasscodeAuthenticator") else { return }
            let nav = UINavigationController()
            XCTAssertEqual(screen(nav), Passcode.build(item: item)(nav))
        default:
            XCTFail("Event should be a dismiss event")
        }
    }

    func testBiometricPresentEvent () {
        let authenticator = BBIDBiometricAuthenticator()
        let client = Client(clientSecret: "")
        try? client.add(authenticator)

        let factory = Factory(client: client)

        let notification = Notification(name: .init("show"), object: nil, userInfo: ["target": "BBIDBiometricAuthenticator"])
        let navigation = factory.navigation(for: notification, using: Presenter.self)
        guard let aNavigation = navigation else {
            XCTFail("Navigation should not be nil")
            return
        }
        switch aNavigation {
        case .present(let screen):
            guard let item = client.authenticatorRenderable("BBIDBiometricAuthenticator") else { return }
            let nav = UINavigationController()
            XCTAssertEqual(screen(nav), Biometric.build(item: item)(nav))
        default:
            XCTFail("Event should be a dismiss event")
        }
    }

    func testInvalidTargetEvent() {
        let client = Client(clientSecret: "")
        let factory = Factory(client: client)

        let notification = Notification(name: .init("show"), object: nil, userInfo: nil)
        let navigation = factory.navigation(for: notification, using: Presenter.self)
        XCTAssertNil(navigation)
    }

    func testInvalidRenderableEvent() {
        let client = Client(clientSecret: "")
        let factory = Factory(client: client)

        let notification = Notification(name: .init("show"), object: nil, userInfo: ["target": "BBIDBiometricAuthenticator"])
        let navigation = factory.navigation(for: notification, using: Presenter.self)
        XCTAssertNil(navigation)
    }
}
