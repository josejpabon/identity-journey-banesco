//
//  Created by Backbase R&D B.V. on 17/04/2020.
//

import XCTest
import BackbaseIdentity
@testable import IdentityAuthenticationJourney

private struct DelegateTester {
    func setSessionState(_ sessionState: SessionState, using delegate: AuthClientDelegate) {
        delegate.sessionState(sessionState)
    }
}

internal final class AuthClientDelegateProxyTests: XCTestCase {
    func testProxy() {
        var session: SessionState?
        let proxy = AuthClientDelegateProxy { proxySession in
            session = proxySession
        }

        let tester = DelegateTester()
        tester.setSessionState(.valid, using: proxy)

        XCTAssertEqual(session, .valid)
    }
}
