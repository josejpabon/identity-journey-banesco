//
//  Created by Backbase R&D B.V. on 17/04/2020.
//

import XCTest
import BackbaseIdentity
@testable import IdentityAuthenticationJourney

private struct DelegateTester {
    func setSuccess(using delegate: BBIDPasscodeChangeDelegate) {
        delegate.passcodeChangeDidSucceed()
    }

    func setError(_ error: Error, using delegate: BBIDPasscodeChangeDelegate) {
        delegate.passcodeChangeDidFail(with: error)
    }
}

internal final class BBIDPasscodeChangeDelegateProxyTests: XCTestCase {
    private enum Error: Swift.Error {
        case error
    }

    func testProxy() {
        var success = false
        var error: Swift.Error?
        let proxy = BBIDPasscodeChangeDelegateProxy { result in
            switch result {
            case .success:
                success = true
            case .failure(let proxyError):
                error = proxyError
            }
        }

        let tester = DelegateTester()
        tester.setSuccess(using: proxy)
        tester.setError(Error.error, using: proxy)

        XCTAssert(success)
        XCTAssertEqual(error?.localizedDescription, Error.error.localizedDescription)
    }
}
