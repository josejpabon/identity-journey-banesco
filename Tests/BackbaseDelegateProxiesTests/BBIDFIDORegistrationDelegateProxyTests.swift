//
//  Created by Backbase R&D B.V. on 17/04/2020.
//

import XCTest
import BackbaseIdentity
@testable import IdentityAuthenticationJourney

private struct DelegateTester {
    func setSuccess(using delegate: BBIDFIDORegistrationDelegate) {
        delegate.uafRegistrationDidSucceed()
    }

    func setError(_ error: Error, using delegate: BBIDFIDORegistrationDelegate) {
        delegate.uafRegistrationDidFail(with: error)
    }
}

internal final class BBIDFIDORegistrationDelegateProxyTests: XCTestCase {
    private enum Error: Swift.Error {
        case error
    }

    func testProxy() {
        var success = false
        var error: Swift.Error?
        let proxy = BBIDFIDORegistrationDelegateProxy { result in
            switch result {
            case .success:
                success = true
            case .failure(let proxyError):
                error = proxyError
            }
        }

        let tester = DelegateTester()
        tester.setSuccess(using: proxy)
        tester.setError(Error.error, using: proxy)

        XCTAssert(success)
        XCTAssertEqual(error?.localizedDescription, Error.error.localizedDescription)
    }
}
