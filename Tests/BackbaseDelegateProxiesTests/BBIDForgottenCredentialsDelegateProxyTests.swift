//
//  Created by Backbase R&D B.V. on 17/04/2020.
//

import XCTest
import BackbaseIdentity
@testable import IdentityAuthenticationJourney

private struct DelegateTester {
    func setUsernameData(_ data: Data, using delegate: BBIDForgottenCredentialsDelegate) {
        delegate.forgotUsernameDidSucceed(data)
    }

    func setUsernameError(_ error: Error, using delegate: BBIDForgottenCredentialsDelegate) {
        delegate.forgotUsernameDidFailWithError(error)
    }

    func setPasswordData(_ data: Data, using delegate: BBIDForgottenCredentialsDelegate) {
        delegate.forgotPasswordDidSucceed(data)
    }

    func setPasswordError(_ error: Error, using delegate: BBIDForgottenCredentialsDelegate) {
        delegate.forgotPasswordDidFailWithError(error)
    }
}

// swiftlint:disable:next type_name
internal final class BBIDForgottenCredentialsDelegateProxyTests: XCTestCase {
    private enum Error: Swift.Error {
        case error1
        case error2
    }

    func testProxy() {
        var usernameData: Data?
        var usernameError: Swift.Error?

        var passwordData: Data?
        var passwordError: Swift.Error?

        let proxy = BBIDForgottenCredentialsDelegateProxy(usernameHandler: { result in
            switch result {
            case .success(let proxyData):
                usernameData = proxyData
            case .failure(let proxyError):
                usernameError = proxyError
            }
        }, passwordHandler: { result in
            switch result {
            case .success(let proxyData):
                passwordData = proxyData
            case .failure(let proxyError):
                passwordError = proxyError
            }
        })

        let tester = DelegateTester()

        tester.setUsernameData(Data(), using: proxy)
        tester.setUsernameError(Error.error1, using: proxy)

        XCTAssertEqual(usernameData, Data())
        XCTAssertEqual(usernameError?.localizedDescription, Error.error1.localizedDescription)

        tester.setPasswordData(Data(), using: proxy)
        tester.setPasswordError(Error.error2, using: proxy)

        XCTAssertEqual(passwordData, Data())
        XCTAssertEqual(passwordError?.localizedDescription, Error.error2.localizedDescription)
    }
}
