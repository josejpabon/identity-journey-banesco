//
//  Created by Backbase R&D B.V. on 17/04/2020.
//

import XCTest
import BackbaseIdentity
@testable import IdentityAuthenticationJourney

private struct DelegateTester {
    func setHeaders(_ headers: [String: String], using delegate: PasswordAuthClientDelegate) {
        delegate.authenticationDidSucceed(with: headers)
    }

    func setError(_ error: Error, using delegate: PasswordAuthClientDelegate) {
        delegate.authenticationDidFail(with: error)
    }
}

internal final class PasswordAuthClientDelegateProxyTests: XCTestCase {
    private enum Error: Swift.Error {
        case error
    }

    func testProxy() {
        var headers: [String: String]?
        var error: Swift.Error?
        let proxy = PasswordAuthClientDelegateProxy { result in
            switch result {
            case .success(let proxyHeaders):
                headers = proxyHeaders
            case .failure(let proxyError):
                error = proxyError
            }
        }

        let tester = DelegateTester()
        tester.setHeaders(["key": "value"], using: proxy)
        tester.setError(Error.error, using: proxy)

        XCTAssertEqual(headers, ["key": "value"])
        XCTAssertEqual(error?.localizedDescription, Error.error.localizedDescription)
    }
}
