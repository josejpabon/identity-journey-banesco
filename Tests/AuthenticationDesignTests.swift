//
//  Created by Backbase R&D B.V. on 28/10/2020.
//

import XCTest
@testable import IdentityAuthenticationJourney

internal final class AuthenticationDesignTests: XCTestCase {
    func testBackgroundView() throws {
        let testConfig = Authentication.Configuration()
        let testBackgroundView = Authentication.Design.BackgroundView()
        
        // The initial background color
        testBackgroundView.background = .solid(.red)
        XCTAssertEqual(testBackgroundView.backgroundColor, .red)
        
        // The default background view style
        testConfig.design.styles.background(testBackgroundView)
        XCTAssertEqual(testBackgroundView.backgroundColor, testConfig.design.colors.foundation.default)

        // A custom background style
        testConfig.design.styles.background = {
            $0.backgroundColor = .yellow
        }
        testConfig.design.styles.background(testBackgroundView)
        XCTAssertEqual(testBackgroundView.backgroundColor, .yellow)
    }
}
