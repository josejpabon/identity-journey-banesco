//
//  Created by Backbase R&D B.V. on 01/09/2020.
//

import XCTest
import Resolver
@testable import IdentityAuthenticationJourney

internal final class SetupCompleteTests: XCTestCase {
    override func setUp() {
        super.setUp()
        Resolver.register { Authentication.Configuration() }
        Resolver.register { FalsyAuthUseCase() as AuthenticationUseCase }
    }

    func testScreenIsReturningCorrectViewController() {
        let navigationController = UINavigationController()
        let user = User(username: "test", password: "test")
        let controller = SetupComplete.build(navigationController: navigationController)(user)
        XCTAssert(controller is SetupCompleteViewController)
    }
}
