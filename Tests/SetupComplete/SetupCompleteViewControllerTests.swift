//
//  Created by Backbase R&D B.V. on 01/09/2020.
//

import XCTest
import UIKit
@testable import IdentityAuthenticationJourney

internal final class SetupCompleteViewControllerTests: XCTestCase {
    func testViewControllerHasSomeLayout() {
        let viewController = SetupCompleteViewController()
        XCTAssertFalse(viewController.view.constraints.isEmpty, "The view should have some constraints applied")
    }
}
