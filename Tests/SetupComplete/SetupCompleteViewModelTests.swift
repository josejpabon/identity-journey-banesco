//
//  Created by Backbase R&D B.V. on 01/09/2020.
//

import XCTest
import RxSwift
import RxTest
import Resolver
@testable import IdentityAuthenticationJourney

internal final class SetupCompleteViewModelTests: XCTestCase {
    override func setUp() {
        super.setUp()
        Resolver.register { Authentication.Configuration() }
        Resolver.register { FalsyAuthUseCase() as AuthenticationUseCase }
    }

    func testBinding() {
        var config: Authentication.Configuration = Resolver.resolve()
        let user = User(username: "test", password: "test")

        let disposeBag = DisposeBag()
        let scheduler = TestScheduler(initialClock: 0)

        let viewModel = SetupCompleteViewModel(user: user)

        let output = viewModel.bind(cancel: .just(()), dismiss: .just(()))

        let cancelButtonIcon = scheduler.createObserver(UIImage?.self)
        output.cancelButtonIcon.drive(cancelButtonIcon).disposed(by: disposeBag)
        XCTAssertEqual(cancelButtonIcon.events, [.next(0, config.setupComplete.cancelButtonIcon), .completed(0)])

        let backgroundImage = scheduler.createObserver(UIImage?.self)
        output.backgroundImage.drive(backgroundImage).disposed(by: disposeBag)
        XCTAssertEqual(backgroundImage.events, [.next(0, config.setupComplete.backgroundImage), .completed(0)])

        let image = scheduler.createObserver(UIImage?.self)
        output.image.drive(image).disposed(by: disposeBag)
        XCTAssertEqual(image.events, [.next(0, config.setupComplete.image), .completed(0)])

        let title = scheduler.createObserver(String.self)
        output.title.drive(title).disposed(by: disposeBag)
        XCTAssertEqual(title.events, [.next(0, config.setupComplete.strings.title()), .completed(0)])

        let subtitle = scheduler.createObserver(String.self)
        output.subtitle.drive(subtitle).disposed(by: disposeBag)
        XCTAssertEqual(subtitle.events, [.next(0, config.setupComplete.strings.subtitle()), .completed(0)])

        let dismissButtonTitle = scheduler.createObserver(String.self)
        output.dismissButtonTitle.drive(dismissButtonTitle).disposed(by: disposeBag)
        XCTAssertEqual(dismissButtonTitle.events, [.next(0, config.setupComplete.strings.dismissButtonTitle()), .completed(0)])
    }

    func testResumeAuthentication() {
        let disposeBag = DisposeBag()
        let scheduler = TestScheduler(initialClock: 0)

        let user = User(username: "test", password: "test")
        let viewModel = SetupCompleteViewModel(user: user)

        let output = viewModel.bind(cancel: .just(()), dismiss: .just(()))

        let isLoading = scheduler.createObserver(Bool.self)
        output.isLoading.drive(isLoading).disposed(by: disposeBag)
        XCTAssertEqual(isLoading.events, [.next(0, false)])

        let errorAlert = scheduler.createObserver(UIAlertController.self)
        output.errorAlert.emit(to: errorAlert).disposed(by: disposeBag)
        XCTAssert(errorAlert.events.isEmpty)

        viewModel.resumeAuthentication(for: user)
        XCTAssertEqual(isLoading.events, [.next(0, false), .next(0, true), .next(0, false)])
        XCTAssertFalse(errorAlert.events.isEmpty)
    }

    func testAlertController() {
        let config: Authentication.Configuration = Resolver.resolve()

        let user = User(username: "test", password: "test")
        let viewModel = SetupCompleteViewModel(user: user)
        let error = Authentication.Error.cancelledByUser
        let alert = viewModel.alertController(for: error)
        XCTAssertEqual(alert.title, config.strings.alertTitle(error)())
        XCTAssertEqual(alert.message, config.strings.alertMessage(error)())
        XCTAssertEqual(alert.preferredStyle, .alert)

        let dismiss = alert.actions.first
        XCTAssertEqual(dismiss?.title, config.strings.alertConfirmOption())
    }
}
