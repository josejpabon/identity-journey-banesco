//
//  Created by Backbase R&D B.V. on 23/07/2020.
//

import XCTest
import BackbaseIdentity
import LocalAuthentication
@testable import IdentityAuthenticationJourney

internal final class AuthenticationErrorTests: XCTestCase {
    func testInitInBiometric() {
        let error1 = Authentication.Error(biometric: Authentication.Error.cancelledByUser)
        XCTAssertEqual(error1, .cancelledByUser)

        let error2 = Authentication.Error(biometric: Authentication.Error.invalidCredentials, biometryType: .denied)
        XCTAssertEqual(error2, .biometricUsageDenied)

        let error3 = Authentication.Error(biometric: Authentication.Error.invalidCredentials, biometryType: .lockedOut)
        XCTAssertEqual(error3, .biometricLockout)

        let error4 = Authentication.Error(biometric: Authentication.Error.invalidCredentials, biometryType: .faceID)
        XCTAssertEqual(error4, .invalidCredentials)
    }

    func testInitInPasscode() {
        let error1 = Authentication.Error(passcode: Authentication.Error.cancelledByUser)
        XCTAssertEqual(error1, .cancelledByUser)

        let error2 = Authentication.Error(passcode: Authentication.Error.invalidCredentials)
        XCTAssertEqual(error2, .invalidPasscode)
    }

    func testInitFromSDKErrors() {
        let error1 = Authentication.Error(sdk: Authentication.Error.cancelledByUser)
        XCTAssertEqual(error1, .cancelledByUser)

        let error2 = NSError(domain: "", code: NSURLErrorNotConnectedToInternet, userInfo: nil)
        XCTAssertEqual(Authentication.Error(sdk: error2), .notConnected)

        let error3 = NSError(domain: "", code: 401, userInfo: nil)
        XCTAssertEqual(Authentication.Error(sdk: error3), .invalidCredentials)

        let error4 = NSError(domain: "", code: 403, userInfo: nil)
        XCTAssertEqual(Authentication.Error(sdk: error4), .invalidCredentials)

        let error5 = NSError(domain: "", code: 1012, userInfo: nil)
        XCTAssertEqual(Authentication.Error(sdk: error5), .invalidCredentials)

        let error6 = NSError(domain: "", code: BBIDErrors.deviceKeyNotFound.rawValue, userInfo: nil)
        XCTAssertEqual(Authentication.Error(sdk: error6), .incompleteEnrollment)

        let error7 = NSError(domain: "", code: BBIDErrors.invalidDeviceId.rawValue, userInfo: nil)
        XCTAssertEqual(Authentication.Error(sdk: error7), .incompleteEnrollment)

        let error8 = NSError(domain: "", code: 1007, userInfo: nil)
        XCTAssertEqual(Authentication.Error(sdk: error8), .incompleteEnrollment)

        let error9 = NSError(domain: "", code: 1020, userInfo: nil)
        XCTAssertEqual(Authentication.Error(sdk: error9), .accountLocked)

        let error10 = NSError(domain: "", code: BBIDErrors.userCancelledAuthenticator.rawValue, userInfo: nil)
        XCTAssertEqual(Authentication.Error(sdk: error10), .cancelledByUser)

        let error11 = NSError(domain: "", code: BBIDErrors.userDeniedUsage.rawValue, userInfo: nil)
        XCTAssertEqual(Authentication.Error(sdk: error11), .cancelledByUser)

        let error12 = NSError(domain: "", code: LAError.userCancel.rawValue, userInfo: nil)
        XCTAssertEqual(Authentication.Error(sdk: error12), .cancelledByUser)

        let error13 = NSError(domain: "", code: BBIDErrors.passcodeMismatch.rawValue, userInfo: nil)
        XCTAssertEqual(Authentication.Error(sdk: error13), .passcodeMismatch)
    }
}
