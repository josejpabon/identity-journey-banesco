///
//  Created by Backbase R&D B.V. on 06/11/2020.
//

import XCTest
import BackbaseIdentity
@testable import IdentityAuthenticationJourney

internal final class AuthenticatorsTests: XCTestCase {
    func testRegisterAll() {
        let auth = Authenticators()
        let client = BBIDAuthClient()

        XCTAssertNil(client.deviceAuthenticator())
        XCTAssert(client.fidoAuthenticators().isEmpty)

        auth.registerAll(using: client)
        XCTAssertNotNil(client.deviceAuthenticator())
        XCTAssertEqual(client.fidoAuthenticators().count, 2)
    }

    func testPrepareForBiometric() {
        let auth = Authenticators()
        let client = BBIDAuthClient()

        auth.prepareForBiometric(in: client)
        XCTAssertEqual(client.fidoAuthenticators(), [auth.biometric])
    }

    func testPrepareForPasscode() {
        let auth = Authenticators()
        let client = BBIDAuthClient()

        auth.prepareForPasscode(in: client)
        XCTAssertEqual(client.fidoAuthenticators(), [auth.passcode])
    }
}
