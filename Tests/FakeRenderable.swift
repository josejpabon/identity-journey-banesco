//
//  Created by Backbase R&D B.V. on 03/04/2020.
//

import Backbase

internal final class FakeRenderable: NSObject, Renderable {
    func preference(forKey key: String) -> String? {
        return nil
    }

    func allPreferences() -> [String: String] {
        return [:]
    }

    func setPreference(_ key: String, withValue value: String?) {}

    func itemType() -> BBItemType {
        return .page
    }

    func itemName() -> String? {
        return "test"
    }

    func itemId() -> String {
        return "test"
    }

    func itemChildren() -> [Renderable] {
        return []
    }

    func itemParent() -> Renderable? {
        return nil
    }

    func itemIcon(byName name: String) -> IconPack? {
        return nil
    }

    func itemIcon(by index: UInt) -> IconPack? {
        return nil
    }

    func itemIcons() -> [IconPack] {
        return []
    }
}
