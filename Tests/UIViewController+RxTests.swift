//
//  UIViewController+RxTests.swift
//  IdentityAuthenticationJourneyTests
//
//  Created by Christopher Mash on 03/11/2020.
//

import XCTest
import RxSwift
import RxTest
import Resolver
@testable import IdentityAuthenticationJourney

class UIViewController_RxTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testViewDidLoad() throws {
        let vc = UIViewController()
        var eventGenerated = false
        let _ = vc.rx.viewDidLoad.asObservable().subscribe { event in
            eventGenerated = true
        }
        // force view to load
        let _ = vc.view
        XCTAssertTrue(eventGenerated)
    }
    
    func testViewWillAppear() throws {
        let vc = UIViewController()
        var eventElement: Bool?
        let _ = vc.rx.viewWillAppear.asObservable().subscribe { event in
            eventElement = event.element
        }
        vc.viewWillAppear(true)
        XCTAssertEqual(eventElement, true)
    }
    
    func testViewDidAppear() throws {
        let vc = UIViewController()
        var eventElement: Bool?
        let _ = vc.rx.viewDidAppear.asObservable().subscribe { event in
            eventElement = event.element
        }
        vc.viewDidAppear(true)
        XCTAssertEqual(eventElement, true)
    }
    
    func testViewWillDisppear() throws {
        let vc = UIViewController()
        var eventElement: Bool?
        let _ = vc.rx.viewWillDisappear.asObservable().subscribe { event in
            eventElement = event.element
        }
        vc.viewWillDisappear(true)
        XCTAssertEqual(eventElement, true)
    }
    
    func testViewDidDisappear() throws {
        let vc = UIViewController()
        var eventElement: Bool?
        let _ = vc.rx.viewDidDisappear.asObservable().subscribe { event in
            eventElement = event.element
        }
        vc.viewDidDisappear(true)
        XCTAssertEqual(eventElement, true)
    }
    
    func testViewWillLayoutSubviews() throws {
        let vc = UIViewController()
        var eventGenerated = false
        let _ = vc.rx.viewWillLayoutSubviews.asObservable().subscribe { event in
            eventGenerated = true
        }
        // force view to layout
        vc.view.layoutIfNeeded()
        XCTAssertTrue(eventGenerated)
    }
    
    func testViewDidLayoutSubviews() throws {
        let vc = UIViewController()
        var eventGenerated = false
        let _ = vc.rx.viewDidLayoutSubviews.asObservable().subscribe { event in
            eventGenerated = true
        }
        // force view to layout
        vc.view.layoutIfNeeded()
        XCTAssertTrue(eventGenerated)
    }
    
    func testDidReceiveMemoryWarning() throws {
        let vc = UIViewController()
        var eventGenerated = false
        let _ = vc.rx.didReceiveMemoryWarning.asObservable().subscribe { event in
            eventGenerated = true
        }
        vc.didReceiveMemoryWarning()
        XCTAssertTrue(eventGenerated)
    }
    
    func testWillMoveToParentViewController() throws {
        let vc = UIViewController()
        var vcMovedTo: UIViewController??
        let _ = vc.rx.willMoveToParentViewController.asObservable().subscribe { event in
            vcMovedTo = event.element
        }
        let vc2 = UIViewController()
        vc.willMove(toParent: vc2)
        XCTAssertEqual(vcMovedTo, vc2)
    }
    
    func testDidMoveToParentViewController() throws {
        let vc = UIViewController()
        var vcMovedTo: UIViewController??
        let _ = vc.rx.didMoveToParentViewController.asObservable().subscribe { event in
            vcMovedTo = event.element
        }
        let vc2 = UIViewController()
        vc.didMove(toParent: vc2)
        XCTAssertEqual(vcMovedTo, vc2)
    }
    
    func testIsVisible() throws {
        let vc = UIViewController()
        var eventElement: Bool?
        let _ = vc.rx.isVisible.subscribe { event in
            eventElement = event.element
        }
        vc.viewDidAppear(true)
        XCTAssertEqual(eventElement, true)
        vc.viewWillDisappear(true)
        XCTAssertEqual(eventElement, false)
    }
    
    func testIsDismissing() throws {
        let vc = UIViewController()
        var eventElement: Bool?
        let _ = vc.rx.isDismissing.subscribe { event in
            eventElement = event.element
        }
        vc.dismiss(animated: true, completion: nil)
        XCTAssertEqual(eventElement, true)
    }

}
