//
// Copyright © 2020 Backbase R&D B.V. All rights reserved.
//

import Foundation
import XCTest

extension XCUIElement {
    func clearTextField() {
        guard let stringValue = value as? String else {
            XCTFail("Tried to clear and enter text into a non string value")
            return
        }
        
        let deleteString = String(repeating: XCUIKeyboardKey.delete.rawValue, count: stringValue.count)
        tap()
        typeText(deleteString)
    }
    
    func clearAndEnterText(text: String) {
        clearTextField()
        typeText(text)
    }
    
    func waitForElementToAppear() {
        _ = waitForExistence(timeout: 5)
    }
    
    func scrollDownUntilElementDisplayed() {
        let MAX_SCROLLS = 5
        var count = 0
        while isHittable == false && count < MAX_SCROLLS {
            BaseTestCase.app.swipeUp()
            count += 1
        }
    }
    
    func scrollDownUntilElementExists() {
        let MAX_SCROLLS = 5
        var count = 0
        while exists == false && count < MAX_SCROLLS {
            BaseTestCase.app.swipeUp()
            count += 1
        }
    }
    
    func scrollUpUntilElementDisplayed() {
        let MAX_SCROLLS = 10
        var count = 0
        while isHittable == false && count < MAX_SCROLLS {
            BaseTestCase.app.swipeDown()
            count += 1
        }
    }
    
    func hasFocus() -> Bool {
        let hasKeyboardFocus = (self.value(forKey: "hasKeyboardFocus") as? Bool) ?? false
        return hasKeyboardFocus
    }
    
    func labelContains(text: String) -> Bool {
        let predicate = NSPredicate(format: "label CONTAINS %@", text)
        return staticTexts.matching(predicate).firstMatch.exists
    }
}
