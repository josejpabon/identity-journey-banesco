//
//  Created by Backbase R&D B.V. on 05/03/2020.
//

import XCTest

internal struct LoginScreen {
    static let titleLabel = BaseTestCase.app.staticTexts.matching(identifier: "login.titleLabel").firstMatch
    static let subtitleLabel = BaseTestCase.app.staticTexts.matching(identifier: "login.subtitleLabel").firstMatch
    static let biometricsButton = BaseTestCase.app.buttons.matching(identifier: "login.biometricButton").firstMatch
    static let passcodeButton = BaseTestCase.app.buttons.matching(identifier: "login.passcodeButton").firstMatch
    static let retryAlertDialogButton = BaseTestCase.app.alerts.firstMatch.buttons["Retry"].firstMatch
    static let retryAlertDialog = BaseTestCase.app.alerts.firstMatch.staticTexts["Incorrect passcode"].firstMatch
    
    static func loginWithBiometrics() {
        biometricsButton.waitForElementToAppear()
        biometricsButton.tap()
    }

    static func loginWithPasscode() {
        passcodeButton.waitForElementToAppear()
        passcodeButton.tap()
    }

    static func retryIncorrectPasscode() {
        retryAlertDialogButton.waitForElementToAppear()
        retryAlertDialogButton.tap()
    }
}
