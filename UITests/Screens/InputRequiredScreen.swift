//
//  Copyright © 2020 Backbase R&D B.V. All rights reserved.
//

import XCTest

internal struct InputRequiredScreen {
    static let closeButton = BaseTestCase.app.buttons.matching(identifier: "inputRequired.cancelButton").firstMatch
    static let confirmEmailButton = BaseTestCase.app.buttons.matching(identifier: "inputRequired.submitButton").firstMatch
    static let emailInput = BaseTestCase.app.textFields.matching(identifier: "inputRequired.email.textField").firstMatch
    static let inputRequiredTitle = BaseTestCase.app.staticTexts.matching(identifier: "inputRequired.email.textField").firstMatch
    static let inputRequiredSubTitle = BaseTestCase.app.staticTexts.matching(identifier: "inputRequired.email.textField").firstMatch
    
    static let planeImage = BaseTestCase.app.images.matching(identifier: "EmailSent").firstMatch
    static let emailSentTitle = BaseTestCase.app.staticTexts.matching(identifier: "emailSent.titleLabel").firstMatch.label
    static let emailSentSubTitle = BaseTestCase.app.staticTexts.matching(identifier: "emailSent.subtitleLabel").firstMatch.label
    static let emailSentCloseButton = BaseTestCase.app.buttons.matching(identifier: "emailSent.cancelButton").firstMatch
    static let doneButton = BaseTestCase.app.buttons.matching(identifier: "emailSent.dismissButton").firstMatch
    
    static let invalidEmailAlert = BaseTestCase.app.alerts["Invalid e-mail"].firstMatch
    
    
    static func sendEmailConfirmation() {
        confirmEmailButton.tap()
    }
    
    static func cancelEmailConfirmation() {
        closeButton.tap()
    }
    
    static func enterEmail(email: String) {
        emailInput.tap()
        emailInput.typeText(email)
    }
    
    static func forgetUsernameDone() {
        doneButton.tap()
    }
}
