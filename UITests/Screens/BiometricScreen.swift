//
//  Copyright © 2020 Backbase R&D B.V. All rights reserved.
//

import XCTest

internal struct BiometricScreen {
    static let allowButton = BaseTestCase.app.buttons.matching(identifier: "biometricRegistration.allowButton").firstMatch
    static let denyButton = BaseTestCase.app.buttons.matching(identifier: "biometricRegistration.denyButton").firstMatch
    static let cancelButton = BaseTestCase.app.buttons.matching(identifier: "biometricRegistration.cancelButton").firstMatch
    
    static func allowBiometrics() {
        allowButton.waitForElementToAppear()
        allowButton.tap()
    }
    
    static func setBiometricsLater() {
        denyButton.waitForElementToAppear()
        denyButton.tap()
    }
}
