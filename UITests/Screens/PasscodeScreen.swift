//
//  Created by Backbase R&D B.V. on 05/03/2020.
//

import XCTest

internal struct PasscodeScreen {
    static let cancelButton = BaseTestCase.app.buttons.matching(identifier: "passcode.cancelButton").firstMatch
    
    static func dismiss() {
        cancelButton.waitForElementToAppear()
        cancelButton.tap()
    }
}
