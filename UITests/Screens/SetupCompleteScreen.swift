//
//  Created by Backbase R&D B.V. on 05/03/2020.
//

import XCTest

internal struct SetupCompleteScreen {
    static let titleLabel =
        BaseTestCase.app.staticTexts.matching(identifier: "setupComplete.titleLabel").firstMatch.label
    static let subtitleLabel =
        BaseTestCase.app.staticTexts.matching(identifier:"setupComplete.subtitleLabel").firstMatch.label
    static let getStartedButton =
        BaseTestCase.app.buttons.matching(identifier: "setupComplete.dismissButton").firstMatch
    static let cancelButton =
        BaseTestCase.app.buttons.matching(identifier: "setupComplete.cancelButton").firstMatch
    
    static func getStartedWithApp() {
        getStartedButton.tap()
    }
    
    static func dismissGetStartedScreenByCancelButton(){
        cancelButton.tap()
    }
}
