//
//  Created by Backbase R&D B.V. on 05/03/2020.
//

import XCTest

internal struct AuthenticatedScreen {
    // Settings menu in test app to enable settings hence strings are not prt of config
    static let endSessionButton = BaseTestCase.app.buttons["End Session"].firstMatch
    static let logOutButton = BaseTestCase.app.buttons["Log out"].firstMatch


    static func endSession() {
        endSessionButton.tap()
    }

    static func logOut() {
        logOutButton.tap()
    }
}
