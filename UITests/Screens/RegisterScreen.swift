//
//  Created by Backbase R&D B.V. on 05/03/2020.
//

import XCTest

internal struct RegisterScreen {
    static let titleLabel = BaseTestCase.app.staticTexts.matching(identifier: "register.titleLabel").firstMatch
    static let subtitleLabel = BaseTestCase.app.staticTexts.matching(identifier: "register.subtitleLabel").firstMatch

    static let usernameInput = BaseTestCase.app.textFields.matching(identifier: "register.usernameInput.textField").firstMatch
    static let usernamePlaceholder = BaseTestCase.app.staticTexts.matching(identifier: "register.usernameInput.placeholderLabel").firstMatch
    static let usernameInputErrorMessage = BaseTestCase.app.staticTexts.matching(identifier: "register.usernameInput.errorLabel").firstMatch

    static let passwordInput = BaseTestCase.app.secureTextFields.matching(identifier: "register.passwordInput.textField").firstMatch
    static let passwordPlaceholder = BaseTestCase.app.staticTexts.matching(identifier: "register.passwordInput.placeholderLabel").firstMatch
    static let passwordInputErrorMessage = BaseTestCase.app.staticTexts.matching(identifier: "register.passwordInput.errorLabel").firstMatch

    static let loginButton = BaseTestCase.app.buttons.matching(identifier: "register.loginButton").firstMatch
    
    static let forgetUsernameButton = BaseTestCase.app.buttons.matching(identifier: "register.forgotUsernameButton").firstMatch
    
    

    static let allImages = BaseTestCase.app.images.allElementsBoundByIndex
    static let allLabels = BaseTestCase.app.staticTexts.allElementsBoundByIndex
    static let allTextFields = BaseTestCase.app.textFields.allElementsBoundByIndex
    static let allSecureTextFields = BaseTestCase.app.secureTextFields.allElementsBoundByIndex
    static let tempBlockedAlert = BaseTestCase.app.alerts["You’re temporarily blocked"].firstMatch
    
    static func loginWithCredentials() {
        loginButton.tap()
    }

    static func dismissBlockedAlert() {
        BaseTestCase.app.alerts.buttons.firstMatch.tap()
    }
    
    static func gotoForgetUsername() {
        forgetUsernameButton.tap()
    }
}
