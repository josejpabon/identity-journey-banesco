//
//  Copyright © 2020 Backbase R&D B.V. All rights reserved.
//

import XCTest

extension BaseTestCase {
    func enterPasscode(_ passcode: String) {
        BaseTestCase.app.keyboards.firstMatch.waitForElementToAppear()
        passcode.map { String($0) }.forEach { tapKeys(keyToPress: $0) }
    }
    
    func tapKeys(keyToPress: String) {
        BaseTestCase.app.keys[keyToPress].tap()
    }
}
