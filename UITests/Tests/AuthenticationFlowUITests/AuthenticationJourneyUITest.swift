//
//  Created by Backbase R&D B.V. on 05/03/2020.
//

import XCTest

internal final class AuthenticationJourneyUITest: BaseTestCase {
    
    func test_givenAppIsLaunched_whenUsernameAndPasswordNotEntered_userCannotLogIn() throws {
        XCTContext.runActivity(named: "When Username and Password is not entered, on tapping on Login button error is displayed") { _ in
            RegisterScreen.titleLabel.waitForElementToAppear()
            RegisterScreen.loginWithCredentials()
            XCTAssertEqual(
                RegisterScreen.usernameInputErrorMessage.label,
                "*Username is required",
                "Check existance of Username not entered error"
            )
            XCTAssertEqual(
                RegisterScreen.passwordInputErrorMessage.label,
                "*Password is required",
                "Check existance of Password not entered error"
            )
        }
    }

    func test_whenRegistrationIsComplete_thenUserCanLogin() throws {
        XCTContext.runActivity(named: "when username and password is entered user can navigate to biometric screen") { _ in
            RegisterScreen.titleLabel.waitForElementToAppear()
            login(username: Authentication.username, password: Authentication.password)
        }
        XCTContext.runActivity(named: "when user allows Biometrics user can choose, confirm passcode and login") { _ in
            BiometricScreen.allowBiometrics()
            enterPasscode(Authentication.validPasscode)
            enterPasscode(Authentication.validPasscode)
        }

        XCTContext.runActivity(named: "When user has entered passcode, setup complete screen is displayed ") { _ in
            XCTAssertEqual(
                SetupCompleteScreen.titleLabel,
                "Well done!",
                "check the existance of setup complete title"
            )
            XCTAssertEqual(
                SetupCompleteScreen.subtitleLabel,
                "Your device is registered successfully and now you can use the app",
                "check the existance of setup complete title"
            )
            SetupCompleteScreen.getStartedWithApp()
        }

        XCTContext.runActivity(named: "given user is logged in when user taps on logout then user gets logged out of the app") { _ in
            AuthenticatedScreen.logOut()
            RegisterScreen.titleLabel.waitForElementToAppear()
            XCTAssertTrue(RegisterScreen.titleLabel.exists, "Check existance of Get Started title")
        }
    }

    func test_givenUserLogsIn_whenUserDismissBiometric_userCanLoginWithPasscode() throws {
        XCTContext.runActivity(named: "when username and password is entered user can navigate to biometric screen") { _ in
            RegisterScreen.titleLabel.waitForElementToAppear()
            login(username: Authentication.username, password: Authentication.password)
        }
        XCTContext.runActivity(named: "when user deny biometrics usage, user can choose, confirm passcode and login") { _ in
            BiometricScreen.setBiometricsLater()
            enterPasscode(Authentication.validPasscode)
            enterPasscode(Authentication.validPasscode)
        }

        XCTContext.runActivity(named: "When user has entered passcode, setup complete screen is displayed ") { _ in
            XCTAssertEqual(
                SetupCompleteScreen.titleLabel,
                "Well done!",
                "check the existance of setup complete title"
            )
            XCTAssertEqual(
                SetupCompleteScreen.subtitleLabel,
                "Your device is registered successfully and now you can use the app",
                "check the existance of setup complete title"
            )
            SetupCompleteScreen.getStartedWithApp()
        }

        XCTContext.runActivity(named: "given user ends session, user can only login with passcode") { _ in
            AuthenticatedScreen.endSession()
            XCTAssertTrue(LoginScreen.titleLabel.exists)
            XCTAssertFalse(LoginScreen.biometricsButton.exists)
            LoginScreen.loginWithPasscode()
            enterPasscode(Authentication.validPasscode)
            XCTAssertTrue(
                AuthenticatedScreen.endSessionButton.exists,
                "user has logged in as End Session button exists, which verifies that user has logged in"
            )
        }
    }

    func test_givenUserLogsIn_whenUserDismissBiometricAndPasscode_userCanLogin() throws {
        XCTContext.runActivity(named: "when username and password is entered user can navigate to biometric screen") { _ in
            RegisterScreen.titleLabel.waitForElementToAppear()
            login(username: Authentication.username, password: Authentication.password)
        }
        XCTContext.runActivity(named: "when biometric and passcode are dismissed then user can log in") { _ in
            BiometricScreen.setBiometricsLater()
            PasscodeScreen.dismiss()
        }

        XCTContext.runActivity(named: "When user has dismissed Biometrics and passcode, setup complete screen is displayed ") { _ in
            XCTAssertEqual(
                SetupCompleteScreen.titleLabel,
                "Well done!",
                "check the existance of setup complete title"
            )
            XCTAssertEqual(
                SetupCompleteScreen.subtitleLabel,
                "Your device is registered successfully and now you can use the app",
                "check the existance of setup complete title"
            )
            SetupCompleteScreen.getStartedWithApp()
        }

        XCTContext.runActivity(named: "given user has dismissed biometric and passcode, user can register") { _ in
            AuthenticatedScreen.endSessionButton.waitForElementToAppear()
            XCTAssertTrue(
                AuthenticatedScreen.endSessionButton.exists,
                "user has logged in as End Session button exists, which verifies that user has logged in"
            )
        }
    }

    func test_givenUserDismissBiometricAndPasscode_whenUserLogsBackIn_thenBiometricScreen_And_Passcode_IsNotDisplayed() throws {
        XCTContext.runActivity(named: "when username and password is entered user can navigate to biometric screen") { _ in
            RegisterScreen.titleLabel.waitForElementToAppear()
            login(username: Authentication.username, password: Authentication.password)
        }
        XCTContext.runActivity(named: "when biometric and passcode are dismissed then user can log in") { _ in
            BiometricScreen.setBiometricsLater()
            PasscodeScreen.dismiss()
        }

        XCTContext.runActivity(named: "When user has entered passcode, setup complete screen is displayed ") { _ in
            XCTAssertEqual(
                SetupCompleteScreen.titleLabel,
                "Well done!",
                "check the existance of setup complete title"
            )
            XCTAssertEqual(
                SetupCompleteScreen.subtitleLabel,
                "Your device is registered successfully and now you can use the app",
                "check the existance of setup complete title"
            )
            SetupCompleteScreen.getStartedWithApp()
        }

        XCTContext.runActivity(named: "given user is logged in when user taps on logout then user gets logged out of the app") { _ in
            RegisterScreen.titleLabel.waitForElementToAppear()
            AuthenticatedScreen.endSession()
            XCTAssertTrue(
                RegisterScreen.titleLabel.exists,
                "user has logged in as End Session button exists, which verifies that user has logged in"
            )
        }

        XCTContext.runActivity(named: "when username and password is entered user can log in directly") { _ in
            login(username: Authentication.username, password: Authentication.password)
            XCTAssertTrue(
                AuthenticatedScreen.endSessionButton.exists,
                "user has logged in as End Session button exists, which verifies that user has logged in"
            )
        }
    }


    func test_givenUserEndsSession_whenUserNavigatesToLogin_thenUserCanLoginWithBiometrics() throws {
        XCTContext.runActivity(named: "given user logged in") { _ in
            RegisterScreen.titleLabel.waitForElementToAppear()
            login(username: Authentication.username, password: Authentication.password)
            BiometricScreen.allowBiometrics()
            enterPasscode(Authentication.validPasscode)
            enterPasscode(Authentication.validPasscode)
            SetupCompleteScreen.getStartedWithApp()
        }

        XCTContext.runActivity(named: "when user taps on end session then user gets navigated to Login screen") { _ in
            AuthenticatedScreen.endSession()
            LoginScreen.titleLabel.waitForElementToAppear()
            XCTAssertTrue(LoginScreen.titleLabel.exists, "Check existance of Login title")
        }

        XCTContext.runActivity(named: "then user logs in with biometrics") { _ in
            LoginScreen.loginWithBiometrics()
        }
        XCTContext.runActivity(named: "and user is logged in when user logs out then user is navigated to username screen") { _ in
            AuthenticatedScreen.endSessionButton.waitForElementToAppear()
            XCTAssertTrue(
                AuthenticatedScreen.endSessionButton.exists,
                "user has logged in as End Session button exists, which verifies that user has logged in"
            )
        }
    }

    func test_givenWhileRegisteration_whenTryingWrongPasscode3Times_thenUserIsBlocked() throws {
        XCTContext.runActivity(named: "given user logged in") { _ in
            RegisterScreen.titleLabel.waitForElementToAppear()
            login(username: Authentication.username, password: Authentication.password)
            BiometricScreen.allowBiometrics()
            enterPasscode(Authentication.validPasscode)
            enterPasscode(Authentication.validPasscode)
            SetupCompleteScreen.getStartedWithApp()
        }

        XCTContext.runActivity(named: "when user taps on end session then user gets navigated to welcome back screen") { _ in
            AuthenticatedScreen.endSession()
            LoginScreen.titleLabel.waitForElementToAppear()
            XCTAssertTrue(LoginScreen.titleLabel.exists, "Check existance of Welcome Back title")
        }
        XCTContext.runActivity(named: "and user enters wrong passcode three time, then user is blocked") { _ in
            LoginScreen.loginWithPasscode()
            enterPasscode(Authentication.invalidPasscode)
            XCTAssertTrue(LoginScreen.retryAlertDialog.exists, "incorrect passcode alert displayed")
            LoginScreen.retryIncorrectPasscode()
            enterPasscode(Authentication.invalidPasscode)
            XCTAssertTrue(LoginScreen.retryAlertDialog.exists, "incorrect passcode alert displayed")
            LoginScreen.retryIncorrectPasscode()
            enterPasscode(Authentication.invalidPasscode)
            RegisterScreen.tempBlockedAlert.waitForElementToAppear()
            XCTAssertTrue(
                RegisterScreen.tempBlockedAlert.exists,
                "user has logged in as End Session button exists, which verifies that user has logged in"
            )
        }
    }

    func test_givenOnUsernameScreen_whenForgotUsernameTapped_thenUserCanRecoverUsername() throws {
        XCTContext.runActivity(named: "given app is launched") { _ in
            RegisterScreen.titleLabel.waitForElementToAppear()
        }
        XCTContext.runActivity(named: "when user taps on forget username and enters valid e-mail") { _ in
            RegisterScreen.gotoForgetUsername()
            InputRequiredScreen.confirmEmailButton.waitForElementToAppear()
            InputRequiredScreen.enterEmail(email: Authentication.validEmail)
            InputRequiredScreen.sendEmailConfirmation()
        }
        XCTContext.runActivity(named: "then an e-mail is sent to recover username") { _ in
            InputRequiredScreen.planeImage.waitForElementToAppear()
            XCTAssertEqual(
                InputRequiredScreen.emailSentTitle,
                "On its way",
                "check the existance of email sent title"
            )
            InputRequiredScreen.forgetUsernameDone()
            RegisterScreen.titleLabel.waitForElementToAppear()
            XCTAssertTrue(RegisterScreen.titleLabel.exists, "check that user is navigated to Username Screen")
        }
    }

    func test_givenOnForgetUsernameScreen_whenInvalidEmail_thenErrorIsDisplayed() throws {
        XCTContext.runActivity(named: "given app launched") { _ in
            RegisterScreen.titleLabel.waitForElementToAppear()
        }

        XCTContext.runActivity(named: "when invalid e-mail entered") { _ in
            RegisterScreen.gotoForgetUsername()
            InputRequiredScreen.confirmEmailButton.waitForElementToAppear()
            InputRequiredScreen.enterEmail(email: Authentication.inValidEmail)
            InputRequiredScreen.sendEmailConfirmation()
        }

        XCTContext.runActivity(named: "then error alert is displayed") { _ in
            XCTAssertTrue(
                InputRequiredScreen.invalidEmailAlert.waitForExistence(timeout: 5),
                "No invalid email error alert shown within timeout period"
            )
        }
    }
    
    // MARK: - Private
    
    private func login(username: String, password: String) {
        RegisterScreen.usernameInput.typeText(username)
        RegisterScreen.passwordInput.tap()
        RegisterScreen.passwordInput.typeText(password)
        RegisterScreen.loginButton.tap()
    }
}
