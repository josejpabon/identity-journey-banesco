//
//  Copyright © 2020 Backbase R&D B.V. All rights reserved.
//

import XCTest

internal class BaseTestCase: XCTestCase {
    static let app = XCUIApplication()

    override func setUp() {
        super.setUp()

        Biometrics.enrolled()
        BaseTestCase.app.launchArguments = ["RESET_AUTH"]
        BaseTestCase.app.launch()

        addUIInterruptionMonitor(withDescription: "Local Notifications") { (alert) -> Bool in
            let notifPermission = "Would Like to Send You Notifications"
            if alert.labelContains(text: notifPermission) {
                alert.buttons["Allow"].tap()
                return true
            }
            return false
        }
    }

    override func tearDown() {
        XCUIApplication().terminate()
        super.tearDown()
    }
}
