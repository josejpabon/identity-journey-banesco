//
//  Copyright © 2020 Backbase R&D B.V. All rights reserved.
//

import Foundation

struct Authentication {
    static let username: String = "test"
    static let password: String = "test"
    static let validPasscode: String = "11111"
    static let invalidPasscode: String = "33333"
    static let validEmail: String = "xyz@abc.com"
    static let inValidEmail: String = "abc.com"
}
