//
//  Created by Backbase R&D B.V. on 30/4/2020.
//

import XCTest
import SnapKit
import Resolver
import SnapshotTesting
@testable import IdentityAuthenticationJourney

//These tests have to be run on iPhone 11.
final class AuthenticationSnapshotTests: XCTestCase {
    private let precision: Float = 1.0

    override class func setUp() {
        super.setUp()
        Resolver.register { TruthyAuthUseCase() as AuthenticationUseCase }
        // uncomment this line to take new reference screenshots
//         isRecording = true
    }

    func testRegister() {
        Resolver.register { Authentication.Configuration() }
        let viewController = Register.build()(UINavigationController())
        generateScreenshotsForViewController(viewController, name: "register")
    }

    func testLogin() {
        Resolver.register { Authentication.Configuration() }
        let viewController = Login.build()(UINavigationController())
        generateScreenshotsForViewController(viewController, name: "login")
    }

    func testBiometricRegistration() {
        Resolver.register { Authentication.Configuration() }
        let viewModel = BiometricRegistrationViewModel(contract: Biometric.authenticator)
        let view = BiometricRegistrationView()
        view.bind(viewModel: viewModel)
        generateScreenshotsForView(view, name: "biometric_registration")
    }

    func testPasscodeRegistration() {
        Resolver.register { Authentication.Configuration() }
        let viewModel = PasscodeRegistrationViewModel(contract: Passcode.authenticator)
        let view = PasscodeRegistrationView(frame: UIScreen.main.bounds)
        view.bind(viewModel: viewModel)
        generateScreenshotsForView(view, name: "passcode_registration")
    }

    func testPasscodeRegistrationWithFourDigits() {
        var config = Authentication.Configuration()
        config.passcode.length = 4
        Resolver.register { config }
        let viewModel = PasscodeRegistrationViewModel(contract: Passcode.authenticator)
        let view = PasscodeRegistrationView(frame: UIScreen.main.bounds)
        view.bind(viewModel: viewModel)
        generateScreenshotsForView(view, name: "passcode_registration_4_digits")
    }

    func testPasscodeRegistrationWithSixDigits() {
        var config = Authentication.Configuration()
        config.passcode.length = 6
        Resolver.register { config }
        let viewModel = PasscodeRegistrationViewModel(contract: Passcode.authenticator)
        let view = PasscodeRegistrationView(frame: UIScreen.main.bounds)
        view.bind(viewModel: viewModel)
        generateScreenshotsForView(view, name: "passcode_registration_6_digits")
    }

    func testPasscodeLogin() {
        Resolver.register { Authentication.Configuration() }
        let viewModel = PasscodeLoginViewModel(contract: Passcode.authenticator)
        let view = PasscodeLoginView(frame: UIScreen.main.bounds)
        view.bind(viewModel: viewModel)
        generateScreenshotsForView(view, name: "passcode_login")
    }

    func testPasscodeChange() {
        Resolver.register { Authentication.Configuration() }
        let viewModel = PasscodeChangeViewModel(contract: Passcode.authenticator)
        let view = PasscodeChangeView(frame: UIScreen.main.bounds)
        view.bind(viewModel: viewModel)
        generateScreenshotsForView(view, name: "passcode_change")
    }

    func testInputRequiredForEmail() {
        Resolver.register { Authentication.Configuration() }
        let viewModel = InputRequiredViewModel(fields: [.email], contract: InputRequired.authenticator)
        let view = InputRequiredView(frame: UIScreen.main.bounds)
        view.bind(viewModel: viewModel)
        generateScreenshotsForView(view, name: "input_required_email")
    }

    func testInputRequiredForMultipleFields() {
        Resolver.register { Authentication.Configuration() }
        let fields: [InputField] = [.email, .unknown("Username"), .unknown("Another field")]
        let viewModel = InputRequiredViewModel(fields: fields, contract: InputRequired.authenticator)
        let view = InputRequiredView(frame: UIScreen.main.bounds)
        view.bind(viewModel: viewModel)
        generateScreenshotsForView(view, name: "input_required_multiple_fields")
    }

    func testSetupComplete() {
        Resolver.register { Authentication.Configuration() }
        let user = User(username: "test", password: "test")
        let viewController = SetupComplete.build(navigationController: UINavigationController())(user)
        generateScreenshotsForViewController(viewController, name: "setup_complete")
    }

    func testEmailSent() {
        var configuration = Authentication.Configuration()
        configuration.emailSent.animation = nil
        Resolver.register { configuration }
        let viewController = EmailSent.build(navigationController: UINavigationController())
        generateScreenshotsForViewController(viewController, name: "email_sent")
    }

    func testSolidBackground() {
        let view = Authentication.Design.BackgroundView()
        view.background = .solid(.init(light: .orange, dark: .darkGray))
        generateScreenshotsForView(view, name: "background_solid")
    }

    func testGradientBackground() {
        let view = Authentication.Design.BackgroundView()
        view.background = .gradient(.init(
            startPoint: .init(point: .init(x: 0, y: 0), color: .init(light: .yellow, dark: .black)),
            endPoint: .init(point: .init(x: 1, y: 1), color: .init(light: .red, dark: .green))
        ))
        generateScreenshotsForView(view, name: "background_gradient")
    }

    func testImageBackground() {
        let view = Authentication.Design.BackgroundView()
        let testImage = try! loadImage(named: "test_image")
        view.background = .image(testImage)
        generateScreenshotsForView(view, name: "background_image")
    }

    // MARK: - Private

    private func generateScreenshotsForViewController(_ viewController: UIViewController, name: String) {
        func generateSnapshots(name: String) {
            assertSnapshot(matching: viewController, as: .image(on: .iPhoneSe(.portrait), precision: precision), named: "iphone_se", testName: name)
            assertSnapshot(matching: viewController, as: .image(on: .iPhone8(.portrait), precision: precision), named: "iphone_8", testName: name)
            assertSnapshot(matching: viewController, as: .image(on: .iPhone8Plus(.portrait), precision: precision), named: "iphone_8_plus", testName: name)
            assertSnapshot(matching: viewController, as: .image(on: .iPhoneX(.portrait), precision: precision), named: "iphone_x", testName: name)
            assertSnapshot(matching: viewController, as: .image(on: .iPhoneXsMax(.portrait), precision: precision), named: "iphone_xs_max", testName: name)
        }
        if #available(iOS 13.0, *) {
            viewController.overrideUserInterfaceStyle = .light
            generateSnapshots(name: "light_\(name)")

            viewController.overrideUserInterfaceStyle = .dark
            generateSnapshots(name: "dark_\(name)")
        } else {
            generateSnapshots(name: name)
        }
    }

    private func generateScreenshotsForView(_ view: UIView, name: String) {
        let viewController = TestViewController(testView: view)
        generateScreenshotsForViewController(viewController, name: name)
    }

    private func loadImage(named name: String, type: String = "png") throws -> UIImage {
        let bundle = Bundle(for: Self.self)
        guard let path = bundle.path(forResource: name, ofType: type) else {
            throw NSError(domain: "loadImage", code: 1, userInfo: nil)
        }
        guard let image = UIImage(contentsOfFile: path) else {
            throw NSError(domain: "loadImage", code: 2, userInfo: nil)
        }
        return image
    }
}

private final class TestViewController<View: UIView>: UIViewController {
    var testView: View
    required init(testView: View) {
        self.testView = testView
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(testView)
        testView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
