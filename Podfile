platform :ios, '12.0'

plugin 'cocoapods-art', sources: %w[
  bbartifacts3
  bbartifacts-retail3
  bbartifacts-identity
]

install! 'cocoapods', deterministic_uuids: false
source 'https://cdn.cocoapods.org/'

use_frameworks!
inhibit_all_warnings!

abstract_target 'Common' do
  pod 'BackbaseIdentity', '~> 1.4'
  pod 'BackbaseDesignSystem', '~> 1.0'
  pod 'SnapKit', '~> 5'
  pod 'Resolver', '~> 1.2.1'
  pod 'RxCocoa', '~> 5'
  pod 'RxSwift', '~> 5'
  
  pod 'SwiftLint'

  target 'IdentityAuthenticationJourney'
  
  target 'TestApp'

  target 'Tests' do
    inherit! :search_paths

    pod 'RxTest',     '~> 5'
    pod 'RxBlocking', '~> 5'
  end

  target 'SnapshotTests' do
    inherit! :search_paths
    
    pod 'SnapshotTesting', '~> 1.8.2'
  end

  target 'UITests'
end

post_install do |installer_representation|
  installer_representation.pods_project.targets.each do |target|
    target.build_configurations.each do |config|
      config.build_settings['CLANG_ENABLE_CODE_COVERAGE'] = 'NO'
      config.build_settings['ENABLE_BITCODE'] = 'YES'
      config.build_settings['BUILD_LIBRARY_FOR_DISTRIBUTION'] = 'YES'
      config.build_settings['EXPANDED_CODE_SIGN_IDENTITY'] = ''
      config.build_settings['CODE_SIGNING_REQUIRED'] = 'NO'
      config.build_settings['CODE_SIGNING_ALLOWED'] = 'NO'

      cflags = config.build_settings['OTHER_CFLAGS'] || ['$(inherited)']
      if config.name == 'Release'
        cflags << '-fembed-bitcode'
        config.build_settings['BITCODE_GENERATION_MODE'] = 'bitcode'
      else
        cflags << '-fembed-bitcode-marker'
        config.build_settings['BITCODE_GENERATION_MODE'] = 'marker'
      end

      config.build_settings['OTHER_CFLAGS'] = cflags
    end
  end
end
