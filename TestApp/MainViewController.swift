//
//  Created by Backbase R&D B.V. on 23/09/2020.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit

internal final class MainViewController: UIViewController {
    func bind(viewModel: MainViewModel) {
        self.viewModel = viewModel

        let output = viewModel.bind(
            endSession: endSessionButton.rx.tap.asObservable(),
            logout: logOutButton.rx.tap.asObservable()
        )

        disposeBag.insert(
            output.endSessionButtonTitle.drive(endSessionButton.rx.title()),
            output.logoutButtonTitle.drive(logOutButton.rx.title())
        )
    }

    // MARK: - View lifecycle methods

    override func loadView() {
        super.loadView()

        view.backgroundColor = .white

        view.addSubview(stackView)
        stackView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(8*3)
        }
    }

    // MARK: - Private

    private lazy var endSessionButton = UIButton(type: .system)
    private lazy var logOutButton = UIButton(type: .system)

    private lazy var stackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [endSessionButton, logOutButton])
        view.axis = .vertical
        view.alignment = .fill
        view.distribution = .fillEqually
        view.spacing = 8
        return view
    }()

    private var viewModel: MainViewModel?
    private let disposeBag = DisposeBag()
}
