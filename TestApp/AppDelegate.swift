//
//  Created by Backbase R&D B.V. on 23/09/2020.
//

import UIKit
import Backbase
import IdentityAuthenticationJourney
import Resolver

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions
        launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {

        window = UIWindow()
        window?.rootViewController = UIViewController()
        window?.makeKeyAndVisible()

        initializeBackbase()
        // This flag is set by UI tests to reset the device between tests.
        if ProcessInfo.processInfo.arguments.contains("RESET_AUTH") {
            authenticationUseCase.reset()
            // This is speeds up animation and makes test exection 2 times faster
            UIApplication.shared.keyWindow?.layer.speed = 100
            
            #if targetEnvironment(simulator)
            // Disable hardware keyboards.
            let setHardwareLayout = NSSelectorFromString("setHardwareLayout:")
            UITextInputMode.activeInputModes
                // Filter `UIKeyboardInputMode`s.
                .filter({ $0.responds(to: setHardwareLayout) })
                .forEach { $0.perform(setHardwareLayout, with: nil) }
            #endif
        }

        authenticationUseCase.validateSession()

        return true
    }

    // MARK: - Backbase SDK

    private func initializeBackbase() {
        do {
            try Backbase.initialize("config.json", forceDecryption: false)
            Backbase.register(navigationEventListener: self, selector: #selector(handleNavigationEvent))
        } catch {
            fatalError("Backbase MSDK initialization failed: \(error.localizedDescription)")
        }
    }

    // MARK: - Authentication Journey

    private var modalViewController: UIViewController?

    private lazy var authenticationUseCase: IdentityAuthenticationUseCase = { [weak self] in
        let usecase = IdentityAuthenticationUseCase(sessionChangeHandler: self?.handleSessionChange(newSession:))
        Backbase.register(authClient: usecase)

        Resolver.register { Authentication.Configuration() }
        Resolver.register { usecase as AuthenticationUseCase }

        return usecase
    }()

    @objc
    private func handleNavigationEvent(_ notification: Notification) {
        guard let navigation = authenticationUseCase.navigation(for: notification) else { return }
        DispatchQueue.main.async {
            switch navigation {
            case .dismissCurrent:
                self.modalViewController?.dismiss(animated: true)
            case .present(let screen):
                let navigationController = UINavigationController()
                let viewController = screen(navigationController)
                self.modalViewController = viewController
                self.window?.rootViewController?.present(viewController, animated: true)
            @unknown default:
                break
            }
        }
    }

    private func handleSessionChange(newSession session: Session) {
        DispatchQueue.main.async {
            let windowViewController: UIViewController
            let navigationController = UINavigationController()

            switch session {
            case .valid:
                windowViewController = Main.build()
            case .none:
                if self.authenticationUseCase.isEnrolled {
                    windowViewController = Login.build()(navigationController)
                } else {
                    windowViewController = Register.build()(navigationController)
                }
            case .locked:
                windowViewController = Register.build(session: .locked)(navigationController)
            case .expired:
                windowViewController = Login.build(session: .expired)(navigationController)
            @unknown default:
                fatalError()
            }
            navigationController.viewControllers = [windowViewController]
            self.window?.rootViewController = navigationController
        }
    }
}
