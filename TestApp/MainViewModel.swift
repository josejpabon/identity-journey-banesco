//
//  Created by Backbase R&D B.V. on 23/09/2020.
//

import RxSwift
import RxCocoa
import Resolver
import IdentityAuthenticationJourney

internal final class MainViewModel {
    func bind(
        endSession: Observable<Void>,
        logout: Observable<Void>
    ) -> (
        endSessionButtonTitle: Driver<String>,
        logoutButtonTitle: Driver<String>
    ) {

        endSession
            .bind { [weak self] in
                self?.authenticationUseCase.endSession(callback: nil)
            }
            .disposed(by: disposeBag)

        logout
            .bind { [weak self] in
                self?.authenticationUseCase.logOut(callback: nil)
            }
            .disposed(by: disposeBag)

        return (
            endSessionButtonTitle: .just("End Session"),
            logoutButtonTitle: .just("Log out")
        )
    }

    // MARK: - Private

    @LazyInjected
    private var authenticationUseCase: AuthenticationUseCase
    private let disposeBag = DisposeBag()
}
