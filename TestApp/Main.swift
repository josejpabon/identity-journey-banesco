//
//  Created by Backbase R&D B.V. on 23/09/2020.
//

import UIKit

struct Main {
    static func build() -> UIViewController {
        let viewModel = MainViewModel()
        let viewController = MainViewController()
        viewController.bind(viewModel: viewModel)
        return viewController
    }
}
