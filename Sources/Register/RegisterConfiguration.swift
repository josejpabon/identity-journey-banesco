//
//  Created by Backbase R&D B.V. on 3/23/20.
//

import UIKit
import Resolver

public extension Register {
    struct Configuration {
        internal init(journeyDesign: Authentication.Design) {
            self.journeyDesign = journeyDesign
        }

        /// Register design configurations.
        public lazy var design = Design(journeyDesign: journeyDesign)

        /// Register strings.
        public var strings = Strings()

        /// Register animation configurations.
        /// **Note**: Set this to` nil` to disable animations.
        public var animation: Animation? = Animation()

        /// Register router.
        public var router = Router(
            didTapForgotUsername: { navigationController in
                return { result in
                    switch result {
                    case .success:
                        let viewController = EmailSent.build(navigationController: navigationController)
                        navigationController.present(viewController, animated: true, completion: {
                            isWaitingToCompleteAuthentication.accept(false)
                        })
                    case .failure(let error):
                        if error == .cancelledByUser {
                            isWaitingToCompleteAuthentication.accept(false)
                            return
                        }
                        guard let configuration = Resolver.optional(Authentication.Configuration.self) else {
                            isWaitingToCompleteAuthentication.accept(false)
                            return
                        }
                        let alert = UIAlertController(
                            title: configuration.strings.alertTitle(error)(),
                            message: configuration.strings.alertMessage(error)(),
                            preferredStyle: .alert
                        )
                        let dimissAction = UIAlertAction(title: configuration.strings.alertConfirmOption(), style: .default)
                        alert.addAction(dimissAction)
                        navigationController.present(alert, animated: true, completion: {
                            isWaitingToCompleteAuthentication.accept(false)
                        })
                    }
                }
            },
            preCompletion: { navigationController in
                return { user in
                    let controller = SetupComplete.build(navigationController: navigationController)(user)
                    navigationController.present(controller, animated: true)
                }
            }
        )

        // MARK: - Private

        private let journeyDesign: Authentication.Design
    }
}
