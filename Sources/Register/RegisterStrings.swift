//
//  Created by Backbase R&D B.V. on 15/5/2020.
//

import Foundation

public extension Register {
    /// Register strings.
    struct Strings {
        /// Create a new strings object with default values.
        public init() {
            // no code required.
        }

        /// authentication.register.labels.title
        public var title = localized(key: "labels.title")

        /// authentication.register.labels.subtitle
        public var subtitle = localized(key: "labels.subtitle")

        /// authentication.register.inputs.username.placeholder
        public var usernamePlaceholder = localized(key: "inputs.username.placeholder")

        /// authentication.register.inputs.username.emptyError
        public var emptyUsernameError = localized(key: "inputs.username.emptyError")

        /// authentication.register.inputs.password.placeholder
        public var passwordPlaceholder = localized(key: "inputs.password.placeholder")

        /// authentication.register.inputs.password.emptyError
        public var emptyPasswordError = localized(key: "inputs.password.emptyError")

        /// authentication.register.buttons.forgotUsername
        public var forgotUsernameButtonTitle = localized(key: "buttons.forgotUsername")

        /// authentication.register.buttons.login
        public var loginButtonTitle = localized(key: "buttons.login")

        private static func localized(key: String) -> LocalizedString {
            let prefix = "authentication.register."
            return LocalizedString(key: prefix + key, in: .authentication)
        }
    }
}
