//
//  Created by Backbase R&D B.V. on 24/4/20.
//

import UIKit

public extension Register {
    /// Register design-related configurations.
    struct Design {
        internal init(journeyDesign: Authentication.Design) {
            self.journeyDesign = journeyDesign
        }

        /// Applied to all `BackgroundView` views.
        public lazy var background = journeyDesign.styles.background

        /// Applied to all title labels in register screens.
        public lazy var title = journeyDesign.styles.title

        /// Applied to all subtitle labels in register screens.
        public lazy var subtitle = journeyDesign.styles.subtitle

        /// Applied to the username input label in register screens.
        public lazy var usernameInputLabel = journeyDesign.styles.formLabel
        
        /// Applied to the username input in register screens.
        public lazy var usernameInput = journeyDesign.styles.formTextInput(.username)

        /// Applied to the username input label in register screens.
        public lazy var passwordInputLabel = journeyDesign.styles.formLabel
        
        /// Applied to the password input in register screens.
        public lazy var passwordInput = journeyDesign.styles.formTextInput(.password)

        /// Applied to the "forgot username" button in register screens.
        public lazy var forgotUsernameButton = journeyDesign.styles.button(.plain)

        /// Applied to the login button in register screens.
        public lazy var loginButton = journeyDesign.styles.button(.primary)

        // MARK: - Private

        private let journeyDesign: Authentication.Design
    }
}
