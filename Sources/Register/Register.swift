//
//  Created by Backbase R&D B.V. on 3/23/20.
//

import UIKit
import Resolver

/// Authentication namespace used for grouping register level configuration.
public struct Register {
    /// Build a register view controller from a session state.
    /// - Parameter session: Session state (default is `Session.none`).
    /// - Returns: view controller building closure.
    public static func build(session: Session = .none) -> (UINavigationController) -> UIViewController {
        return { navigationController in
            var configuration = Resolver.optional(Authentication.Configuration.self)

            let viewModel = RegisterViewModel(session: session)
            viewModel.preCompletion = configuration?.register.router.preCompletion?(navigationController)
            viewModel.didTapForgotUsername = configuration?.register.router.didTapForgotUsername?(navigationController)

            let viewController = RegisterViewController()
            viewController.bind(viewModel: viewModel)

            return viewController
        }
    }
}
