//
//  Created by Backbase R&D B.V. on 3/23/20.
//

import UIKit
import RxSwift
import RxCocoa
import Resolver
import class BackbaseDesignSystem.TextInput

internal final class RegisterViewModel {
    init(session: Session) {
        self.session = session
    }

    func bind(
        username: Observable<String>,
        password: Observable<String>,
        forgotUsername: Observable<Void>,
        login: Observable<Void>,
        tap: Observable<Void>,
        willAppear: Observable<Void>,
        didAppear: Observable<Void>
    ) -> (
        title: Driver<String>,
        subtitle: Driver<String>,
        usernameInputState: Driver<TextInput.State?>,
        passwordInputState: Driver<TextInput.State?>,
        forgotUsernameButtonTitle: Driver<String>,
        loginButtonTitle: Driver<String>,
        errorAlert: Signal<UIAlertController>,
        loading: Driver<Bool>,
        waiting: Driver<Bool>,
        editing: Driver<Bool>
    ) {
        didAppear
            .compactMap { [weak self] in return self?.session }
            .compactMap { [weak self] session -> UIAlertController? in
                return session == .locked ? self?.accountLockedAlertController() : nil
            }
            .bind(to: errorAlertRelay)
            .disposed(by: disposeBag)

        willAppear
            .map { false }
            .bind(to: editingRelay)
            .disposed(by: disposeBag)

        willAppear
            .take(1)
            .compactMap { [weak self] in return self?.session }
            .map { $0 != .locked }
            .bind(to: editingRelay)
            .disposed(by: disposeBag)

        tap
            .map { false }
            .bind(to: editingRelay)
            .disposed(by: disposeBag)

        let user = Observable<User>.combineLatest(username, password) {
            User(username: $0, password: $1)
        }

        user
            .skipUntil(firstLoginRelay)
            .subscribe(onNext: { [weak self] user in
                guard let self = self else { return }
                self.emptyUsernameErrorRelay.accept(user.username.isEmpty ? self.configuration.register.strings.emptyUsernameError() : nil)
                self.emptyPasswordErrorRelay.accept(user.password.isEmpty ? self.configuration.register.strings.emptyPasswordError() : nil)
            })
            .disposed(by: disposeBag)

        login
            .withLatestFrom(user)
            .bind(onNext: { [weak self] user in
                self?.firstLoginRelay.accept(true)

                if user.username.isEmpty {
                    self?.emptyUsernameErrorRelay.accept(self?.configuration.register.strings.emptyUsernameError())
                }

                if user.password.isEmpty {
                    self?.emptyPasswordErrorRelay.accept(self?.configuration.register.strings.emptyPasswordError())
                }

                if user.isValid {
                    self?.authenticate(user: user)
                }
            })
            .disposed(by: disposeBag)

        forgotUsername
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in self?.forgotUsername() })
            .disposed(by: disposeBag)

        loadingRelay
            .filter { !$0 }
            .bind(to: isWaitingToCompleteAuthentication)
            .disposed(by: disposeBag)

        let waiting = isWaitingToCompleteAuthentication.asDriver(onErrorJustReturn: false)

        return (
            title: .just(configuration.register.strings.title()),
            subtitle: .just(configuration.register.strings.subtitle()),
            usernameInputState: usernameInputState.asDriver(onErrorJustReturn: nil),
            passwordInputState: passwordInputState.asDriver(onErrorJustReturn: nil),
            forgotUsernameButtonTitle: .just(configuration.register.strings.forgotUsernameButtonTitle()),
            loginButtonTitle: .just(configuration.register.strings.loginButtonTitle()),
            errorAlert: errorAlertRelay.asSignal(),
            loading: loadingRelay.asDriver(onErrorJustReturn: false),
            waiting: waiting,
            editing: editingRelay.asDriver(onErrorJustReturn: false)
        )
    }

    lazy var usernameInputState: Observable<TextInput.State?> = Observable.combineLatest(
        Observable<String>.just(configuration.register.strings.usernamePlaceholder()),
        emptyUsernameErrorRelay.asObservable()
    )
    .map { TextInput.State(label: $0.0, errorMessage: $0.1) }

    lazy var passwordInputState: Observable<TextInput.State?> = Observable.combineLatest(
        Observable<String>.just(configuration.register.strings.passwordPlaceholder()),
        emptyPasswordErrorRelay.asObservable()
    )
    .map { TextInput.State(label: $0.0, errorMessage: $0.1) }

    func forgotUsername() {
        editingRelay.accept(false)
        authenticationUseCase.forgotUsername { [weak self] result in
            DispatchQueue.main.async {
                self?.didTapForgotUsername?(result)
            }
        }
    }

    func accountLockedAlertController() -> UIAlertController {
        let alert = UIAlertController(
            title: configuration.strings.alertTitle(.accountLocked)(),
            message: configuration.strings.alertMessage(.accountLocked)(),
            preferredStyle: .alert
        )
        let dimissAction = UIAlertAction(title: configuration.strings.alertConfirmOption(), style: .default)
        alert.addAction(dimissAction)
        return alert
    }

    func alertController(for error: Authentication.Error) -> UIAlertController {
        let alert = UIAlertController(
            title: configuration.strings.alertTitle(error)(),
            message: configuration.strings.alertMessage(error)(),
            preferredStyle: .alert
        )
        let dimissAction = UIAlertAction(title: configuration.strings.alertConfirmOption(), style: .default)
        alert.addAction(dimissAction)
        return alert
    }

    func authenticate(user: User) {
        loadingRelay.accept(true)
        var handler: (() -> Void)?
        if let preCompletion = self.preCompletion {
            handler = {
                DispatchQueue.main.async {
                    preCompletion(user)
                }
            }
        }
        
        authenticationUseCase.authenticate(
            user: user,
            preCompletion: handler,
            postCompletion: { [weak self] result in
                self?.loadingRelay.accept(false)
                switch result {
                case .success:
                    break
                case .failure(let error):
                    guard let self = self else { return }
                    // the `incompleteEnrollment` error alert is usually presented in passcode or biometric screens
                    // thus, no need to handle it here again.
                    if error != .incompleteEnrollment {
                        self.errorAlertRelay.accept(self.alertController(for: error))
                    }
                }
            }
        )
    }

    var preCompletion: ((User) -> Void)?
    var didTapForgotUsername: ((Register.Router.InputRequiredResult) -> Void)?

    // MARK: - Private

    private let session: Session

    @LazyInjected
    private var configuration: Authentication.Configuration

    @LazyInjected
    private var authenticationUseCase: AuthenticationUseCase

    private let emptyUsernameErrorRelay = BehaviorRelay<String?>(value: nil)
    private let emptyPasswordErrorRelay = BehaviorRelay<String?>(value: nil)

    private let firstLoginRelay = PublishRelay<Bool>()
    private let loadingRelay = PublishRelay<Bool>()
    private let editingRelay = PublishRelay<Bool>()

    private let errorAlertRelay = PublishRelay<UIAlertController>()

    private let disposeBag = DisposeBag()
}

private extension User {
    var isValid: Bool { !username.isEmpty && !password.isEmpty }
}
