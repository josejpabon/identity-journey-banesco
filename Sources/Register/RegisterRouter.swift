//
//  Created by Backbase R&D B.V. on 26/08/2020.
//

import UIKit

public extension Register {
    /// Register router.
    struct Router {
        /// Result of Input required submit operation.
        public typealias InputRequiredResult = Result<Data, Authentication.Error>

        /// An optional handler that gets called when the "forgot username" button is tapped.
        /// Use this handler to as a customization point to present a custom forgot username screen.
        public var didTapForgotUsername: ((UINavigationController) -> (InputRequiredResult) -> Void)?

        /// An optional handler that takes an instance of `User` as input.
        /// Use this handler to present screens or perform actions before completing the registration flow.
        public var preCompletion: ((UINavigationController) -> (User) -> Void)?
        
        public var someCompletion: ((UINavigationController) -> (User) -> Void)?
    }
}
