//
//  Created by Backbase R&D B.V. on 09/09/20.
//

import UIKit

public extension Register {
    /// Authentication journey register animation configurations.
    struct Animation {
        /// An object that represent all animatable views in register screen.
        public struct Animatable {
            /// Forgot username button.
            public let forgotUsernameButton: UIButton
        }

        /// Create a new configurations object with default values.
        public init() {
            // no code required.
        }

        /// Animation block to be applied to animatable when the view is loading.
        /// **Note**: Set this to` nil` to disable animations.
        public var onLoading: ((Bool) -> (Animatable) -> Void)? = { loading in
            return { animatable in
                UIView.animate(
                    withDuration: 0.5,
                    delay: 0,
                    usingSpringWithDamping: 0.75,
                    initialSpringVelocity: 1,
                    options: .curveEaseOut,
                    animations: {
                        animatable.forgotUsernameButton.alpha = loading ? 0 : 1
                        animatable.forgotUsernameButton.isHidden = loading
                    },
                    completion: nil
                )
            }
        }
    }
}
