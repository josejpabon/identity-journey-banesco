//
//  Created by Backbase R&D B.V. on 3/23/20.
//

import UIKit
import RxSwift
import SnapKit
import Resolver
import BackbaseDesignSystem

internal final class RegisterViewController: UIViewController {
    func bind(viewModel: RegisterViewModel) {
        self.viewModel = viewModel

        let output = viewModel.bind(
            username: usernameInput.textField.rx.text.orEmpty.asObservable(),
            password: passwordInput.textField.rx.text.orEmpty.asObservable(),
            forgotUsername: forgotUsernameButton.rx.tap.asObservable(),
            login: loginButton.rx.tap.asObservable(),
            tap: tapGestureRecognizer.rx.event.map({ _ in Void() }),
            willAppear: self.rx.viewWillAppear.map({ _ in Void() }),
            didAppear: self.rx.viewDidAppear.map({ _ in Void() })
        )

        disposeBag.insert(
            output.title.drive(titleLabel.rx.text),
            output.subtitle.drive(subtitleLabel.rx.text),
            output.usernameInputState.drive(onNext: { [weak self] state in
                self?.usernameInput.errorLabel.text = state?.errorMessage
                self?.usernameInputLabel.text = state?.label
            }),
            output.passwordInputState.drive(onNext: { [weak self] state in
                self?.passwordInput.errorLabel.text = state?.errorMessage
                self?.passwordInputLabel.text = state?.label
            }),
            output.forgotUsernameButtonTitle.drive(forgotUsernameButton.rx.title()),
            output.loginButtonTitle.drive(loginButton.rx.title()),
            output.loading.drive(onNext: { [weak self] loading in
                if let animation = self?.configuration.register.animation {
                    guard let animatable = self?.animatable else { return }
                    animation.onLoading?(loading)(animatable)
                } else {
                    self?.forgotUsernameButton.isHidden = loading
                }
            }),
            output.loading.drive(loginButton.rx.isAnimating),
            output.waiting.drive(loadingView.rx.isAnimating),
            output.loading.map({ !$0 }).drive(view.rx.isUserInteractionEnabled),
            output.loading.drive(onNext: { [weak self] _ in
                self?.passwordInput.textField.isSecureTextEntry = true
            }),
            output.editing.filter({ $0 }).drive(usernameInput.textField.rx.firstResponder),
            output.editing.filter({ !$0 }).drive(usernameInput.textField.rx.firstResponder),
            output.editing.filter({ !$0 }).drive(passwordInput.textField.rx.firstResponder),
            output.errorAlert.emit(onNext: { [weak self] alert in
                self?.present(alert, animated: true)
            })
        )

        self.rx.keyboardHeight.bind { [weak self] height in
            self?.updateScrollView(for: height)
        }
        .disposed(by: disposeBag)
    }

    // MARK: - View lifecycle methods

    override func loadView() {
        super.loadView()

        view.addSubviews(backgroundView, scrollView, loadingView)
        scrollView.addGestureRecognizer(tapGestureRecognizer)

        backgroundView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        let safeArea = view.safeAreaLayoutGuide

        scrollView.snp.makeConstraints { make in
            make.edges.equalTo(safeArea)
        }

        loadingView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        stackView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(DesignSystem.shared.spacer.sm * 6)
            make.bottom.equalToSuperview().inset(DesignSystem.shared.spacer.md)
            make.width.equalToSuperview().inset(DesignSystem.shared.sizer.md)
            make.centerX.equalToSuperview()
        }
        
        loginButton.snp.makeConstraints { make in
            make.height.equalTo(DesignSystem.shared.sizer.sm * 6)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configuration.register.design.background(backgroundView)
        configuration.register.design.title(titleLabel)
        configuration.register.design.subtitle(subtitleLabel)
        configuration.register.design.usernameInputLabel(usernameInputLabel)
        configuration.register.design.usernameInput(usernameInput)
        configuration.register.design.passwordInputLabel(passwordInputLabel)
        configuration.register.design.passwordInput(passwordInput)
        configuration.register.design.forgotUsernameButton(forgotUsernameButton)
        configuration.register.design.loginButton(loginButton)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    // MARK: - Private

    private lazy var animatable = Register.Animation.Animatable(forgotUsernameButton: forgotUsernameButton)

    @LazyInjected
    private var configuration: Authentication.Configuration

    private var viewModel: RegisterViewModel?
    private let disposeBag = DisposeBag()

    private lazy var backgroundView = Authentication.Design.BackgroundView(frame: view.frame)
    private lazy var tapGestureRecognizer = UITapGestureRecognizer()

    private lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.backgroundColor = .clear
        view.addSubviews(stackView)
        return view
    }()

    private lazy var loadingView = LoadingView()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "register.titleLabel"
        label.numberOfLines = 0
        return label
    }()

    private let subtitleLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "register.subtitleLabel"
        label.numberOfLines = 0
        return label
    }()

    private lazy var stackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [
            titleLabel,
            subtitleLabel,
            usernameInputLabel,
            usernameInput,
            passwordInputLabel,
            passwordInput,
            forgotUsernameButton,
            loginButton
        ])
        view.setCustomSpacing(DesignSystem.shared.spacer.sm, after: titleLabel)
        view.setCustomSpacing(DesignSystem.shared.spacer.lg, after: forgotUsernameButton)
        view.setCustomSpacing(DesignSystem.shared.spacer.xxs, after: usernameInputLabel)
        view.setCustomSpacing(DesignSystem.shared.spacer.xxs, after: passwordInputLabel)
        view.axis = .vertical
        view.alignment = .fill
        view.distribution = .fill
        view.spacing = DesignSystem.shared.spacer.md
        return view
    }()

    private let forgotUsernameButton: Button = {
        let button = Button()
        button.accessibilityIdentifier = "register.forgotUsernameButton"
        return button
    }()

    private let usernameInputLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "register.usernameInput.placeholderLabel"
        return label
    }()
    
    private let usernameInput: TextInput = {
        let input = TextInput()
        input.accessibilityIdentifier = "register.usernameInput"
        return input
    }()

    private let passwordInputLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "register.passwordInput.placeholderLabel"
        return label
    }()
    
    private let passwordInput: TextInput = {
        let input = TextInput()
        input.accessibilityIdentifier = "register.passwordInput"
        return input
    }()

    private let loginButton: Button = {
        let button = Button()
        button.accessibilityIdentifier = "register.loginButton"
        return button
    }()

    private func updateScrollView(for hight: CGFloat) {
        scrollView.contentInset.bottom = hight
    }
}
