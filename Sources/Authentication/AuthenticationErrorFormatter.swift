//
//  Created by Backbase R&D B.V. on 13/5/2020.
//

public extension Authentication {
    /// Used to transform `Authentication.Error` into another type like `String` or `UIImage`.
    typealias ErrorFormatter<T> = (Authentication.Error) -> T
}
