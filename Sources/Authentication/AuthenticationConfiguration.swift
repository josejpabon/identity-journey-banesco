//
//  Created by Backbase R&D B.V. on 22/01/2020.
//

import Foundation

public extension Authentication {
    /// Authentication journey configurations.
    struct Configuration {
        /// Create a new configurations object with default values.
        public init() {
            // no code required.
        }

        /// Register screen configurations.
        public lazy var register = Register.Configuration(journeyDesign: design)

        /// Login screen configurations.
        public lazy var login = Login.Configuration(journeyDesign: design)

        /// Passcode screen configurations.
        public lazy var passcode = Passcode.Configuration(journeyDesign: design)

        /// Biometric screen configurations.
        public lazy var biometric = Biometric.Configuration(journeyDesign: design)

        /// Input required screen configurations.
        public lazy var inputRequired = InputRequired.Configuration(journeyDesign: design)

        /// Email sent screen configurations.
        public lazy var emailSent = EmailSent.Configuration(journeyDesign: design)

        /// Setup complete screen configurations.
        public lazy var setupComplete = SetupComplete.Configuration(journeyDesign: design)

        /// Journey strings.
        public var strings = Strings()

        /// Journey design.
        public var design = Design()
    }
}
