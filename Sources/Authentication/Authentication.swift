//
//  Created by Backbase R&D B.V. on 6/5/2019.
//

import UIKit
import RxRelay

/// This flag is used to show a full screen loader to block unintended user interactions
/// when displaying passcode or biometric screens on top of the register screen.
internal var isWaitingToCompleteAuthentication = PublishRelay<Bool>()

/// Authentication namespace used for grouping journey level configuration.
public struct Authentication {
    /// An enum that represents a navigation event.
    public enum Navigation {
        /// Dismiss current screen's view controller.
        case dismissCurrent

        /// Present a view controller.
        case present((UINavigationController) -> UIViewController)
    }
}
