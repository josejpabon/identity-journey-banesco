//
//  Created by Backbase R&D B.V. on 13/5/2020.
//

import Foundation
import BackbaseIdentity
import LocalAuthentication

public extension Authentication {
    /// Authentication journey errors.
    enum Error: Swift.Error {
        /// the device has no internet connection.
        case notConnected

        /// The entered credentials are wrong.
        case invalidCredentials

        /// The entered passcode is wrong.
        case invalidPasscode

        /// Session expired.
        case sessionExpired

        /// Account is locked. Registration flow should be restarted.
        case accountLocked

        /// The operation was cancelled by user.
        case cancelledByUser

        /// The registration flow timed out, or was interrupted.
        case incompleteEnrollment

        /// The entered passcodes do not match.
        case passcodeMismatch

        /// Biometric sensor usage was denied by user or system.
        case biometricUsageDenied

        /// Biometric sensor was locked out by the system.
        case biometricLockout

        /// Invalid email format.
        case invalidEmail

        /// Unknown error thrown by Identity SDK.
        case sdk(Swift.Error)
    }
}

internal extension Authentication.Error {
    init(biometric error: Error, biometryType: BiometryType = .allowed) {
        let authError = Authentication.Error(sdk: error)
        if authError == .cancelledByUser {
            self = authError
            return
        }
        switch biometryType {
        case .denied:
            self = .biometricUsageDenied
        case .lockedOut:
            self = .biometricLockout
        default:
            self = authError
        }
    }

    init(passcode error: Error) {
        let authError = Authentication.Error(sdk: error)
        if authError == .invalidCredentials {
            self = .invalidPasscode
        } else {
            self = authError
        }
    }

    init(inputRequired error: Error) {
        let nsError = error as NSError
        if nsError.code == 0 && error.localizedDescription == "Invalid email" {
            self = .invalidEmail
        } else {
            self = Authentication.Error(sdk: error)
        }
    }

    init(sdk error: Error) {
        if let authError = error as? Authentication.Error {
            self = authError
            return
        }
        
        var nsError = error as NSError

        if let headers = nsError.headers, headers["Www-Authenticate"] == "Bearer challenge_types=device-key" {
            self = .incompleteEnrollment
            return
        }

        if let rootCause = nsError.rootCause() {
            nsError = rootCause as NSError
        }

        switch nsError.code {
        case NSURLErrorNotConnectedToInternet:
            self = .notConnected

        case 401, 403, 1012:
            self = .invalidCredentials

        case BBIDErrors.deviceKeyNotFound.rawValue,
             BBIDErrors.invalidDeviceId.rawValue,
             1007:
            self = .incompleteEnrollment

        case 1020:
            self = .accountLocked

        case BBIDErrors.userCancelledAuthenticator.rawValue,
             BBIDErrors.userDeniedUsage.rawValue,
             LAError.userCancel.rawValue:
            self = .cancelledByUser

        case BBIDErrors.passcodeMismatch.rawValue:
            self = .passcodeMismatch

        default:
            self = .sdk(error)
        }
    }
}

extension Authentication.Error: Equatable {
    public static func == (lhs: Authentication.Error, rhs: Authentication.Error) -> Bool {
        return lhs.localizedDescription == rhs.localizedDescription
    }
}

extension Authentication.Error: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .notConnected:
            return "Make sure your device is connected to the internet."
        case .invalidCredentials:
            return "Incorrect username or password. Try again"
        case .invalidPasscode:
            return "Incorrect passcode. Try again"
        case .sessionExpired:
            return "Session expired. Log in and try again"
        case .accountLocked:
            return "Device locked, restart the authentication flow"
        case .cancelledByUser:
            return "Authentication was cancelled by user"
        case .incompleteEnrollment:
            return "Enrollment was not completed correctly. Please try again."
        case .passcodeMismatch:
            return "Passcodes do not match. Try again"
        case .biometricUsageDenied:
            return "Biometric usage denied. Fix this from Settings and try again"
        case .biometricLockout:
            return "Biometric sensor was locked out due to serveral wrong attempts. Lock the device, unlock it with passcode and try again"
        case .invalidEmail:
            return "Invalid email format"
        case .sdk(let error):
            return error.localizedDescription
        }
    }
}

private extension NSError {
    var headers: [String: String]? {
        guard let details = userInfo["details"] as? [String: Any] else { return nil }
        return details["headers"] as? [String: String]
    }
}
