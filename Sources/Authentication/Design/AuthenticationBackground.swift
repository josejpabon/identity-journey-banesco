//
//  Created by Backbase R&D B.V. on 26/10/2020.
//

import UIKit

extension Authentication.Design {
    /// Represents background options.
    public enum Background {
        /// Set background to solid `UIColor`.
        case solid(UIColor?)

        /// Set background to `Gradient`.
        case gradient(Gradient)

        /// Set background to `UIImage`.
        case image(UIImage?)
    }

    /// `UIView` subclass where the background can be configured using the `Background` enum.
    open class BackgroundView: UIView {
        internal var gradientView: GradientView?
        internal var imageView: UIImageView?

        /// The bounds rectangle, which describes the view’s location and size in its own coordinate system.
        open override var bounds: CGRect {
            didSet {
                gradientView?.frame = bounds
                imageView?.frame = bounds
            }
        }

        /// View's `Background`.
        /// Setting this to `nil` will remove all background color, gradient, and image.
        open var background: Background? {
            didSet {
                guard let background = background else {
                    backgroundColor = nil
                    setupImageView(for: nil)
                    setupGradientView(for: nil)
                    return
                }

                switch background {
                case .solid(let color):
                    setupGradientView(for: nil)
                    setupImageView(for: nil)
                    backgroundColor = color
                case .gradient(let gradient):
                    backgroundColor = nil
                    setupImageView(for: nil)
                    setupGradientView(for: gradient)
                case .image(let uiImage):
                    backgroundColor = nil
                    setupGradientView(for: nil)
                    setupImageView(for: uiImage)
                }
            }
        }

        internal func setupImageView(for image: UIImage?) {
            guard let image = image else {
                imageView?.removeFromSuperview()
                imageView = nil
                return
            }

            guard let view = imageView else {
                let newView = UIImageView(frame: frame)
                newView.contentMode = .scaleAspectFill
                imageView = newView
                insertSubview(newView, at: 0)
                setupImageView(for: image)
                return
            }

            view.image = image
        }

        internal func setupGradientView(for gradient: Gradient?) {
            guard let gradient = gradient else {
                gradientView?.removeFromSuperview()
                gradientView = nil
                return
            }

            guard let view = gradientView else {
                let newView = GradientView(frame: frame)
                gradientView = newView
                insertSubview(newView, at: 0)
                setupGradientView(for: gradient)
                return
            }

            view.gradient = gradient
        }
    }
}
