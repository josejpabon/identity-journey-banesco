//
//  Created by Backbase R&D B.V. on 26/10/2020.
//

import UIKit
import SnapKit

extension Authentication.Design {
    /// Use this view for one time password entry, like passcodes.
    public final class OTPInput: UIControl {
        private let length: Int

        /// Create a new `OTPInput` view with a given length.
        /// - Parameter length: code length.
        public required init(length: Int) {
            self.length = length
            super.init(frame: .zero)

            addSubview(stackView)

            stackView.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }

            fields.forEach { field in
                field.snp.makeConstraints { make in
                    make.height.equalTo(field.snp.width).multipliedBy(1.1)
                }
            }
        }

        @available(*, unavailable)
        public  required init?(coder: NSCoder) {
            fatalError()
        }

        /// Asks UIKit to make this object the first responder in its window.
        /// - Returns: `true` if this object is now the first-responder or `false` if it is not.
        @discardableResult
        override public func becomeFirstResponder() -> Bool {
            return fields.first?.becomeFirstResponder() ?? super.becomeFirstResponder()
        }

        /// Identifies whether the text object should disable text copying and in some cases hide the text being entered.
        public var isSecureTextEntry: Bool = false {
            didSet {
                fields.forEach { $0.isSecureTextEntry = isSecureTextEntry }
            }
        }

        /// The text displayed by the view's text fields.
        public var text: String? {
            get {
                let text = fields.compactMap(\.text).joined()
                guard text.count == length else { return nil }
                return text
            }
            set {
                guard let newText = newValue else {
                    fields.forEach { $0.text = "" }
                    becomeFirstResponder()
                    return
                }
                guard newText.count == length else { return }
                newText.enumerated().forEach { fields[$0.offset].text = "\($0.element)" }
            }
        }

        /// The border width of the text fields layer.
        public var fieldsBorderWidth: CGFloat = 1 {
            didSet {
                fields.forEach { $0.layer.borderWidth = fieldsBorderWidth }
            }
        }

        /// The border color of the text fields layer
        public var fieldsBorderColor: UIColor? {
            didSet {
                fields.forEach { $0.layer.borderColor = fieldsBorderColor?.cgColor }
            }
        }

        /// The background color of text fields.
        public var fieldsBackgroundColor: UIColor? {
            didSet {
                fields.forEach { $0.backgroundColor = fieldsBackgroundColor }
            }
        }

        /// The text color of text fields.
        public var fieldsTextColor: UIColor? {
            didSet {
                fields.forEach { $0.textColor = fieldsTextColor }
            }
        }

        // MARK: - Private

        private lazy var fields: [Field] = {
            let range = 0..<length
            let fields = range.map(createField)
            range.forEach { index in
                fields[index].previousField = fields[safe: index - 1]
                fields[index].nextField = fields[safe: index + 1]
            }
            return fields
        }()

        internal var textFields: [UITextField] { fields }

        private lazy var stackView: UIStackView = {
            let view = UIStackView(arrangedSubviews: fields)
            view.axis = .horizontal
            view.distribution = .fillEqually
            view.alignment = .fill
            view.spacing = 16
            return view
        }()

        private func createField(index: Int) -> Field {
            let field = Field()
            field.delegate = self
            field.keyboardType = .numberPad
            field.textAlignment = .center
            field.isSecureTextEntry = isSecureTextEntry
            field.font = .systemFont(ofSize: 32, weight: .semibold)
            field.textColor = .black
            field.layer.borderColor = UIColor.lightGray.cgColor
            field.layer.borderWidth = 1
            field.layer.cornerRadius = 8
            return field
        }
    }

    private final class Field: UITextField {
        weak var previousField: Field?
        weak var nextField: Field?

        override func deleteBackward() {
            if text == "" {
                previousField?.becomeFirstResponder()
            }
        }
    }
}

// MARK: - UITextFieldDelegate

extension Authentication.Design.OTPInput: UITextFieldDelegate {
    /// Asks the delegate if the specified text should be changed.
    /// - Parameters:
    ///   - textField: The text field containing the text.
    ///   - range: The range of characters to be replaced.
    ///   - string: The replacement string for the specified range.
    /// - Returns: `true` if the specified text range should be replaced; otherwise, `false` to keep the old text.
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard string.count <= 1 else { return false }
        guard let field = textField as? Authentication.Design.Field else { return false }

        switch range.length {
        case 0:
            if let nextField = field.nextField {
                nextField.becomeFirstResponder()
            } else {
                field.resignFirstResponder()
            }
            field.text = string
            sendActions(for: .valueChanged)
            return false
        case 1:
            field.previousField?.becomeFirstResponder()
            field.text = ""
            sendActions(for: .valueChanged)
            return false
        default:
            return false
        }
    }
}

private extension Collection {
    subscript(safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
