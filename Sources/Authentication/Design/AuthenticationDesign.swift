//
//  Created by Backbase R&D B.V. on 7/5/2020.
//

import UIKit
import BackbaseDesignSystem

public extension Authentication {
    /// Journey design-related configurations.
    class Design {
        /// Colors.
        public var colors = Colors()

        /// Styles.
        public lazy var styles = Styles(colors: colors)
    }
}
