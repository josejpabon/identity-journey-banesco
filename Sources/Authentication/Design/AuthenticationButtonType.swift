//
//  Created by Backbase R&D B.V. on 26/10/2020.
//

import BackbaseDesignSystem
import UIKit
import SnapKit

extension Authentication.Design {

    /// An enum representing different types of button styles.
    public enum ButtonType: CaseIterable {
        /// Primary button style.
        case primary
        /// Secondary button style.
        case secondary
        /// Success button style.
        case success
        /// Warning button style.
        case warning
        /// Danger button style.
        case danger
        /// Info button style.
        case info
        /// Plain button style.
        case plain
    }
}

internal extension Authentication.Design.ButtonType {
    /// Background color for button style.
    var backgroundColor: UIColor? {
        switch self {
        case .primary:
            return DesignSystem.shared.colors.primary.default
        case .secondary:
            return DesignSystem.shared.colors.secondary.default
        case .success:
            return DesignSystem.shared.colors.success.default
        case .warning:
            return DesignSystem.shared.colors.warning.default
        case .danger:
            return DesignSystem.shared.colors.danger.default
        case .info:
            return DesignSystem.shared.colors.info.default
        case .plain:
            return .clear // Use a design system color instead?
        }
    }

    /// Tint color for button style.
    var tintColor: UIColor? {
        switch self {
        case .plain:
            return DesignSystem.shared.colors.text.default
        default:
            return DesignSystem.shared.colors.surfacePrimary.default
        }
    }

    /// Corner radius for button style.
    var cornerRadius: CGFloat {
        switch self {
        case .plain:
            return 0
        default:
            return 8
        }
    }

    /// Content edge insets for button style.
    var contentEdgeInsets: UIEdgeInsets {
        switch self {
        case .plain:
            return .zero
        default:
            return .init(top: 8, left: 16, bottom: 8, right: 16)
        }
    }
}
