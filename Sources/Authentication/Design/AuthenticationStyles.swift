//
//  Created by Backbase R&D B.V. on 7/5/2020.
//

import UIKit
import BackbaseDesignSystem

public extension Authentication.Design {
    /// Journey styles.
    class Styles {
        internal init(colors: Colors) {
            self.colors = colors
        }

        /// Applied to all `BackgroundView` views.
        public lazy var background: Style<Authentication.Design.BackgroundView> = { [unowned self] view in
            view.background = .solid(self.colors.foundation.default)
        }

        /// Applied to all title labels.
        public lazy var title: Style<UILabel> = { [unowned self] label in
            label.numberOfLines = 0
            label.textAlignment = .center
            label.font = .preferredFont(forTextStyle: .title1, weight: .bold)
            label.textColor = self.colors.foundation.onFoundation.default
        }

        /// Applied to all subtitle labels.
        public lazy var subtitle: Style<UILabel> = { [unowned self] label in
            label.numberOfLines = 0
            label.textAlignment = .center
            label.font = .preferredFont(forTextStyle: .subheadline)
            label.textColor = self.colors.foundation.onFoundation.default
        }

        /// Applied to all image views.
        public lazy var image: Style<UIImageView> = { [unowned self] image in
            image.contentMode = .scaleAspectFit
            image.tintColor = self.colors.foundation.onFoundation.default
        }

        /// Applied to all form labels.
        public lazy var formLabel: Style<UILabel> = { [unowned self] label in
            DesignSystem.shared.styles.formLabel(label)
            label.textColor = self.colors.foundation.onFoundation.default
        }
        
        /// Applied to all form text inputs.
        public lazy var formTextInput: StyleSelector<UITextContentType, TextInput> = { [unowned self] type in
            return { input in

                switch type {
                case .newPassword, .password:
                    DesignSystem.shared.styles.passwordTextInput(input)
                case .emailAddress, .username:
                    DesignSystem.shared.styles.textInput(input)
                    input.textField.keyboardType = type == .emailAddress ? .emailAddress : .asciiCapable
                    input.textField.autocorrectionType = .no
                    input.textField.autocapitalizationType = .none
                default:
                    DesignSystem.shared.styles.textInput(input)
                }

                input.textField.backgroundColor = self.colors.surfacePrimary.default
                input.textField.textColor = self.colors.surfacePrimary.onSurfacePrimary.default
                input.textField.tintColor = self.colors.surfacePrimary.onSurfacePrimary.default
                
                input.errorStyle = { input in
                    input.textField.layer.borderWidth = 1
                    input.textField.layer.borderColor = DesignSystem.shared.colors.danger.default.cgColor
                }
                
                input.textField.snp.makeConstraints { make in
                    make.height.equalTo(DesignSystem.shared.sizer.sm * 6)
                }
            }
        }

        /// Applied to all OTP input views.
        public lazy var otpInput: Style<Authentication.Design.OTPInput> = { [unowned self] input in
            input.fieldsBorderColor = nil
            input.fieldsBorderWidth = 0
            input.fieldsBackgroundColor = self.colors.surfacePrimary.default
            input.fieldsTextColor = self.colors.surfacePrimary.onSurfacePrimary.default
        }

        public lazy var loading: Style<UIActivityIndicatorView> = { [unowned self] view in
            DesignSystem.shared.styles.loadingIndicator(view)
            view.color = self.colors.foundation.onFoundation.default
        }

        /// Applied to all buttons.
        public lazy var button: StyleSelector<Authentication.Design.ButtonType, Button> = { [unowned self] type in
            return { button in
                switch type {
                case .primary:
                    DesignSystem.shared.styles.primaryButton(button)
                    button.normalBackgroundColor = self.colors.highContrast.primary
                    button.highlightedBackgroundColor = self.colors.highContrast.primary
                    button.setTitleColor(self.colors.highContrast.tint, for: .normal)
                    button.setTitleColor(self.colors.highContrast.tint, for: .highlighted)
                    button.tintColor = self.colors.highContrast.tint
                case .plain:
                    DesignSystem.shared.styles.linkButton(button)
                    button.setTitleColor(self.colors.foundation.onFoundation.default, for:
                        .normal)
                    button.setTitleColor(self.colors.foundation.onFoundation.default, for: .highlighted)
                    button.tintColor = self.colors.foundation.onFoundation.default
                    
                default:
                    break
                }
                
                DesignSystem.shared.styles.loadingIndicator(button.indicator)
            }
        }

        // MARK: - Private

        private var colors: Colors
    }
}
