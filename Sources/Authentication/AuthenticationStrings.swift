//
//  Created by Backbase R&D B.V. on 23/07/2019.
//

import Foundation

public extension Authentication {
    /// Authentication journey strings.
    struct Strings {
        /// Create a new strings object with default values.
        public init() {
            // no code required.
        }

        /// authentication.alerts.{error}.title
        public var alertTitle: Authentication.ErrorFormatter<LocalizedString> = { error in
            switch error {
            case .notConnected:
                return localized(key: "alerts.notConnected.title")
            case .invalidCredentials:
                return localized(key: "alerts.invalidCredentials.title")
            case .invalidPasscode:
                return localized(key: "alerts.invalidPasscode.title")
            case .sessionExpired:
                return localized(key: "alerts.sessionExpired.title")
            case .accountLocked:
                return localized(key: "alerts.accountLocked.title")
            case .cancelledByUser:
                return localized(key: "alerts.cancelledByUser.title")
            case .incompleteEnrollment:
                return localized(key: "alerts.incompleteEnrollment.title")
            case .passcodeMismatch:
                return localized(key: "alerts.passcodeMismatch.title")
            case .biometricUsageDenied:
                return localized(key: "alerts.biometricUsageDenied.title")
            case .biometricLockout:
                return localized(key: "alerts.biometricLockout.title")
            case .invalidEmail:
                return localized(key: "alerts.invalidEmail.title")
            case .sdk(let error):
                return localized(key: "alerts.unknown.title")
            }
        }

        /// authentication.alerts.{error}.message
        public var alertMessage: Authentication.ErrorFormatter<LocalizedString> = { error in
            switch error {
            case .notConnected:
                return localized(key: "alerts.notConnected.message")
            case .invalidCredentials:
                return localized(key: "alerts.invalidCredentials.message")
            case .invalidPasscode:
                return localized(key: "alerts.invalidPasscode.message")
            case .sessionExpired:
                return localized(key: "alerts.sessionExpired.message")
            case .accountLocked:
                return localized(key: "alerts.accountLocked.message")
            case .cancelledByUser:
                return localized(key: "alerts.cancelledByUser.message")
            case .incompleteEnrollment:
                return localized(key: "alerts.incompleteEnrollment.message")
            case .passcodeMismatch:
                return localized(key: "alerts.passcodeMismatch.message")
            case .biometricUsageDenied:
                return localized(key: "alerts.biometricUsageDenied.message")
            case .biometricLockout:
                return localized(key: "alerts.biometricLockout.message")
            case .invalidEmail:
                return localized(key: "alerts.invalidEmail.message")
            case .sdk(let error):
            #if DEBUG
                return LocalizedString(value: error.localizedDescription)
            #else
                return localized(key: "alerts.unknown.message")
            #endif
            }
        }

        /// authentication.alerts.options.confirm
        public var alertConfirmOption = localized(key: "alerts.options.confirm")

        /// authentication.alerts.options.retry
        public var alertRetryOption = localized(key: "alerts.options.retry")

        /// authentication.alerts.options.cancel
        public var alertCancelOption = localized(key: "alerts.options.cancel")

        /// authentication.alerts.options.settings
        public var alertSettingsOption = localized(key: "alerts.options.settings")

        private static func localized(key: String) -> LocalizedString {
            let prefix = "authentication."
            return LocalizedString(key: prefix + key, in: .authentication)
        }
    }
}
