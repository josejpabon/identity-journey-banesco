//
//  Created by Backbase R&D B.V. on 14/5/2020.
//

import Foundation

/// Localization helper used to make resolving localization value easier across multiple bundles.
public struct LocalizedString: ExpressibleByStringLiteral {
    /// Creates a new localized string with an exact value
    /// - Parameter value: Value of the localized string
    public init(value: String) {
        self.key = value
        self.bundles = []
    }

    /// Creates a new localized string by using the value as a key for the translation.
    /// - Parameter value: The value to be used as a localization key.
    public init(stringLiteral value: String) {
        self = LocalizedString(key: value)
    }

    /// Creates a new localized string by using the key to find the translation in the given bundles.
    /// - Parameters:
    ///   - key: The localization key for the lookup
    ///   - bundles: List of bundles to go through. Bundles will automatically include the `main` bundle.
    public init(key: String, in bundles: Bundle?...) {
        self.key = key
        self.bundles = ([.main] + bundles).compactMap { $0 }
    }

    /// Called when the struct is used as a function.
    /// - Returns: The localization value
    public func callAsFunction() -> String {
        return bundles
            .compactMap { bundle -> String? in
                let localized = bundle.localizedString(forKey: key, value: key, table: nil)
                if localized == key {
                    return nil
                }
                return localized
            }
            .first ?? key
    }

    /// Value of the localized string
    public var value: String { callAsFunction() }

    // MARK: Private

    private let key: String
    private let bundles: [Bundle]
}
