//
//  Created by Backbase R&D B.V. on 3/24/20.
//

import UIKit
import RxSwift
import RxCocoa
import Resolver
import LocalAuthentication

internal final class LoginViewModel {
    init(session: Session) {
        self.session = session
    }

    func bind(
        authenticateWithBiometrics: Observable<Void>,
        authenticateWithPasscode: Observable<Void>,
        canShowSessionAlert: Observable<Bool>
    ) -> (
        image: Driver<UIImage?>,
        title: Driver<String>,
        subtitle: Driver<String>,
        biometricButtonTitle: Driver<String>,
        passcodeButtonTitle: Driver<String>,
        biometricEnabled: Driver<Bool>,
        passcodeEnabled: Driver<Bool>,
        errorAlert: Signal<UIAlertController>,
        biometricLoading: Driver<Bool>,
        passcodeLoading: Driver<Bool>
    ) {
        canShowSessionAlert
            .compactMap { [weak self] _ in self?.session }
            .compactMap { [weak self] session -> UIAlertController? in
                switch session {
                case .expired:
                    return self?.sessionExpiredAlertController
                default:
                    return nil
                }
            }
            .bind(to: errorAlertRelay)
            .disposed(by: disposeBag)

        authenticateWithBiometrics
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.startBiometricsFlow()
            })
            .disposed(by: disposeBag)

        authenticateWithPasscode
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.startPasscodeFlow()
            })
            .disposed(by: disposeBag)

        return (
            image: .just(configuration.login.image),
            title: .just(configuration.login.strings.title()),
            subtitle: .just(subtitle(forcePasscode: !isBiometricSupported)),
            biometricButtonTitle: .just(biometricButtonTitle()),
            passcodeButtonTitle: .just(configuration.login.strings.passcodeButtonTitle()),
            biometricEnabled: .just(isBiometricSupported),
            passcodeEnabled: .just(isPasscodeSupported),
            errorAlert: errorAlertRelay.asSignal(),
            biometricLoading: biometricLoadingRelay.asDriver(onErrorJustReturn: false),
            passcodeLoading: passcodeLoadingRelay.asDriver(onErrorJustReturn: false)
        )
    }

    func startBiometricsFlow() {
        biometricLoadingRelay.accept(true)
        authenticationUseCase.authenticateWithBiometrics { [weak self] result in
            self?.biometricLoadingRelay.accept(false)
            switch result {
            case .success:
                break
            case .failure(let error):
                guard let self = self else { return }
                if error != .cancelledByUser {
                    self.errorAlertRelay.accept(self.alertController(for: error))
                }
            }
        }
    }

    func startPasscodeFlow() {
        passcodeLoadingRelay.accept(true)
        authenticationUseCase.authenticateWithPasscode { [weak self] result in
            self?.passcodeLoadingRelay.accept(false)
            switch result {
            case .success:
                break
            case .failure(let error):
                guard let self = self else { return }
                if error != .cancelledByUser {
                    self.errorAlertRelay.accept(self.alertController(for: error))
                }
            }
        }
    }

    func subtitle(for type: BiometryType = .allowed, forcePasscode: Bool) -> String {
        if forcePasscode {
            return configuration.login.strings.subtitle(.none)()
        }
        return configuration.login.strings.subtitle(type)()
    }

    var isBiometricSupported: Bool {
        guard configuration.login.biometricEnabled else { return false }
        if BiometryType.allowed == .lockedOut { return true }
        guard authenticationUseCase.isBiometricEnrolled else { return false }
        return [.faceID, .touchID].contains(BiometryType.allowed)
    }

    var isPasscodeSupported: Bool {
        return authenticationUseCase.isPasscodeEnrolled
    }

    func biometricButtonTitle(for type: BiometryType = .allowed) -> String {
        return configuration.login.strings.biometricButtonTitle(type)?() ?? ""
    }

    var sessionExpiredAlertController: UIAlertController {
        let alert = UIAlertController(
            title: configuration.strings.alertTitle(.sessionExpired)(),
            message: configuration.strings.alertMessage(.sessionExpired)(),
            preferredStyle: .alert
        )

        let dimissAction = UIAlertAction(title: configuration.strings.alertConfirmOption(), style: .default)
        alert.addAction(dimissAction)

        return alert
    }

    func alertController(for error: Authentication.Error) -> UIAlertController {
        let alert = UIAlertController(
            title: configuration.strings.alertTitle(error)(),
            message: configuration.strings.alertMessage(error)(),
            preferredStyle: .alert
        )

        let dimissAction = UIAlertAction(title: configuration.strings.alertConfirmOption(), style: .default)
        alert.addAction(dimissAction)

        return alert
    }

    // MARK: - Private

    private let session: Session

    @LazyInjected
    private var configuration: Authentication.Configuration

    @LazyInjected
    private var authenticationUseCase: AuthenticationUseCase

    private let passcodeLoadingRelay = PublishRelay<Bool>()
    private let biometricLoadingRelay = PublishRelay<Bool>()
    private let errorAlertRelay = PublishRelay<UIAlertController>()
    private let disposeBag = DisposeBag()
}
