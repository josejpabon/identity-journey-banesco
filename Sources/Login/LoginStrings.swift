//
//  Created by Backbase R&D B.V. on 15/5/2020.
//

import Foundation

public extension Login {
    /// Login strings.
    struct Strings {
        /// Create a new strings object with default values.
        public init() {
            // no code required.
        }

        /// authentication.login.labels.title
        public var title = localized(key: "labels.title")

        /// authentication.login.labels.subtitle.{biometryType}
        /// or authentication.login.labels.subtitle.passcode if biometric authentication is not supported.
        public var subtitle: (BiometryType) -> LocalizedString = { type in
            switch type {
            case .faceID:
                return localized(key: "labels.subtitle.faceID")
            case .touchID:
                return localized(key: "labels.subtitle.touchID")
            case .lockedOut:
                return localized(key: "labels.subtitle.lockedOut")
            default:
                return localized(key: "labels.subtitle.passcode")
            }
        }

        /// authentication.login.buttons.biometric.{biometryType}
        public var biometricButtonTitle: (BiometryType) -> LocalizedString? = { type in
            switch type {
            case .faceID:
                return localized(key: "buttons.biometric.faceID")
            case .touchID:
                return localized(key: "buttons.biometric.touchID")
            case .lockedOut:
                return localized(key: "buttons.biometric.lockedOut")
            default:
                return nil
            }
        }

        /// authentication.login.buttons.passcode
        public var passcodeButtonTitle = localized(key: "buttons.passcode")

        private static func localized(key: String) -> LocalizedString {
            let prefix = "authentication.login."
            return LocalizedString(key: prefix + key, in: .authentication)
        }
    }
}
