//
//  Created by Backbase R&D B.V. on 24/4/20.
//

import UIKit

public extension Login {
    /// Login design-related configurations.
    struct Design {
        internal init(journeyDesign: Authentication.Design) {
            self.journeyDesign = journeyDesign
        }

        /// Applied to all `BackgroundView` views.
        public lazy var background = journeyDesign.styles.background

        /// Applied to all image views in login screens.
        public lazy var image = journeyDesign.styles.image

        /// Applied to all title labels in login screens.
        public lazy var title = journeyDesign.styles.title

        /// Applied to all subtitle labels in login screens.
        public lazy var subtitle = journeyDesign.styles.subtitle

        /// Applied to biometric button in login screens.
        public lazy var biometricButton = journeyDesign.styles.button(.primary)

        /// Applied to passcode button in login screens.
        public lazy var passcodeButton = journeyDesign.styles.button(.plain)

        // MARK: - Private

        private let journeyDesign: Authentication.Design
    }
}
