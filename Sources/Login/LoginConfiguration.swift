//
//  Created by Backbase R&D B.V. on 3/24/20.
//

import UIKit
import LocalAuthentication

public extension Login {
    /// Authentication journey login configurations.
    struct Configuration {
        internal init(journeyDesign: Authentication.Design) {
            self.journeyDesign = journeyDesign
        }

        /// Whether to allow biometrics or not (default is `true`).
        public var biometricEnabled = true

        /// Login screen image (default is `UIImage.named("LoginLogo", in: .authentication)`)
        public var image = UIImage.named("LoginLogo", in: .authentication)

        /// Login design configurations.
        public lazy var design = Design(journeyDesign: journeyDesign)

        /// Login strings.
        public var strings = Strings()

        // MARK: - Private

        private let journeyDesign: Authentication.Design
    }
}
