//
//  Created by Backbase R&D B.V. on 3/24/20.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit
import Resolver
import BackbaseDesignSystem

internal final class LoginViewController: UIViewController {
    func bind(viewModel: LoginViewModel) {
        self.viewModel = viewModel

        let output = viewModel.bind(
            authenticateWithBiometrics: biometricButton.rx.tap.asObservable(),
            authenticateWithPasscode: passcodeButton.rx.tap.asObservable(),
            canShowSessionAlert: self.rx.viewDidAppear.asObservable()
        )

        output.errorAlert
            .emit(onNext: { [weak self] alert in
                self?.present(alert, animated: true)
            })
            .disposed(by: disposeBag)

        disposeBag.insert(
            output.image.drive(imageView.rx.image),
            output.title.drive(titleLabel.rx.text),
            output.subtitle.drive(subtitleLabel.rx.text),
            output.biometricButtonTitle.drive(biometricButton.rx.title()),
            output.passcodeButtonTitle.drive(passcodeButton.rx.title()),
            output.biometricEnabled.map({ !$0 }).drive(biometricButton.rx.isHidden),
            output.passcodeEnabled.map({ !$0 }).drive(passcodeButton.rx.isHidden),
            output.biometricLoading.drive(biometricButton.rx.isAnimating),
            output.passcodeLoading.drive(passcodeButton.rx.isAnimating)
        )

        output
            .biometricLoading.drive(onNext: { [weak self] loading in
                self?.view.isUserInteractionEnabled = !loading
            })
            .disposed(by: disposeBag)

        output
            .passcodeLoading.drive(onNext: { [weak self] loading in
                self?.view.isUserInteractionEnabled = !loading
            })
            .disposed(by: disposeBag)
    }

    // MARK: - View lifecycle methods

    override func loadView() {
        super.loadView()

        view.addSubviews(backgroundView, imageView, titleLabel, subtitleLabel, buttonsStackView)

        backgroundView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        let safeArea = view.safeAreaLayoutGuide

        imageView.snp.makeConstraints { make in
            make.height.equalTo(DesignSystem.shared.sizer.sm * 9)
            make.top.equalTo(safeArea).offset(DesignSystem.shared.spacer.sm * 10)
            make.leading.trailing.equalTo(safeArea).inset(DesignSystem.shared.spacer.xl)
        }

        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(imageView.snp.bottom).offset(DesignSystem.shared.spacer.xl)
            make.leading.trailing.equalTo(safeArea).inset(DesignSystem.shared.spacer.md)
        }

        subtitleLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(DesignSystem.shared.spacer.md)
            make.leading.trailing.equalTo(titleLabel)
        }

        biometricButton.snp.makeConstraints { make in
            make.height.equalTo(DesignSystem.shared.sizer.sm * 6)
        }

        buttonsStackView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(safeArea).inset(DesignSystem.shared.spacer.xl)
            make.bottom.equalTo(safeArea).inset(DesignSystem.shared.spacer.md)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configuration.login.design.background(backgroundView)
        configuration.login.design.image(imageView)
        configuration.login.design.title(titleLabel)
        configuration.login.design.subtitle(subtitleLabel)
        configuration.login.design.biometricButton(biometricButton)
        configuration.login.design.passcodeButton(passcodeButton)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    // MARK: - Private

    private lazy var backgroundView = Authentication.Design.BackgroundView(frame: view.frame)

    private lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.accessibilityIdentifier = "login.imageView"
        return view
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "login.titleLabel"
        return label
    }()

    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "login.subtitleLabel"
        return label
    }()

    private lazy var biometricButton: Button = {
        let button = Button()
        button.accessibilityIdentifier = "login.biometricButton"
        return button
    }()

    private lazy var passcodeButton: Button = {
        let button = Button()
        button.accessibilityIdentifier = "login.passcodeButton"
        return button
    }()

    private lazy var buttonsStackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [biometricButton, passcodeButton])
        view.axis = .vertical
        view.distribution = .fill
        view.alignment = .fill
        view.spacing = DesignSystem.shared.spacer.md
        return view
    }()

    private var viewModel: LoginViewModel?
    private let disposeBag = DisposeBag()

    @LazyInjected
    private var configuration: Authentication.Configuration

}
