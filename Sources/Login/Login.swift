//
//  Created by Backbase R&D B.V. on 3/24/20.
//

import UIKit

/// Authentication namespace used for grouping login level configuration.
public struct Login {
    /// Build a login view controller from a session state.
    /// - Parameter session: Session state (default is `Session.none`).
    /// - Returns: view controller building closure.
    public static func build(session: Session = .none) -> (UINavigationController) -> UIViewController {
        return { _ in
            let viewModel = LoginViewModel(session: session)
            let viewController = LoginViewController()
            viewController.bind(viewModel: viewModel)
            return viewController
        }
    }
}
