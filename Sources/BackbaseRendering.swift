//
//  Created by Backbase R&D B.V. on 3/24/20.
//

import UIKit
import Backbase

internal final class RenderableContainerView: UIView {
    weak var viewController: RenderingViewController?
}

internal protocol RenderableView: UIView {
    var viewController: RenderingViewController? { get }
    var visibleView: UIView? { get set }
}

extension RenderableView {
    var viewController: RenderingViewController? {
        guard let container = superview as? RenderableContainerView else { return nil }
        return container.viewController
    }
}

internal final class RenderingViewController: UIViewController {
    var renderer: Renderer?
    weak var renderable: Renderable?

    required init(withRenderable renderable: Renderable?) {
        super.init(nibName: nil, bundle: nil)
        self.renderable = renderable
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError()
    }

    override func loadView() {
        let container = RenderableContainerView()
        container.viewController = self
        self.view = container
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.setNavigationBarHidden(true, animated: false)
        
        if #available(iOS 13.0, *) {
            isModalInPresentation = true
        }

        if let renderable = self.renderable {
            do {
                renderer = try BBRendererFactory.renderer(forItem: renderable)
                try renderer?.start(view)
            } catch {
                print(error.localizedDescription)
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        renderer?.willAppear()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        renderer?.didAppear()
        isWaitingToCompleteAuthentication.accept(true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
        renderer?.willDisappear()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        renderer?.didDisappear()
    }
}
