//
//  Created by Backbase R&D B.V. on 21/08/2020.
//

import UIKit

/// Possible states of a session.
public enum Session {
    /// There is no session at the moment.
    case none
    /// There is at least one valid session at the moment.
    case valid
    /// The current session expired.
    case expired
    /// The account was locked.
    case locked
}
