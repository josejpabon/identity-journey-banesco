//
//  Created by Backbase R&D B.V. on 21/8/2020.
//

import BackbaseIdentity

internal struct Authenticators {
    let device = BBIDDeviceAuthenticator()
    let biometric = Biometric.authenticator
    let passcode = Passcode.authenticator
    let inputRequired = InputRequired.authenticator

    func registerAll(using client: BBIDAuthClient) {
        try? client.add(biometric)
        try? client.add(passcode)
        try? client.add(device)
        try? client.add(inputRequired)
    }

    func prepareForBiometric(in client: BBIDAuthClient) {
        try? client.removeAuthenticator(byType: BBIDPasscodeAuthenticator.self)
        try? client.add(biometric)
    }

    func prepareForPasscode(in client: BBIDAuthClient) {
        try? client.removeAuthenticator(byType: BBIDBiometricAuthenticator.self)
        try? client.add(passcode)
    }
}
