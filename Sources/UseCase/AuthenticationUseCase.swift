//
//  Created by Backbase R&D B.V. on 18/12/2019.
//

import Foundation

/// Conform to this protocol to perform authentication-related operations.
public protocol AuthenticationUseCase {
    typealias Headers = [String: String]
    typealias AuthenticationResult = Result<Headers, Authentication.Error>
    typealias AuthenticationHandler = (AuthenticationResult) -> Void

    typealias ForgottenCredentialsResult = Result<Data, Authentication.Error>
    typealias ForgottenCredentialsHandler = (ForgottenCredentialsResult) -> Void

    typealias PasscodeChangeResult = Result<Void, Authentication.Error>
    typealias PasscodeChangeHandler = (PasscodeChangeResult) -> Void

    typealias SessionHandler = (Session) -> Void

    /// Start the registration flow for a given `User` object.
    /// - Parameters:
    ///   - user: user.
    ///   - preCompletion: an optional handler to be called before the flow finishes.
    ///   - postCompletion: an optional callback that will be called after the flow finishes.
    func authenticate(user: User, preCompletion: (() -> Void)?, postCompletion: AuthenticationHandler?)

    /// Start authentication with biometrics for the authenticated user.
    /// - Parameter callback: an optional callback that will be called after the flow finishes.
    func authenticateWithBiometrics(callback: AuthenticationHandler?)

    /// Start authentication with passcode for the authenticated user.
    /// - Parameter callback: an optional callback that will be called after the flow finishes.
    func authenticateWithPasscode(callback: AuthenticationHandler?)

    /// Start the "complete registration" flow for the authenticated user.
    /// - Parameter callback: an optional callback that will be called after the flow finishes.
    func completeRegistration(callback: AuthenticationHandler?)

    /// Start the "change passcode" flow for the authenticated user.
    /// - Parameter callback: an optional callback that will be called after the flow finishes.
    func changePasscode(callback: PasscodeChangeHandler?)

    /// Start the "forgot username" flow for the registered user.
    /// - Parameter callback: an optional callback that will be called after the flow finishes.
    func forgotUsername(callback: ForgottenCredentialsHandler?)

    /// End current session for the authenticated user.
    /// - Parameter callback: an optional callback that will be called after the operation finishes.
    func endSession(callback: SessionHandler?)

    /// End current session for the authenticated user and reset the authentication flow.
    /// - Parameter callback: an optional callback that will be called after the operation finishes.
    func logOut(callback: SessionHandler?)

    /// End current session for the authenticated user and set the session state to `Session.expired`.
    /// - Parameter callback: an optional callback that will be called after the operation finishes.
    func expireSession(callback: SessionHandler?)

    /// End current session for the authenticated user and set the session state to `Session.locked`.
    /// - Parameter callback: an optional callback that will be called after the operation finishes.
    func lockAccount(callback: SessionHandler?)

    /// Check session validity.
    /// - Parameter callback: an optional callback that will be called after the operation finishes.
    func validateSession(callback: SessionHandler?)

    /// Cached user's username.
    var cachedUsername: String? { get }

    /// Whether there is a registered user that the use case can use for login.
    var isEnrolled: Bool { get }

    /// Whether there is a registered user that has finished registration and biometrics setup.
    var isBiometricEnrolled: Bool { get }

    /// Whether there is a registered user that has finished registration and passcode setup.
    var isPasscodeEnrolled: Bool { get }

    /// Last known session state.
    var lastSession: Session? { get }
}
