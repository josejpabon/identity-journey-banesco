//
//  Created by Backbase R&D B.V. on 3/26/20.
//

import Backbase
import BackbaseIdentity
import UIKit

// swiftlint:disable file_length

/// Single class to handle all authentication-related operations.
/// - Conforms to:
///     - `AuthenticationUseCase`.
public final class IdentityAuthenticationUseCase: BBIDAuthClient {
    /// Create a new instance of the use case.
    /// - Parameters:
    ///   - clientSecret: an optional client secret (default is `nil`).
    ///   - sessionChangeHandler: an optional handler that will be called with the session changes.
    ///   - navigationHandler: Optional navigation handler that will be called with additional screens
    public required init(
        clientSecret: String? = nil,
        sessionChangeHandler: SessionHandler?
    ) {
        super.init(clientSecret: clientSecret)
        self.sessionChangeHandler = sessionChangeHandler
        authenticators.registerAll(using: self)
    }

    /// Last known session state.
    public private(set) var lastSession: Session?

    /// Create a navigation event from a notification.
    /// - Parameter notification: notification.
    /// - Returns: Optional navigation event.
    public func navigation(for notification: Notification) -> Authentication.Navigation? {
        let factory = IdentityAuthenticationNavigationFactory(client: self)
        return factory.navigation(for: notification)
    }

    /// After executing the user will be unable to authenticate using the previously approved authenticators
    /// and will be force to enroll again.
    public override func reset() {
        storage.value = nil
        super.reset()
    }

    internal var sessionChangeHandler: SessionHandler?

    // used to keep a reference to sdk delegates proxies
    internal var proxies: [String: NSObject] = [:]

    // MARK: - Authenticators
    private lazy var authenticators = Authenticators()

    // MARK: - Encrypted storage
    private lazy var storage = Storage(key: "username")
}

// MARK: - AuthenticationUseCase methods

extension IdentityAuthenticationUseCase: AuthenticationUseCase {
    /// Start the registration flow for a given `User` object.
    /// - Parameters:
    ///   - user: user.
    ///   - preCompletion: an optional handler to be called before the flow finishes.
    ///   - postCompletion: an optional callback that will be called after the flow finishes.
    public func authenticate(
        user: User,
        preCompletion: (() -> Void)? = nil,
        postCompletion: AuthenticationHandler?
    ) {
        let proxy = PasswordAuthClientResumingDelegateProxy.create(
            handler: { [weak self] result in
                if (try? result.get()) != nil {
                    self?.storage.value = user.username
                    self?.validateSession()
                }
                postCompletion?(result)
            },
            resumingHandler: preCompletion
        )
        proxies["authenticate"] = proxy
        authenticate(withUserId: user.username,
                     credentials: user.password,
                     headers: nil,
                     additionalBodyParameters: nil,
                     tokenNames: [],
                     delegate: proxy)
    }

    /// Start authentication with biometrics for the authenticated user.
    /// - Parameter callback: an optional callback that will be called after the flow finishes.
    public func authenticateWithBiometrics(callback: AuthenticationHandler? = nil) {
        guard let username = cachedUsername else { return }
        authenticators.prepareForBiometric(in: self)
        let proxy = PasswordAuthClientDelegateProxy(handler: { [weak self] result in
            guard let self = self else { return }
            if (try? result.get()) != nil {
                self.validateSession()
            }
            self.authenticators.registerAll(using: self)
            callback?(result.mapError { Authentication.Error(biometric: $0) })
        })
        proxies["authenticateWithBiometrics"] = proxy
        authenticateRegisteredDevice(withUsername: username, delegate: proxy)
    }

    /// Start authentication with passcode for the authenticated user.
    /// - Parameter callback: an optional callback that will be called after the flow finishes.
    public func authenticateWithPasscode(callback: AuthenticationHandler? = nil) {
        guard let username = cachedUsername else { return }
        authenticators.prepareForPasscode(in: self)
        let proxy = PasswordAuthClientDelegateProxy(handler: { [weak self] result in
            guard let self = self else { return }
            if (try? result.get()) != nil {
                self.validateSession()
            }
            callback?(result.mapError(Authentication.Error.init(passcode:)))
            self.authenticators.registerAll(using: self)
        })
        proxies["authenticateWithPasscode"] = proxy
        authenticateRegisteredDevice(withUsername: username, delegate: proxy)
    }

    /// Start the "complete registration" flow for the authenticated user.
    /// - Parameter callback: an optional callback that will be called after the flow finishes.
    public func completeRegistration(callback: AuthenticationHandler?) {
        guard let username = cachedUsername else { return }
        let proxy = BBIDFIDORegistrationDelegateProxy { callback?($0.map { [:] }) }
        proxies["completeRegistration"] = proxy
        self.startUAFRegistration(withUsername: username, delegate: proxy)
    }

    /// Start the "change passcode" flow for the authenticated user.
    /// - Parameter callback: an optional callback that will be called after the flow finishes.
    public func changePasscode(callback: PasscodeChangeHandler? = nil) {
        guard let username = cachedUsername else { return }
        let proxy = BBIDPasscodeChangeDelegateProxy { callback?($0.mapError(Authentication.Error.init(passcode:))) }
        proxies["changePasscode"] = proxy
        BBIDPasscodeManager.changePasscode(username, authClient: self, delegate: proxy)
    }

    /// Start the "forgot username" flow for the registered user.
    /// - Parameter callback: an optional callback that will be called after the flow finishes.
    public func forgotUsername(callback: ForgottenCredentialsHandler? = nil) {
        let proxy = BBIDForgottenCredentialsDelegateProxy(usernameHandler: { callback?($0) }, passwordHandler: nil)
        proxies["forgotUsername"] = proxy
        BBIDForgottenCredentialsManager.retrieveForgottenUsername(proxy)
    }

    /// End current session for the authenticated user.
    /// - Parameter callback: an optional callback that will be called after the operation finishes.
    public func endSession(callback: SessionHandler? = nil) {
        let proxy = AuthClientDelegateProxy { [weak self] state in
            let session = state.session
            self?.lastSession = session
            self?.sessionChangeHandler?(session)
            callback?(session)
        }
        proxies["endSession"] = proxy
        self.endSession(with: proxy)
    }

    /// End current session for the authenticated user and reset the authentication flow.
    /// - Parameter callback: an optional callback that will be called after the operation finishes.
    public func logOut(callback: SessionHandler?) {
        self.reset()
        let proxy = AuthClientDelegateProxy { [weak self] state in
            let session = state.session
            self?.lastSession = session
            self?.sessionChangeHandler?(session)
            callback?(session)
        }
        proxies["logOut"] = proxy
        self.endSession(with: proxy)
    }

    /// End current session for the authenticated user and set the session state to `Session.expired`.
    /// - Parameter callback: an optional callback that will be called after the operation finishes.
    public func expireSession(callback: SessionHandler? = nil) {
        let proxy = AuthClientDelegateProxy { [weak self] _ in
            self?.lastSession = .expired
            self?.sessionChangeHandler?(.expired)
            callback?(.expired)
        }
        proxies["expireSession"] = proxy
        self.endSession(with: proxy)
    }

    /// End current session for the authenticated user and set the session state to `Session.locked`.
    /// - Parameter callback: an optional callback that will be called after the operation finishes.
    public func lockAccount(callback: SessionHandler? = nil) {
        reset()
        let proxy = AuthClientDelegateProxy { [weak self] _ in
            self?.lastSession = .locked
            self?.sessionChangeHandler?(.locked)
            callback?(.locked)
        }
        proxies["lockAccount"] = proxy
        self.endSession(with: proxy)
    }

    /// Check session validity.
    /// - Parameter callback: an optional callback that will be called after the operation finishes.
    public func validateSession(callback: SessionHandler? = nil) {
        let proxy = AuthClientDelegateProxy { [weak self] state in
            let session = state.session
            self?.lastSession = session
            self?.sessionChangeHandler?(session)
            callback?(session)
        }
        proxies["validateSession"] = proxy
        checkSessionValidity(proxy)
    }

    /// Cached user's username.
    public var cachedUsername: String? { storage.value }

    /// Whether there is a registered user that the use case can use for login.
    public var isEnrolled: Bool {
        return isDeviceRegistered() && isPasscodeEnrolled
    }

    /// Whether there is a registered user that has finished registration and biometrics setup.
    public var isBiometricEnrolled: Bool {
        guard let username = cachedUsername else { return false }
        return authenticators.biometric.isRegistered(forUsername: username)
    }

    /// Whether there is a registered user that has finished registration and passcode setup.
    public var isPasscodeEnrolled: Bool {
        guard let username = cachedUsername else { return false }
        return authenticators.passcode.isRegistered(forUsername: username)
    }
}
