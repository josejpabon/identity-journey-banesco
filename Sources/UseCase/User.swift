//
//  Created by Backbase R&D B.V. on 21/08/2020.
//

/// An object representing user's credentials.
public struct User: Equatable {
    /// Username.
    public let username: String
    /// Password.
    public let password: String

    /// Create a new `User` instance.
    /// - Parameters:
    ///   - username: User's username.
    ///   - password: User's password.
    public init(username: String, password: String) {
        self.username = username
        self.password = password
    }
}
