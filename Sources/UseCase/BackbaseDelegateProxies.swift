//
//  Created by Backbase R&D B.V. on 3/25/20.
//

import Backbase
import BackbaseIdentity

internal class PasswordAuthClientDelegateProxy: NSObject, PasswordAuthClientDelegate {
    typealias Result = Swift.Result<[String: String], Authentication.Error>
    typealias Handler = (Result) -> Void

    var handler: Handler

    required init(handler: @escaping Handler) {
        self.handler = handler
        super.init()
    }

    func authenticationDidSucceed(with headers: [String: String]) {
        handler(.success(headers))
    }

    func authenticationDidFail(with error: Error) {
        handler(.failure(.init(sdk: error)))
    }
}

internal final class PasswordAuthClientResumingDelegateProxy: PasswordAuthClientDelegateProxy {
    typealias ResumingHandler = () -> Void

    var resumingHandler: ResumingHandler

    required init(handler: @escaping Handler, resumingHandler: @escaping ResumingHandler) {
        self.resumingHandler = resumingHandler
        super.init(handler: handler)
    }

    @available(*, unavailable)
    required init(handler: @escaping Handler) {
        fatalError()
    }

    func authenticationDidRequire(action response: HTTPURLResponse) {
        resumingHandler()
    }

    static func create(handler: @escaping Handler, resumingHandler: ResumingHandler?) -> PasswordAuthClientDelegateProxy {
        if let resumingHandler = resumingHandler {
            return PasswordAuthClientResumingDelegateProxy(handler: handler, resumingHandler: resumingHandler)
        }
        return PasswordAuthClientDelegateProxy(handler: handler)
    }
}

internal final class AuthClientDelegateProxy: NSObject, AuthClientDelegate {
    typealias Handler = (SessionState) -> Void
    var handler: Handler

    required init(handler: @escaping Handler) {
        self.handler = handler
        super.init()
    }

    func sessionState(_ newSessionState: SessionState) {
        handler(newSessionState)
    }
}

internal final class BBIDPasscodeChangeDelegateProxy: NSObject, BBIDPasscodeChangeDelegate {
    typealias Result = Swift.Result<Void, Authentication.Error>
    typealias Handler = (Result) -> Void

    var handler: Handler

    required init(handler: @escaping Handler) {
        self.handler = handler
        super.init()
    }

    func passcodeChangeDidSucceed() {
        handler(.success(()))
    }

    func passcodeChangeDidFail(with error: Error) {
        handler(.failure(.init(passcode: error)))
    }
}

internal final class BBIDFIDORegistrationDelegateProxy: NSObject, BBIDFIDORegistrationDelegate {
    typealias Result = Swift.Result<Void, Authentication.Error>
    typealias Handler = (Result) -> Void

    var handler: Handler

    required init(handler: @escaping Handler) {
        self.handler = handler
        super.init()
    }

    func uafRegistrationDidSucceed() {
        handler(.success(()))
    }

    func uafRegistrationDidFail(with error: Error) {
        handler(.failure(.init(sdk: error)))
    }
}

internal final class BBIDForgottenCredentialsDelegateProxy: NSObject, BBIDForgottenCredentialsDelegate {
    typealias Result = Swift.Result<Data, Authentication.Error>
    typealias Handler = (Result) -> Void

    var usernameHandler: Handler?
    var passwordHandler: Handler?

    required init(usernameHandler: Handler?, passwordHandler: Handler?) {
        self.usernameHandler = usernameHandler
        self.passwordHandler = passwordHandler
        super.init()
    }

    func forgotUsernameDidSucceed(_ data: Data) {
        usernameHandler?(.success(data))
    }

    func forgotUsernameDidFailWithError(_ error: Error) {
        usernameHandler?(.failure(.init(sdk: error)))
    }

    func forgotPasswordDidSucceed(_ data: Data) {
        passwordHandler?(.success(data))
    }

    func forgotPasswordDidFailWithError(_ error: Error) {
        passwordHandler?(.failure(.init(sdk: error)))
    }
}
