//
//  Created by Backbase R&D B.V. on 07/09/20.
//

import UIKit

/// Authentication namespace used for grouping email sent level configuration.
public struct EmailSent {
    /// Build an email sent view controller.
    public static func build(navigationController: UINavigationController) -> UIViewController {
        let viewModel = EmailSentViewModel()
        let viewController = EmailSentViewController()
        viewController.bind(viewModel: viewModel)
        return viewController
    }
}
