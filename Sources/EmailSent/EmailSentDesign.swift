//
//  Created by Backbase R&D B.V. on 08/09/20.
//

import UIKit

public extension EmailSent {
    /// Email sent design-related configurations.
    struct Design {
        internal init(journeyDesign: Authentication.Design) {
            self.journeyDesign = journeyDesign
        }

        /// Applied to `BackgroundView` view in email sent screen.
        public lazy var background = journeyDesign.styles.background

        /// Applied to image view in email sent screen.
        public lazy var image = journeyDesign.styles.image

        /// Applied to title label in email sent screen.
        public lazy var title = journeyDesign.styles.title

        /// Applied to subtitle label in email sent screen.
        public lazy var subtitle = journeyDesign.styles.subtitle

        /// Applied to cancel button in email sent screen.
        public lazy var cancelButton = journeyDesign.styles.button(.plain)

        /// Applied to done button in email sent screen.
        public lazy var dismissButton = journeyDesign.styles.button(.primary)

        // MARK: - Private

        private let journeyDesign: Authentication.Design
    }
}
