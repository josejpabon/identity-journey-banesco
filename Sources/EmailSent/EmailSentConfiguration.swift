//
//  Created by Backbase R&D B.V. on 07/09/20.
//

import UIKit

public extension EmailSent {
    /// Authentication journey email sent configurations.
    struct Configuration {
        internal init(journeyDesign: Authentication.Design) {
            self.journeyDesign = journeyDesign
        }

        /// Image (default: `UIImage.named("EmailSent", in: .authentication, .design)`)
        public var image = UIImage.named("EmailSent", in: .authentication, .design)

        /// Cancel button icon (default: `UIImage.named("ic_close", in: .authentication, .design)`)
        public var cancelButtonIcon = UIImage.named("ic_close", in: .authentication, .design)

        /// Email sent design configurations.
        public lazy var design = Design(journeyDesign: journeyDesign)

        /// Email sent animation configurations.
        /// **Note**: Set this to` nil` to disable animations.
        public var animation: Animation? = Animation()

        /// Email sent strings.
        public var strings = Strings()

        // MARK: - Private

        private let journeyDesign: Authentication.Design
    }
}
