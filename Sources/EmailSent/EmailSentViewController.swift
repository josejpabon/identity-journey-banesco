//
//  Created by Backbase R&D B.V. on 07/09/20.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit
import Resolver
import BackbaseDesignSystem

internal final class EmailSentViewController: UIViewController {
    func bind(viewModel: EmailSentViewModel) {
        self.viewModel = viewModel

        let output = viewModel.bind(
            cancel: cancelButton.rx.tap.asObservable(),
            dismiss: dismissButton.rx.tap.asObservable()
        )

        disposeBag.insert(
            output.image.drive(imageView.rx.image),
            output.title.drive(titleLabel.rx.text),
            output.subtitle.drive(subtitleLabel.rx.text),
            output.dismissButtonTitle.drive(dismissButton.rx.title()),
            output.cancelButtonIcon.drive(cancelButton.rx.image()),
            output.dismiss.emit(onNext: { [weak self] in self?.dismiss(animated: true) })
        )
    }

    // MARK: - View lifecycle methods

    override func loadView() {
        super.loadView()

        view.addSubviews(backgroundView, imageView, stackView, cancelButton, dismissButton)

        backgroundView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        let safeArea = view.safeAreaLayoutGuide

        cancelButton.snp.makeConstraints { make in
            make.top.equalTo(safeArea).inset(DesignSystem.shared.spacer.sm)
            make.leading.equalTo(safeArea).inset(DesignSystem.shared.spacer.md)
            make.height.equalTo(DesignSystem.shared.spacer.sm * 6)
        }

        imageView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(safeArea).inset(DesignSystem.shared.spacer.md )
            make.top.greaterThanOrEqualTo(safeArea).inset(DesignSystem.shared.spacer.md )
            make.bottom.equalTo(stackView.snp.top)
        }

        stackView.snp.makeConstraints { make in
            make.top.equalTo(safeArea.snp.centerY).offset(-DesignSystem.shared.spacer.md )
            make.leading.trailing.equalTo(safeArea).inset(DesignSystem.shared.spacer.xl * 2)
            make.bottom.lessThanOrEqualTo(safeArea).inset(DesignSystem.shared.spacer.md )
        }

        dismissButton.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalTo(safeArea).inset(DesignSystem.shared.spacer.md )
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        configuration.emailSent.design.background(backgroundView)
        configuration.emailSent.design.image(imageView)
        configuration.emailSent.design.title(titleLabel)
        configuration.emailSent.design.subtitle(subtitleLabel)
        configuration.emailSent.design.cancelButton(cancelButton)
        configuration.emailSent.design.dismissButton(dismissButton)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(true, animated: animated)
        configuration.emailSent.animation?.viewWillAppear?(animatable)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        configuration.emailSent.animation?.viewDidAppear?(animatable)
    }

    // MARK: - Private

    private lazy var animatable = EmailSent.Animation.Animatable(imageView: imageView)

    @LazyInjected
    private var configuration: Authentication.Configuration

    private var viewModel: EmailSentViewModel?
    private let disposeBag = DisposeBag()

    private lazy var backgroundView = Authentication.Design.BackgroundView(frame: view.frame)
    private lazy var imageView = UIImageView()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "emailSent.titleLabel"
        label.numberOfLines = 0
        return label
    }()

    private let subtitleLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "emailSent.subtitleLabel"
        label.numberOfLines = 0
        return label
    }()

    private let dismissButton: Button = {
        let button = Button()
        button.accessibilityIdentifier = "emailSent.dismissButton"
        return button
    }()

    private lazy var stackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [titleLabel, subtitleLabel])
        view.axis = .vertical
        view.alignment = .fill
        view.distribution = .fill
        view.spacing = 8
        return view
    }()

    private lazy var cancelButton: Button = {
        let button = Button()
        button.accessibilityIdentifier = "emailSent.cancelButton"
        return button
    }()
}
