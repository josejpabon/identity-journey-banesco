//
//  Created by Backbase R&D B.V. on 07/09/20.
//

import UIKit
import RxSwift
import RxCocoa
import Resolver

internal final class EmailSentViewModel {
    func bind(
        cancel: Observable<Void>,
        dismiss: Observable<Void>
    ) -> (
        cancelButtonIcon: Driver<UIImage?>,
        image: Driver<UIImage?>,
        title: Driver<String>,
        subtitle: Driver<String>,
        dismissButtonTitle: Driver<String>,
        dismiss: Signal<Void>
    ) {
        let dismiss = Observable.merge(dismiss, cancel)
            .asSignal(onErrorJustReturn: ())

        return (
            cancelButtonIcon: .just(configuration.emailSent.cancelButtonIcon),
            image: .just(configuration.emailSent.image),
            title: .just(configuration.emailSent.strings.title()),
            subtitle: .just(configuration.emailSent.strings.subtitle()),
            dismissButtonTitle: .just(configuration.emailSent.strings.dismissButtonTitle()),
            dismiss: dismiss
        )
    }

    // MARK: - Private

    @LazyInjected
    private var configuration: Authentication.Configuration

    private let disposeBag = DisposeBag()
}
