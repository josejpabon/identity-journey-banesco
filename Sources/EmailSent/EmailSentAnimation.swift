//
//  Created by Backbase R&D B.V. on 09/09/20.
//

import UIKit

public extension EmailSent {
    /// Authentication journey email sent animation configurations.
    struct Animation {
        /// An object that represent all animatable views in email sent screen.
        public struct Animatable {
            /// Image view.
            public let imageView: UIImageView
        }

        /// Create a new configurations object with default values.
        public init() {
            // no code required.
        }

        /// Animation block to be applied to all animatable views before screen is presented.
        /// **Note**: Set this to` nil` to disable animations.
        public var viewWillAppear: ((Animatable) -> Void)? = { animatable in
            let transalation = CGAffineTransform(translationX: -400, y: 400)
            let scale = CGAffineTransform(scaleX: 0.55, y: 0.55)
            animatable.imageView.layer.setAffineTransform(transalation.concatenating(scale))
            animatable.imageView.alpha = 0
        }

        /// Animation block to be applied to all animatable views after the screen is presented.
        /// **Note**: Set this to` nil` to disable animations.
        public var viewDidAppear: ((Animatable) -> Void)? = { animatable in
            UIView.animate(
                withDuration: 1.25,
                delay: 0,
                usingSpringWithDamping: 0.75,
                initialSpringVelocity: 1,
                options: .curveEaseOut,
                animations: {
                    animatable.imageView.layer.setAffineTransform(.identity)
                    animatable.imageView.alpha = 1
                },
                completion: nil
            )
        }
    }
}
