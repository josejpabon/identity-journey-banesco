//
//  Created by Backbase R&D B.V. on 07/09/20.
//

import Foundation

public extension EmailSent {
    /// Email sent strings.
    struct Strings {
        /// Create a new strings object with default values.
        public init() {
            // no code required.
        }

        /// authentication.emailSent.labels.title
        public var title = localized(key: "labels.title")

        /// authentication.emailSent.labels.subtitle
        public var subtitle = localized(key: "labels.subtitle")

        /// authentication.emailSent.buttons.dismiss
        public var dismissButtonTitle = localized(key: "buttons.dismiss")

        private static func localized(key: String) -> LocalizedString {
            let prefix = "authentication.emailSent."
            return LocalizedString(key: prefix + key, in: .authentication)
        }
    }
}
