//
//  Created by Backbase R&D B.V. on 25/8/2020.
//

import Foundation

public extension SetupComplete {
    /// Setup complete strings.
    struct Strings {
        /// Create a new strings object with default values.
        public init() {
            // no code required.
        }

        /// authentication.setupComplete.labels.title
        public var title = localized(key: "labels.title")

        /// authentication.setupComplete.labels.title
        public var subtitle = localized(key: "labels.subtitle")

        /// authentication.setupComplete.buttons.dismiss
        public var dismissButtonTitle = localized(key: "buttons.dismiss")

        private static func localized(key: String) -> LocalizedString {
            let prefix = "authentication.setupComplete."
            return LocalizedString(key: prefix + key, in: .authentication)
        }
    }
}
