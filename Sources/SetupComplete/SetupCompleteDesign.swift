//
//  Created by Backbase R&D B.V. on 25/8/2020.
//

import UIKit

public extension SetupComplete {
    /// Setup complete design-related configurations.
    struct Design {
        internal init(journeyDesign: Authentication.Design) {
            self.journeyDesign = journeyDesign
        }

        /// Applied to background view in setup complete screen.
        public lazy var background = journeyDesign.styles.background

        /// Applied to background image view in setup complete screen.
        public lazy var backgroundImage = journeyDesign.styles.image

        /// Applied to image view in setup complete screen.
        public lazy var image = journeyDesign.styles.image

        /// Applied to title label setup complete screen.
        public lazy var title = journeyDesign.styles.title

        /// Applied to subtitle label in setup complete screen.
        public lazy var subtitle = journeyDesign.styles.subtitle

        /// Applied to cancel button in setup complete screen.
        public lazy var cancelButton = journeyDesign.styles.button(.plain)

        /// Applied to dismiss button in setup complete screen.
        public lazy var dismissButton = journeyDesign.styles.button(.primary)

        // MARK: - Private

        private let journeyDesign: Authentication.Design
    }
}
