//
//  Created by Backbase R&D B.V. on 25/8/2020.
//

import UIKit

/// Authentication namespace used for grouping setup complete level configuration.
public struct SetupComplete {
    /// Build a setup complete view controller.
    /// - Parameter navigationController: parent navigation controller.
    /// - Returns: view controller building closure.
    public static func build(navigationController: UINavigationController) -> (User) -> UIViewController {
        return { user in
            let viewModel = SetupCompleteViewModel(user: user)
            let viewController = SetupCompleteViewController()
            viewController.bind(viewModel: viewModel)
            return viewController
        }
    }
}
