//
//  Created by Backbase R&D B.V. on 25/8/2020.
//

import UIKit

public extension SetupComplete {
    struct Configuration {
        internal init(journeyDesign: Authentication.Design) {
            self.journeyDesign = journeyDesign
        }

        /// Setup complete screen image (default is `UIImage.named("SetupComplete", in: .authentication)`)
        public var image = UIImage.named("SetupComplete", in: .authentication)

        /// Setup complete screen background image (default is `UIImage.named("SetupCompleteBackground", in: .authentication)`)
        public var backgroundImage = UIImage.named("SetupCompleteBackground", in: .authentication)

        /// Cancel button icon (default: `UIImage.named("ic_close", in: .authentication, .design)`)
        public var cancelButtonIcon = UIImage.named("ic_close", in: .authentication, .design)

        /// Register completion design configurations.
        public lazy var design = Design(journeyDesign: journeyDesign)

        /// Register completion strings.
        public var strings = Strings()

        // MARK: - Private

        private let journeyDesign: Authentication.Design
    }
}
