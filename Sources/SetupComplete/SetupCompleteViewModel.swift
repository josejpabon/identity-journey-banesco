//
//  Created by Backbase R&D B.V. on 25/8/2020.
//

import UIKit
import RxSwift
import RxCocoa
import Resolver

internal final class SetupCompleteViewModel {
    init(user: User) {
        self.user = user
    }

    func bind(
        cancel: Observable<Void>,
        dismiss: Observable<Void>
    ) -> (
        cancelButtonIcon: Driver<UIImage?>,
        backgroundImage: Driver<UIImage?>,
        image: Driver<UIImage?>,
        title: Driver<String>,
        subtitle: Driver<String>,
        dismissButtonTitle: Driver<String>,
        isLoading: Driver<Bool>,
        errorAlert: Signal<UIAlertController>
    ) {
        cancel
            .bind(to: dismissRelay)
            .disposed(by: disposeBag)

        dismiss
            .bind(to: dismissRelay)
            .disposed(by: disposeBag)

        dismissRelay
            .compactMap { [weak self] in return self?.user }
            .bind { [weak self] user in self?.resumeAuthentication(for: user) }
            .disposed(by: disposeBag)

        return (
            cancelButtonIcon: .just(configuration.setupComplete.cancelButtonIcon),
            backgroundImage: .just(configuration.setupComplete.backgroundImage),
            image: .just(configuration.setupComplete.image),
            title: .just(configuration.setupComplete.strings.title()),
            subtitle: .just(configuration.setupComplete.strings.subtitle()),
            dismissButtonTitle: .just(configuration.setupComplete.strings.dismissButtonTitle()),
            isLoading: loadingRelay.asDriver(),
            errorAlert: errorAlertRelay.asSignal()
        )
    }

    func resumeAuthentication(for user: User) {
        loadingRelay.accept(true)
        authenticationUseCase.authenticate(user: user, preCompletion: nil) { [weak self] result in
            self?.loadingRelay.accept(false)
            switch result {
            case .success:
                break
            case .failure(let error):
                if let alert = self?.alertController(for: error) {
                    self?.errorAlertRelay.accept(alert)
                }
            }
        }
    }

    func alertController(for error: Authentication.Error) -> UIAlertController {
        let alert = UIAlertController(
            title: configuration.strings.alertTitle(error)(),
            message: configuration.strings.alertMessage(error)(),
            preferredStyle: .alert
        )
        let dimissAction = UIAlertAction(title: configuration.strings.alertConfirmOption(), style: .default)
        alert.addAction(dimissAction)
        return alert
    }

    // MARK: - Private

    private let user: User

    @LazyInjected
    private var configuration: Authentication.Configuration

    @LazyInjected
    private var authenticationUseCase: AuthenticationUseCase

    private let dismissRelay = PublishRelay<Void>()
    private let loadingRelay = BehaviorRelay<Bool>(value: false)
    private let errorAlertRelay = PublishRelay<UIAlertController>()

    private let disposeBag = DisposeBag()
}
