//
//  Created by Backbase R&D B.V. on 25/8/2020.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit
import Resolver
import BackbaseDesignSystem

internal final class SetupCompleteViewController: UIViewController {
    func bind(viewModel: SetupCompleteViewModel) {
        self.viewModel = viewModel

        let output = viewModel.bind(
            cancel: cancelButton.rx.tap.asObservable(),
            dismiss: dismissButton.rx.tap.asObservable()
        )

        disposeBag.insert(
            output.backgroundImage.drive(backgroundImageView.rx.image),
            output.image.drive(imageView.rx.image),
            output.title.drive(titleLabel.rx.text),
            output.subtitle.drive(subtitleLabel.rx.text),
            output.dismissButtonTitle.drive(dismissButton.rx.title()),
            output.cancelButtonIcon.drive(cancelButton.rx.image()),
            output.isLoading.drive(dismissButton.rx.isAnimating),
            output.isLoading.map({ !$0 }).drive(cancelButton.rx.isEnabled),
            output.isLoading.map({ !$0 }).drive(dismissButton.rx.isEnabled),
            output.errorAlert.emit(onNext: { [weak self] alert in
                self?.present(alert, animated: true)
            })
        )
    }

    // MARK: - View lifecycle methods

    override func loadView() {
        super.loadView()

        view.addSubviews(backgroundView, backgroundImageView, imageView, stackView, cancelButton, dismissButton)

        backgroundView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        let safeArea = view.safeAreaLayoutGuide

        cancelButton.snp.makeConstraints { make in
            make.top.equalTo(safeArea).inset(DesignSystem.shared.spacer.sm)
            make.leading.equalTo(safeArea).inset(DesignSystem.shared.spacer.md)
            make.height.equalTo(DesignSystem.shared.sizer.sm*6)
        }

        backgroundImageView.snp.makeConstraints { make in
            make.leading.top.trailing.equalTo(safeArea)
            make.height.equalTo(safeArea).dividedBy(2)
        }

        imageView.snp.makeConstraints { make in
            make.width.height.equalTo(DesignSystem.shared.sizer.sm * 20)
            make.centerX.equalTo(backgroundImageView)
            make.bottom.equalTo(backgroundImageView).offset(-DesignSystem.shared.spacer.sm * 10)
        }

        stackView.snp.makeConstraints { make in
            make.top.equalTo(backgroundImageView.snp.bottom).offset(DesignSystem.shared.spacer.sm)
            make.leading.trailing.equalTo(safeArea).inset(DesignSystem.shared.spacer.sm * 6)
            make.bottom.lessThanOrEqualTo(safeArea).inset(DesignSystem.shared.spacer.md)
        }

        dismissButton.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalTo(safeArea).inset(DesignSystem.shared.spacer.md)
            make.height.equalTo(DesignSystem.shared.sizer.sm * 6)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            isModalInPresentation = true
        }

        configuration.setupComplete.design.background(backgroundView)
        configuration.setupComplete.design.backgroundImage(backgroundImageView)
        configuration.setupComplete.design.image(imageView)
        configuration.setupComplete.design.title(titleLabel)
        configuration.setupComplete.design.subtitle(subtitleLabel)
        configuration.setupComplete.design.cancelButton(cancelButton)
        configuration.setupComplete.design.dismissButton(dismissButton)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    // MARK: - Private

    @LazyInjected
    private var configuration: Authentication.Configuration

    private var viewModel: SetupCompleteViewModel?
    private let disposeBag = DisposeBag()

    private lazy var backgroundView = Authentication.Design.BackgroundView(frame: view.frame)

    private lazy var backgroundImageView = UIImageView()
    private lazy var imageView = UIImageView()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "setupComplete.titleLabel"
        label.numberOfLines = 0
        return label
    }()

    private let subtitleLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "setupComplete.subtitleLabel"
        label.numberOfLines = 0
        return label
    }()

    private let dismissButton: Button = {
        let button = Button()
        button.accessibilityIdentifier = "setupComplete.dismissButton"
        return button
    }()

    private lazy var stackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [titleLabel, subtitleLabel])
        view.axis = .vertical
        view.alignment = .fill
        view.distribution = .fill
        view.spacing = DesignSystem.shared.spacer.sm
        return view
    }()

    private lazy var cancelButton: Button = {
        let button = Button()
        button.accessibilityIdentifier = "setupComplete.cancelButton"
        return button
    }()
}
