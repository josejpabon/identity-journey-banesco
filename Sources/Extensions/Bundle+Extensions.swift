//
//  Created by Backbase R&D B.V. on 6/5/2020.
//

import Foundation

public extension Bundle {
    /// Authentication journey bundle.
    static var authentication: Bundle? {
        let podBundle = Bundle(for: LoginViewModel.self)
        
        guard
            let resourceUrl = podBundle.url(forResource: "IdentityAuthenticationJourneyAssets", withExtension: "bundle"),
            let resourceBundle = Bundle(url: resourceUrl)
            else { return podBundle }

        return resourceBundle
    }
}
