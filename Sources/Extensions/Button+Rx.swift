//
//  Created by Backbase R&D B.V. on 09/09/20.
//

import BackbaseDesignSystem
import RxSwift
import RxCocoa

extension Reactive where Base: Button {
    /// Bindable sink for `startLoading()`, `stopLoading()` methods.
    internal var isAnimating: Binder<Bool> {
        return Binder(self.base) { view, active in
            if active {
                view.startLoading()
            } else {
                view.stopLoading()
            }
        }
    }
}
