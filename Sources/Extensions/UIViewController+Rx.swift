//
//  Created by Backbase R&D B.V. on 16/01/2020.
//

import UIKit
import RxCocoa
import RxSwift

extension Reactive where Base: UIViewController {
    /// `ControlEvent` for the `viewDidLoad` event of the view controller.
    var viewDidLoad: ControlEvent<Void> {
        let source = self.methodInvoked(#selector(Base.viewDidLoad)).map { _ in /* do nothing */ }
        return ControlEvent(events: source)
    }

    /// `ControlEvent` for the `viewWillAppear` event of the view controller.
    var viewWillAppear: ControlEvent<Bool> {
        let source = self.methodInvoked(#selector(Base.viewWillAppear)).map { $0.first as? Bool ?? false }
        return ControlEvent(events: source)
    }

    /// `ControlEvent` for the `viewDidAppear` event of the view controller.
    var viewDidAppear: ControlEvent<Bool> {
        let source = self.methodInvoked(#selector(Base.viewDidAppear)).map { $0.first as? Bool ?? false }
        return ControlEvent(events: source)
    }

    /// `ControlEvent` for the `viewWillDisappear` event of the view controller.
    var viewWillDisappear: ControlEvent<Bool> {
        let source = self.methodInvoked(#selector(Base.viewWillDisappear)).map { $0.first as? Bool ?? false }
        return ControlEvent(events: source)
    }

    /// `ControlEvent` for the `viewDidDisappear` event of the view controller.
    var viewDidDisappear: ControlEvent<Bool> {
        let source = self.methodInvoked(#selector(Base.viewDidDisappear)).map { $0.first as? Bool ?? false }
        return ControlEvent(events: source)
    }

    /// `ControlEvent` for the `viewWillLayoutSubviews` event of the view controller.
    var viewWillLayoutSubviews: ControlEvent<Void> {
        let source = self.methodInvoked(#selector(Base.viewWillLayoutSubviews)).map { _ in /* do nothing */ }
        return ControlEvent(events: source)
    }

    /// `ControlEvent` for the `viewDidLoad` event of the view controller.
    var viewDidLayoutSubviews: ControlEvent<Void> {
        let source = self.methodInvoked(#selector(Base.viewDidLayoutSubviews)).map { _ in /* do nothing */ }
        return ControlEvent(events: source)
    }

    /// `ControlEvent` for the `willMoveToParentViewController` event of the view controller.
    var willMoveToParentViewController: ControlEvent<UIViewController?> {
        let source = self.methodInvoked(#selector(Base.willMove)).map { $0.first as? UIViewController }
        return ControlEvent(events: source)
    }

    /// `ControlEvent` for the `didMoveToParentViewController` event of the view controller.
    var didMoveToParentViewController: ControlEvent<UIViewController?> {
        let source = self.methodInvoked(#selector(Base.didMove)).map { $0.first as? UIViewController }
        return ControlEvent(events: source)
    }

    /// `ControlEvent` for the `didReceiveMemoryWarning` event of the view controller.
    var didReceiveMemoryWarning: ControlEvent<Void> {
        let source = self.methodInvoked(#selector(Base.didReceiveMemoryWarning)).map { _ in /* do nothing */ }
        return ControlEvent(events: source)
    }
    
    /// Rx observable, triggered when the ViewController appearance state changes (true if the View is being displayed, false otherwise)
    var isVisible: Observable<Bool> {
        let viewDidAppearObservable = self.base.rx.viewDidAppear.map { _ in true }
        let viewWillDisappearObservable = self.base.rx.viewWillDisappear.map { _ in false }
        return Observable<Bool>.merge(viewDidAppearObservable, viewWillDisappearObservable)
    }
    
    /// Rx observable, triggered when the ViewController is being dismissed
    var isDismissing: ControlEvent<Bool> {
        let source = self.sentMessage(#selector(Base.dismiss)).map { $0.first as? Bool ?? false }
        return ControlEvent(events: source)
    }

    /// Rx observable, triggered when the the keyboard is being presented or dismissed.
    var keyboardHeight: Observable<CGFloat> {
        let center = NotificationCenter.default
        let willShow = center.rx.notification(UIResponder.keyboardWillShowNotification)
            .map { notification -> CGFloat in
                return (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height ?? 0
            }
        let willHide = center.rx.notification(UIResponder.keyboardWillHideNotification)
            .map { _ -> CGFloat in
                return 0
            }
        return Observable.from([willShow, willHide]).merge()
    }
}
