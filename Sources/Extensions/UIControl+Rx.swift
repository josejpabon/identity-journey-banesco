//
//  Created by Backbase R&D B.V. on 13/07/20.
//

import UIKit
import RxSwift
import RxCocoa

internal extension Reactive where Base: UIControl {
    var firstResponder: RxCocoa.ControlProperty<Bool> {
        return base.rx.controlProperty(
            editingEvents: .allEvents,
            getter: { return $0.isFirstResponder },
            setter: { control, flag in
                _ = flag ? control.becomeFirstResponder() : control.resignFirstResponder()
            }
        )
    }
}
