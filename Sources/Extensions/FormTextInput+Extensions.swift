//
//  Created by Backbase R&D B.V. on 09/09/20.
//

import class BackbaseDesignSystem.TextInput
import RxSwift
import RxCocoa

internal extension TextInput {
    struct State {
        var label: String?
        var errorMessage: String?
    }
}
