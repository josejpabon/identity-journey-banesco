//
//  Created by Backbase R&D B.V. on 02/04/2020.
//

import RxSwift

extension ObservableType {
    /// Takes a sequence of optional elements and returns a sequence of non-optional elements, filtering out any nil values.
    /// - Returns: An observable sequence of non-optional elements
    func unwrapped<Value>() -> Observable<Value> where Element == Value? {
        return self.compactMap { $0 }
    }
}
