//
//  Created by Backbase R&D B.V. on 03/04/2020.
//

import Backbase

internal extension SessionState {
    var session: Session {
        switch self {
        case .valid:
            return .valid
        case .none:
            return .none
        @unknown default:
            return .none
        }
    }
}
