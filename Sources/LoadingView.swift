//
//  Created by Backbase R&D B.V. on 08/07/20.
//

import UIKit
import SnapKit
import BackbaseDesignSystem
import RxSwift
import RxCocoa
import Resolver

internal final class LoadingView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(backgroundView)
        backgroundView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        isHidden = true

        configuration.register.design.background(backgroundView)

        addSubviews(loadingIndicator)
        loadingIndicator.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError()
    }

    lazy var loadingIndicator: UIActivityIndicatorView = {
        var style: UIActivityIndicatorView.Style {
            if #available(iOS 13.0, *) {
                return .large
            } else {
                return .whiteLarge
            }
        }
        let view = UIActivityIndicatorView(style: style)
        configuration.design.styles.loading(view)
        return view
    }()

    // MARK: - Private

    private lazy var backgroundView = Authentication.Design.BackgroundView(frame: frame)

    @LazyInjected
    private var configuration: Authentication.Configuration
}

extension Reactive where Base: LoadingView {
    /// Bindable sink for `startAnimating()`, `stopAnimating()` methods.
    internal var isAnimating: Binder<Bool> {
        return Binder(self.base) { view, active in
            if active {
                view.loadingIndicator.startAnimating()
                view.isHidden = false
            } else {
                view.loadingIndicator.stopAnimating()
                view.isHidden = true
            }
        }
    }
}
