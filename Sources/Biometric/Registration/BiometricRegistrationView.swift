//
//  Created by Backbase R&D B.V. on 3/30/20.
//

import UIKit
import BackbaseDesignSystem
import RxCocoa
import RxSwift
import SnapKit
import Resolver

internal final class BiometricRegistrationView: UIView {
    weak var viewController: RenderingViewController?

    func bind(viewModel: BiometricRegistrationViewModel) {
        self.viewModel = viewModel

        let output = viewModel.bind(
            allow: allowButton.rx.tap.asObservable(),
            deny: denyButton.rx.tap.asObservable(),
            cancel: cancelButton.rx.tap.asObservable()
        )

        output.errorAlert.subscribe(onNext: { [weak self] alert in
            self?.viewController?.present(alert, animated: true)
        })
        .disposed(by: disposeBag)

        disposeBag.insert(
            output.cancelButtonIcon.drive(cancelButton.rx.image()),
            output.image.drive(imageView.rx.image),
            output.title.drive(titleLabel.rx.text),
            output.subtitle.drive(subtitleLabel.rx.text),
            output.allowButtonTitle.drive(allowButton.rx.title()),
            output.denyButtonTitle.drive(denyButton.rx.title())
        )
    }

    // MARK: - UIView methods

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubviews(backgroundView, cancelButton, stackView, buttonsStackView)

        backgroundView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        let safeArea = safeAreaLayoutGuide

        stackView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(safeArea).inset(DesignSystem.shared.spacer.xl)
            make.centerY.equalTo(safeArea).offset(-DesignSystem.shared.spacer.sm * 5)
        }

        buttonsStackView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalTo(safeArea).inset(DesignSystem.shared.spacer.md)
        }

        imageView.snp.makeConstraints { make in
            make.height.equalTo(DesignSystem.shared.sizer.sm * 7)
        }

        cancelButton.snp.makeConstraints { make in
            make.top.equalTo(safeArea).inset(DesignSystem.shared.spacer.sm)
            make.leading.equalTo(safeArea).inset(DesignSystem.shared.spacer.md)
            make.height.equalTo(DesignSystem.shared.sizer.sm*6)
        }

        applyStyles()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError()
    }

    private lazy var backgroundView = Authentication.Design.BackgroundView(frame: frame)

    private lazy var cancelButton: Button = {
        let button = Button()
        button.accessibilityIdentifier = "biometricRegistration.cancelButton"
        return button
    }()

    private lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.accessibilityIdentifier = "biometricRegistration.imageView"
        return view
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "biometricRegistration.titleLabel"
        return label
    }()

    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "biometricRegistration.subtitleLabel"
        return label
    }()

    private lazy var stackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [imageView, titleLabel, subtitleLabel])
        view.axis = .vertical
        view.alignment = .fill
        view.distribution = .fill
        view.spacing = DesignSystem.shared.spacer.sm
        view.setCustomSpacing(DesignSystem.shared.spacer.xl, after: imageView)
        return view
    }()

    private lazy var allowButton: Button = {
        let button = Button()
        button.accessibilityIdentifier = "biometricRegistration.allowButton"
        return button
    }()

    private lazy var denyButton: Button = {
        let button = Button()
        button.accessibilityIdentifier = "biometricRegistration.denyButton"
        return button
    }()

    private lazy var buttonsStackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [allowButton, denyButton])
        view.axis = .vertical
        view.alignment = .fill
        view.distribution = .fill
        view.spacing = DesignSystem.shared.spacer.md
        return view
    }()

    private func applyStyles() {
        configuration.biometric.design.background(backgroundView)
        configuration.biometric.design.image(imageView)
        configuration.biometric.design.title(titleLabel)
        configuration.biometric.design.subtitle(subtitleLabel)
        configuration.biometric.design.allowUsageButton(allowButton)
        configuration.biometric.design.denyUsageButton(denyButton)
        configuration.biometric.design.cancelButton(cancelButton)
    }

    @LazyInjected
    private var configuration: Authentication.Configuration

    private var viewModel: BiometricRegistrationViewModel?
    private let disposeBag = DisposeBag()
}
