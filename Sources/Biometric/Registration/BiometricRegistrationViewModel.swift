//
//  Created by Backbase R&D B.V. on 3/30/20.
//

import UIKit
import RxSwift
import RxCocoa
import Resolver
import LocalAuthentication
import BackbaseIdentity

internal final class BiometricRegistrationViewModel: AuthenticatorViewModel {
    weak var contract: BBIDBiometricAuthenticatorContract?

    required init(contract: BBIDBiometricAuthenticatorContract?) {
        self.contract = contract
    }

    func bind(
        allow: Observable<Void>,
        deny: Observable<Void>,
        cancel: Observable<Void>
    ) -> (
        cancelButtonIcon: Driver<UIImage?>,
        image: Driver<UIImage?>,
        title: Driver<String>,
        subtitle: Driver<String>,
        allowButtonTitle: Driver<String>,
        denyButtonTitle: Driver<String>,
        errorAlert: Observable<UIAlertController>
    ) {
        allow.subscribe(onNext: { [weak self] in
            self?.contract?.userDidGrantBiometricUsage()
        })
        .disposed(by: disposeBag)

        deny.subscribe(onNext: { [weak self] in
            self?.contract?.userDidDenyBiometricUsage()
        })
        .disposed(by: disposeBag)

        cancel.subscribe(onNext: { [weak self] in
            self?.contract?.userDidDenyBiometricUsage()
        })
        .disposed(by: disposeBag)

        return (
            cancelButtonIcon: .just(configuration.biometric.cancelButtonIcon),
            image: .just(promptImage()),
            title: .just(promptTitle()),
            subtitle: .just(promptSubtitle()),
            allowButtonTitle: .just(allowUsageButtonTitle()),
            denyButtonTitle: .just(configuration.biometric.strings.denyUsageButtonTitle()),
            errorAlert: errorAlertRelay.asObservable()
        )
    }

    func didSucceed() {
        contract?.finish()
    }

    func didFail(with error: Authentication.Error) {
        switch error {
        case .cancelledByUser:
            cancel()
        default:
            errorAlertRelay.onNext(errorAlertController(for: error))
        }
    }

    func promptImage(for type: BiometryType = .supported) -> UIImage? {
        return configuration.biometric.promptImage(type)
    }

    func promptTitle(for type: BiometryType = .supported) -> String {
        return configuration.biometric.strings.title(type)?() ?? ""
    }

    func promptSubtitle(for type: BiometryType = .supported) -> String {
        return configuration.biometric.strings.subtitle(type)?() ?? ""
    }

    func allowUsageButtonTitle(for type: BiometryType = .supported) -> String {
        return configuration.biometric.strings.promptButtonTitle(type)?() ?? ""
    }

    func finish() {
        contract?.finish()
    }

    func cancel() {
        contract?.cancel()
    }

    func retry() {
        contract?.retry()
    }

    func errorAlertController(for error: Authentication.Error) -> UIAlertController {
        let title = configuration.strings.alertTitle(error)()
        let message = configuration.strings.alertMessage(error)()
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertActions(for: error).forEach(alert.addAction(_:))
        return alert
    }

    func deniedByUserAlertActions() -> [UIAlertAction] {
        let openSettingsTitle = configuration.strings.alertSettingsOption()
        let openSettingsAction = UIAlertAction(title: openSettingsTitle, style: .default) { [weak self] _ in
            self?.openSettings()
        }

        let cancelTitle = configuration.strings.alertCancelOption()
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel)

        return [openSettingsAction, cancelAction]
    }

    func alertActions(for error: Authentication.Error) -> [UIAlertAction] {
        if error == .biometricUsageDenied {
            return deniedByUserAlertActions()
        }

        let retryTitle = configuration.strings.alertRetryOption()
        let retryAction = UIAlertAction(title: retryTitle, style: .default) { [weak self] _ in
            self?.retry()
        }

        let cancelTitle = configuration.strings.alertCancelOption()
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { [weak self] _ in
            self?.cancel()
        }

        return [retryAction, cancelAction]
    }

    func openSettings() {
        guard let url = URL(string: UIApplication.openSettingsURLString) else { return }
        UIApplication.shared.open(url, options: [:])
    }

    // MARK: - Private

    @LazyInjected
    private var configuration: Authentication.Configuration

    private var errorAlertRelay = PublishSubject<UIAlertController>()
    private let disposeBag = DisposeBag()
}
