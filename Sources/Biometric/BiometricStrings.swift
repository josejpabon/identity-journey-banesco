//
//  Created by Backbase R&D B.V. on 15/5/2020.
//

import Foundation

public extension Biometric {
    /// Biometric strings.
    struct Strings {
        /// Create a new strings object with default values.
        public init() {
            // no code required.
        }

        /// authentication.biometrics.registration.labels.title.{biometryType}
        public var title: (BiometryType) -> LocalizedString? = { type in
            switch type {
            case .faceID:
                return localized(key: "registration.labels.title.faceID")
            case .touchID:
                return localized(key: "registration.labels.title.touchID")
            default:
                return nil
            }
        }

        /// authentication.biometrics.registration.labels.subtitle.{biometryType}
        public var subtitle: (BiometryType) -> LocalizedString? = { type in
            switch type {
            case .faceID:
                return localized(key: "registration.labels.subtitle.faceID")
            case .touchID:
                return localized(key: "registration.labels.subtitle.touchID")
            default:
                return nil
            }
        }

        /// authentication.biometrics.registration.buttons.prompt.{biometryType}
        public var promptButtonTitle: (BiometryType) -> LocalizedString? = { type in
            switch type {
            case .faceID:
                return localized(key: "registration.buttons.prompt.faceID")
            case .touchID:
                return localized(key: "registration.buttons.prompt.touchID")
            default:
                return nil
            }
        }

        /// authentication.biometrics.registration.buttons.cancel
        public var denyUsageButtonTitle = localized(key: "registration.buttons.cancel")

        private static func localized(key: String) -> LocalizedString {
            let prefix = "authentication.biometrics."
            return LocalizedString(key: prefix + key, in: .authentication)
        }
    }
}
