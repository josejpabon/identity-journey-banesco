//
//  Created by Backbase R&D B.V. on 28/4/20.
//

import UIKit

public extension Biometric {
    /// Biometric design-related configurations.
    struct Design {
        internal init(journeyDesign: Authentication.Design) {
            self.journeyDesign = journeyDesign
        }

        /// Applied to all `BackgroundView` views.
        public lazy var background = journeyDesign.styles.background

        /// Applied to all image views in biometric screens.
        public lazy var image = journeyDesign.styles.image

        /// Applied to all title labels in biometric screens.
        public lazy var title = journeyDesign.styles.title

        /// Applied to all subtitle labels in biometric screens.
        public lazy var subtitle = journeyDesign.styles.subtitle

        /// Applied to "allow usage" button in biometric screens.
        public lazy var allowUsageButton = journeyDesign.styles.button(.primary)

        /// Applied to "deny usage" button in biometric screens.
        public lazy var denyUsageButton = journeyDesign.styles.button(.plain)

        /// Applied to "cancel" button in biometric screens.
        public lazy var cancelButton = journeyDesign.styles.button(.plain)

        // MARK: - Private

        private let journeyDesign: Authentication.Design
    }
}
