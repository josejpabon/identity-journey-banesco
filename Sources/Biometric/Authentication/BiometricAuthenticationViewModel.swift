//
//  Created by Backbase R&D B.V. on 3/30/20.
//

import BackbaseIdentity

internal final class BiometricAuthenticationViewModel: AuthenticatorViewModel {
    weak var contract: BBIDBiometricAuthenticatorContract?

    required init(contract: BBIDBiometricAuthenticatorContract?) {
        self.contract = contract
    }

    func authorize() {
        contract?.authorize()
    }

    func didSucceed() {
        contract?.finish()
    }

    func didFail(with error: Authentication.Error) {
        contract?.cancel()
    }
}
