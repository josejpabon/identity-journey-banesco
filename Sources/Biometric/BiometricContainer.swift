//
//  Created by Backbase R&D B.V. on 3/27/20.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit
import Resolver
import BackbaseIdentity

internal final class BiometricContainer: UIView, RenderableView, BBIDBiometricAuthenticatorView {
    func promptForBiometricUsage() {
        let viewModel = BiometricRegistrationViewModel(contract: contract)
        activeViewModel = viewModel

        let view = BiometricRegistrationView()
        view.bind(viewModel: viewModel)
        view.viewController = viewController
        visibleView = view
    }

    func promptForBiometricAuthentication() {
        let viewModel = BiometricAuthenticationViewModel(contract: contract)
        activeViewModel = viewModel
        viewModel.authorize()
    }

    func authenticatorDidSucceed() {
        activeViewModel?.didSucceed()
    }

    func authenticatorDidFail(with error: Error) {
        activeViewModel?.didFail(with: .init(biometric: error))
    }

    // MARK: - Private

    private weak var contract: BBIDBiometricAuthenticatorContract?
    private var activeViewModel: AuthenticatorViewModel?

    // MARK: - Rendering

    static func initialize(with contractImpl: NativeContract, container: UIView) -> (UIView & NativeView)? {
        let view = Self(frame: UIScreen.main.bounds)
        view.contract = contractImpl as? BBIDBiometricAuthenticatorContract
        container.addSubview(view)
        view.snp.makeConstraints { $0.edges.equalToSuperview() }
        return view
    }

    var visibleView: UIView? {
        willSet {
            visibleView?.removeFromSuperview()
        }
        didSet {
            if let view = visibleView {
                addSubview(view)
                view.snp.makeConstraints { $0.edges.equalToSuperview() }
            }
        }
    }
}
