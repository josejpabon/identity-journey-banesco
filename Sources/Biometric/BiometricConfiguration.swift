//
//  Created by Backbase R&D B.V. on 3/27/20.
//

import UIKit

public extension Biometric {
    /// Authentication journey biometric configurations.
    struct Configuration {
        internal init(journeyDesign: Authentication.Design) {
            self.journeyDesign = journeyDesign
        }

        /// Prompt image for a given `BiometryType`.
        /// - Defaults:
        ///     - `BiometryType.faceID` -> `UIImage.named("FaceID", in: .authentication)`.
        ///     - `BiometryType.touchID` -> `UIImage.named("TouchID", in: .authentication)`.
        ///     - default: nil.
        public var promptImage: (BiometryType) -> UIImage? = { type in
            switch type {
            case .faceID:
                return UIImage.named("FaceID", in: .authentication)
            case .touchID:
                return UIImage.named("TouchID", in: .authentication)
            default:
                return nil
            }
        }

        /// Image used for the cancel button. (default: `UIImage.named("ic_close", in: .authentication, .design)`)
        public var cancelButtonIcon = UIImage.named("ic_close", in: .authentication, .design)

        /// Biometric design configurations.
        public lazy var design = Design(journeyDesign: journeyDesign)

        /// Biometric strings.
        public var strings = Strings()

        // MARK: - Private

        private let journeyDesign: Authentication.Design
    }
}
