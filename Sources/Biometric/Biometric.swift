//
//  Created by Backbase R&D B.V. on 3/27/20.
//

import UIKit
import Backbase
import BackbaseIdentity

/// Authentication namespace used for grouping biometric level configuration.
public struct Biometric {
    /// Build a biometric view controller from a `Renderable`.
    /// - Parameter item: `Renderable`.
    /// - Returns: view controller building closure.
    public static func build(item: Renderable) -> (UINavigationController) -> UIViewController {
        return { navigationController in
            navigationController.viewControllers = [RenderingViewController(withRenderable: item)]
            return navigationController
        }
    }

    internal static var authenticator: BBIDBiometricAuthenticator = {
        do {
            return try BBIDBiometricAuthenticator(viewClass: BiometricContainer.self)
        } catch {
            fatalError("Failed to create BBIDBiometricAuthenticator from BiometricContainer: \(error.localizedDescription)")
        }
    }()
}
