//
//  Created by Backbase R&D B.V. on 3/30/20.
//

import UIKit

internal protocol AuthenticatorViewModel {
    func didSucceed()
    func didFail(with error: Authentication.Error)
}
