//
//  Created by Backbase R&D B.V. on 3/27/20.
//

import BackbaseIdentity

internal final class IdentityAuthenticationNavigationFactory {
    private weak var client: BBIDAuthClient?

    required init(client: BBIDAuthClient) {
        self.client = client
    }

    enum Target: String, CaseIterable {
        case biometric = "BBIDBiometricAuthenticator"
        case passcode = "BBIDPasscodeAuthenticator"
        case inputRequired = "BBIDInputRequiredAuthenticator"

        init?(from notification: Notification) {
            guard let target = notification.userInfo?["target"] as? String else { return nil }
            self.init(rawValue: target)
        }
    }

    func navigation(
        for notification: Notification,
        using presenter: BBIDAuthenticatorPresenter.Type = BBIDAuthenticatorPresenter.self
    ) -> Authentication.Navigation? {
        if presenter.isAuthenticatorDismissEvent(notification) {
            return .dismissCurrent
        }

        if presenter.isAuthenticatorShowEvent(notification) {
            guard let target = Target(from: notification) else { return nil }
            guard let item = client?.authenticatorRenderable(target.rawValue) else { return nil }
            switch target {
            case .biometric:
                return .present(Biometric.build(item: item))
            case .passcode:
                return .present(Passcode.build(item: item))
            case .inputRequired:
                return .present(InputRequired.build(item: item))
            }
        }

        return nil
    }
}
