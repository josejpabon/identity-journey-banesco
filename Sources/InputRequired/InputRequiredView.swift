//
//  Created by Backbase R&D B.V. on 16/4/20.
//

import UIKit
import RxSwift
import SnapKit
import Resolver
import BackbaseDesignSystem
import BackbaseIdentity

internal final class InputRequiredView: UIView {
    weak var viewController: RenderingViewController?

    func bind(viewModel: InputRequiredViewModel) {
        self.viewModel = viewModel
        inputs = viewModel.fields.map(createInputTextField)
        labels = viewModel.fields.map(createInputTextFieldLabel)
        zip(labels, inputs).flatMap { [$0, $1] }.forEach(fieldsStackView.addArrangedSubview)
        
        labels.forEach { fieldsStackView.setCustomSpacing((DesignSystem.shared.spacer.xxs), after: $0) }
        
        inputs.first?.becomeFirstResponder()

        let output = viewModel.bind(
            values: inputs.map(\.textField.rx.text),
            tap: tapGestureRecognizer.rx.event.map({ _ in Void() }),
            cancel: cancelButton.rx.tap.asObservable(),
            submit: submitButton.rx.tap.asObservable()
        )

        output.errorAlert.subscribe(onNext: { [weak self]  alert in
            self?.viewController?.present(alert, animated: true)
        })
        .disposed(by: disposeBag)

        output.placeholders.enumerated().forEach { [weak self] (index, placeholder) in
            guard let self = self else { return }
            let label = self.labels[index]
            placeholder.drive(label.rx.text).disposed(by: self.disposeBag)
        }

        disposeBag.insert(
            output.title.drive(titleLabel.rx.text),
            output.subtitle.drive(subtitleLabel.rx.text),
            output.cancelButtonIcon.drive(cancelButton.rx.image()),
            output.submitButtonTitle.drive(submitButton.rx.title()),
            output.unfocusInputs.emit(onNext: { [weak self] in
                self?.endEditing(false)
            })
        )
    }

    // MARK: - UIView methods

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubviews(backgroundView, titleLabel, subtitleLabel, fieldsStackView, submitButton, cancelButton)
        addGestureRecognizer(tapGestureRecognizer)

        backgroundView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        let safeArea = safeAreaLayoutGuide

        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(safeArea).inset(DesignSystem.shared.spacer.sm * 10)
            make.leading.trailing.equalTo(safeArea).inset(DesignSystem.shared.spacer.xl)
        }

        subtitleLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(DesignSystem.shared.spacer.sm)
            make.leading.trailing.equalTo(titleLabel)
        }

        fieldsStackView.snp.makeConstraints { make in
            make.top.equalTo(subtitleLabel.snp.bottom).offset(DesignSystem.shared.spacer.xl)
            make.leading.trailing.equalTo(safeArea).inset(DesignSystem.shared.spacer.md)
        }

        submitButton.snp.makeConstraints { make in
            make.height.equalTo(DesignSystem.shared.sizer.sm * 6)
            make.bottom.equalTo(safeArea).inset(DesignSystem.shared.spacer.md)
            make.leading.trailing.equalTo(safeArea).inset(DesignSystem.shared.spacer.md)
        }

        cancelButton.snp.makeConstraints { make in
            make.top.equalTo(safeArea).inset(DesignSystem.shared.spacer.sm)
            make.leading.equalTo(safeArea).inset(DesignSystem.shared.spacer.md)
            make.height.equalTo(DesignSystem.shared.sizer.sm*6)
        }

        keyboardHeight
            .subscribeOn(MainScheduler.instance)
            .bind { [weak self] height in
                self?.submitButton.snp.updateConstraints({ make in
                    let height = height == 0 ? DesignSystem.shared.spacer.md : height + DesignSystem.shared.spacer.md 
                    make.bottom.equalTo(safeArea).inset(height)
                })
                UIView.animate(withDuration: 0.25) { self?.layoutIfNeeded() }
            }
            .disposed(by: disposeBag)

        applyStyles()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError()
    }

    private func applyStyles() {
        configuration.inputRequired.design.background(backgroundView)
        configuration.inputRequired.design.title(titleLabel)
        configuration.inputRequired.design.subtitle(subtitleLabel)
        configuration.inputRequired.design.submitButton(submitButton)
        configuration.inputRequired.design.cancelButton(cancelButton)
    }

    private func createInputTextFieldLabel(for field: InputField) -> UILabel {
        let label = UILabel()
        label.accessibilityIdentifier = "inputRequired.\(field.key).placeholderLabel"
        configuration.inputRequired.design.formLabel(label)
        return label
    }
    
    private func createInputTextField(for field: InputField) -> TextInput {
        let input = TextInput()
        input.accessibilityIdentifier = "inputRequired.\(field.key)"

        switch field {
        case .email:
            configuration.inputRequired.design.input(.emailAddress)(input)
        default:
            configuration.inputRequired.design.input(.name)(input)
        }

        return input
    }

    private lazy var backgroundView = Authentication.Design.BackgroundView(frame: frame)

    private lazy var tapGestureRecognizer = UITapGestureRecognizer()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "inputRequired.titleLabel"
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 32, weight: .bold)
        return label
    }()

    private let subtitleLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "inputRequired.subtitleLabel"
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 14)
        return label
    }()

    private lazy var fieldsStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .fill
        view.distribution = .fill
        view.spacing = DesignSystem.shared.spacer.sm
        return view
    }()

    private lazy var submitButton: Button = {
        let button = Button()
        button.accessibilityIdentifier = "inputRequired.submitButton"
        button.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        button.layer.cornerRadius = DesignSystem.shared.cornerRadius.medium * 3
        return button
    }()

    private lazy var cancelButton: Button = {
        let button = Button()
        button.accessibilityIdentifier = "inputRequired.cancelButton"
        return button
    }()

    @LazyInjected
    private var configuration: Authentication.Configuration

    private var inputs: [TextInput] = []
    private var labels: [UILabel] = []
    private var viewModel: InputRequiredViewModel?
    private let disposeBag = DisposeBag()

    private var keyboardHeight: Observable<CGFloat> = {
        Observable.from([
            NotificationCenter.default.rx.notification(UIResponder.keyboardWillShowNotification).map { notification in
                let rect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
                return rect?.height ?? .zero
            },
            NotificationCenter.default.rx.notification(UIResponder.keyboardWillHideNotification).map { _ in
                return CGFloat.zero
            }
        ])
        .merge()
    }()
}
