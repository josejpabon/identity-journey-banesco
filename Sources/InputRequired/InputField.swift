//
//  Created by Backbase R&D B.V. on 16/4/20.
//

import Foundation

internal enum InputField {
    case email
    case unknown(String)

    init(_ value: String) {
        switch value {
        case "email":
            self = .email
        default:
            self = .unknown(value)
        }
    }

    var key: String {
        switch self {
        case .email:
            return "email"
        case .unknown(let field):
            return field
        }
    }
}

extension InputField: Equatable {
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.key == rhs.key
    }
}
