//
//  Created by Backbase R&D B.V. on 29/4/20.
//

import UIKit

public extension InputRequired {
    struct Design {
        internal init(journeyDesign: Authentication.Design) {
            self.journeyDesign = journeyDesign
        }

        public lazy var background = journeyDesign.styles.background
        public lazy var title = journeyDesign.styles.title
        public lazy var subtitle = journeyDesign.styles.subtitle
        public lazy var input = journeyDesign.styles.formTextInput
        public lazy var formLabel = journeyDesign.styles.formLabel
        public lazy var submitButton = journeyDesign.styles.button(.primary)
        public lazy var cancelButton = journeyDesign.styles.button(.plain)

        // MARK: - Private

        private let journeyDesign: Authentication.Design
    }
}
