//
//  Created by Backbase R&D B.V. on 16/4/20.
//

import UIKit

public extension InputRequired {
    struct Configuration {
        internal init(journeyDesign: Authentication.Design) {
            self.journeyDesign = journeyDesign
        }

        public var cancelButtonIcon = UIImage.named("ic_close", in: .authentication, .design)
        public lazy var design = Design(journeyDesign: journeyDesign)
        public var strings = Strings()

        // MARK: - Private

        private let journeyDesign: Authentication.Design
    }
}
