//
//  Created by Backbase R&D B.V. on 15/5/2020.
//

import Foundation

public extension InputRequired {
    struct Strings {
        public init() {
            // no code required.
        }

        /// authentication.inputRequired.labels.emailTitle
        public var emailTitle = localized(key: "labels.emailTitle")

        /// authentication.inputRequired.labels.emailSubtitle
        public var emailSubtitle = localized(key: "labels.emailSubtitle")

        /// authentication.inputRequired.inputs.email.placeholder
        public var emailPlaceholder = localized(key: "inputs.email.placeholder")

        /// authentication.inputRequired.buttons.email.submit
        public var emailSubmitButtonTitle = localized(key: "buttons.email.submit")

        /// authentication.inputRequired.labels.unknownTitle
        public var unknownTitle = localized(key: "labels.unknownTitle")

        /// authentication.inputRequired.labels.unknownSubtitle
        public var unknownSubtitle = localized(key: "labels.unknownSubtitle")

        /// authentication.inputRequired.buttons.unknown.submit
        public var unknownSubmitButtonTitle = localized(key: "buttons.unknown.submit")

        private static func localized(key: String) -> LocalizedString {
            let prefix = "authentication.inputRequired."
            return LocalizedString(key: prefix + key, in: .authentication)
        }
    }
}
