//
//  Created by Backbase R&D B.V. on 16/4/20.
//

import UIKit
import Backbase
import BackbaseIdentity

public struct InputRequired {
    public static func build(item: Renderable) -> (UINavigationController) -> UIViewController {
        return { navigationController in
            navigationController.viewControllers = [RenderingViewController(withRenderable: item)]
            return navigationController
        }
    }

    internal static var authenticator: BBIDInputRequiredAuthenticator = {
        do {
            return try BBIDInputRequiredAuthenticator(viewClass: InputRequiredContainer.self)
        } catch {
            fatalError("Failed to create BBIDInputRequiredAuthenticator from InputRequiredContainer: \(error.localizedDescription)")
        }
    }()
}
