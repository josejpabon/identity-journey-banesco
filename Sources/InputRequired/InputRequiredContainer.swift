//
//  Created by Backbase R&D B.V. on 16/4/20.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit
import Resolver
import BackbaseIdentity

internal final class InputRequiredContainer: UIView, RenderableView, BBIDInputRequiredAuthenticatorView {
    var visibleView: UIView?

    func promptUser(forInput fields: [String]) {
        let viewModel = InputRequiredViewModel(
            fields: fields.map(InputField.init),
            contract: contract
        )
        self.viewModel = viewModel
        let view = InputRequiredView()
        view.bind(viewModel: viewModel)
        view.viewController = viewController
        visibleView = view
        addSubview(view)
        view.snp.makeConstraints { $0.edges.equalToSuperview() }
    }

    func authenticatorDidSucceed() {
        viewModel?.didSucceed()
    }

    func authenticatorDidFail(with error: Error) {
        viewModel?.didFail(with: .init(inputRequired: error))
    }

    private weak var contract: BBIDInputRequiredAuthenticatorContract?
    private var viewModel: AuthenticatorViewModel?

    static func initialize(with contractImpl: NativeContract, container: UIView) -> (UIView & NativeView)? {
        let view = Self()
        view.contract = contractImpl as? BBIDInputRequiredAuthenticatorContract
        container.addSubview(view)
        view.snp.makeConstraints { $0.edges.equalToSuperview() }
        return view
    }
}
