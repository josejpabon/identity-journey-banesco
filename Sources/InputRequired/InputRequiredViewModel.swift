//
//  Created by Backbase R&D B.V. on 16/4/20.
//

import UIKit
import RxSwift
import RxCocoa
import Resolver
import BackbaseIdentity

internal final class InputRequiredViewModel: AuthenticatorViewModel {
    internal private(set) var fields: [InputField]
    weak var contract: BBIDInputRequiredAuthenticatorContract?

    required init(fields: [InputField], contract: BBIDInputRequiredAuthenticatorContract?) {
        self.fields = fields
        self.contract = contract
    }

    func bind(
        values: [ControlProperty<String?>],
        tap: Observable<Void>,
        cancel: Observable<Void>,
        submit: Observable<Void>
    ) -> (
        title: Driver<String>,
        subtitle: Driver<String>,
        placeholders: [Driver<String>],
        submitButtonTitle: Driver<String>,
        cancelButtonIcon: Driver<UIImage?>,
        unfocusInputs: Signal<Void>,
        errorAlert: Observable<UIAlertController>
    ) {
        submit
            .withLatestFrom(Observable.combineLatest(values.map { $0.asObservable() }))
            .subscribe(onNext: { [weak self] values in
                guard let self = self else { return }
                var dict: [String: String] = [:]
                zip(self.fields.map(\.key), values).forEach { dict[$0] = $1 ?? "" }
                self.contract?.submit(dict)
            })
            .disposed(by: disposeBag)

        cancel
            .subscribe(onNext: { [weak self] in
                self?.contract?.cancel()
            })
            .disposed(by: disposeBag)

        return (
            title: .just(title(for: fields)),
            subtitle: .just(subtitle(for: fields)),
            placeholders: self.placeholders(for: fields).map(Driver<String>.just),
            submitButtonTitle: .just(submitButtonTitle(for: fields)),
            cancelButtonIcon: .just(configuration.inputRequired.cancelButtonIcon),
            unfocusInputs: tap.asSignal(onErrorJustReturn: ()),
            errorAlert: errorAlertSubject.asObservable()
        )
    }

    func didSucceed() {
        contract?.finish()
    }

    func didFail(with error: Authentication.Error) {
        errorAlertSubject.onNext(alertController(for: error))
    }

    func title(for fields: [InputField]) -> String {
        switch fields {
        case [.email]:
            return configuration.inputRequired.strings.emailTitle()
        default:
            return configuration.inputRequired.strings.unknownTitle()
        }
    }

    func subtitle(for fields: [InputField]) -> String {
        switch fields {
        case [.email]:
            return configuration.inputRequired.strings.emailSubtitle()
        default:
            return configuration.inputRequired.strings.unknownSubtitle()
        }
    }

    func placeholders(for fields: [InputField]) -> [String] {
        return fields.map { field in
            switch field {
            case .email:
                return configuration.inputRequired.strings.emailPlaceholder()
            case .unknown(let field):
                return field
            }
        }
    }

    func submitButtonTitle(for fields: [InputField]) -> String {
        switch fields {
        case [.email]:
            return configuration.inputRequired.strings.emailSubmitButtonTitle()
        default:
            return configuration.inputRequired.strings.unknownSubmitButtonTitle()
        }
    }

    func alertController(for error: Authentication.Error) -> UIAlertController {
        let alert = UIAlertController(
            title: configuration.strings.alertTitle(error)(),
            message: configuration.strings.alertMessage(error)(),
            preferredStyle: .alert
        )

        switch error {
        case .invalidEmail:
            let retryAction = UIAlertAction(title: configuration.strings.alertRetryOption(), style: .default)
            let cancelAction = UIAlertAction(title: configuration.strings.alertCancelOption(), style: .cancel) { [weak self] _ in
                self?.contract?.cancel()
            }
            alert.addAction(retryAction)
            alert.addAction(cancelAction)
        default:
            let confirmAction = UIAlertAction(title: configuration.strings.alertConfirmOption(), style: .default)
            alert.addAction(confirmAction)
        }

        return alert
    }

    // MARK: - Private

    @LazyInjected
    private var configuration: Authentication.Configuration

    private var errorAlertSubject = PublishSubject<UIAlertController>()
    private let disposeBag = DisposeBag()
}
