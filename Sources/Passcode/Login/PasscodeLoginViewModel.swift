//
//  Created by Backbase R&D B.V. on 3/31/20.
//

import UIKit
import RxSwift
import RxCocoa
import Resolver
import BackbaseIdentity

internal final class PasscodeLoginViewModel: AuthenticatorViewModel {
    weak var contract: BBIDPasscodeAuthenticatorContract?

    var didTapTroubleshoot: ((@escaping Passcode.Router.ConfirmButtonHandler) -> Void)?
    
    required init(contract: BBIDPasscodeAuthenticatorContract?) {
        self.contract = contract
    }

    func bind(
        passcode: ControlProperty<String?>,
        cancel: Observable<Void>,
        troubleshoot: Observable<Void>
    ) -> (
        image: Driver<UIImage?>,
        title: Driver<String>,
        subtitle: Driver<String>,
        cancelButtonIcon: Driver<UIImage?>,
        troubleshootButtonTitle: Driver<String>,
        loading: Driver<Bool>,
        alert: Signal<UIAlertController>
    ) {
        passcode
            .unwrapped()
            .subscribe(onNext: { [weak self] passcode in
                self?.loadingRelay.accept(true)
                self?.contract?.passcodeDidEnter(passcode)
            })
            .disposed(by: disposeBag)

        resetFlowSubject
            .subscribe(onNext: {
                passcode.onNext(nil)
            })
            .disposed(by: disposeBag)

        cancel
            .subscribe(onNext: { [weak self] in
                self?.contract?.cancel()
            })
            .disposed(by: disposeBag)

        troubleshoot
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.didTapTroubleshoot? {
                    self?.authenticationUseCase.logOut(callback: nil)
                }
            })
            .disposed(by: disposeBag)

        return (
            image: .just(configuration.passcode.loginImage),
            title: .just(configuration.passcode.strings.loginTitle()),
            subtitle: .just(configuration.passcode.strings.loginSubtitle()),
            cancelButtonIcon: .just(configuration.passcode.cancelButtonIcon),
            troubleshootButtonTitle: .just(configuration.passcode.strings.loginTroubleshootButtonTitle()),
            loading: loadingRelay.asDriver(),
            alert: alertRelay.asSignal()
        )
    }

    func didSucceed() {
        loadingRelay.accept(false)
        contract?.finish()
    }

    func didFail(with error: Authentication.Error) {
        loadingRelay.accept(false)
        if error == .accountLocked {
            authenticationUseCase.lockAccount(callback: nil)
        } else {
            alertRelay.accept(errorAlertController(for: error))
        }
    }

    func errorAlertController(for error: Authentication.Error) -> UIAlertController {
        let alert = UIAlertController(
            title: configuration.strings.alertTitle(error)(),
            message: configuration.strings.alertMessage(error)(),
            preferredStyle: .alert
        )

        let retryTitle = configuration.strings.alertRetryOption()
        let retryAction = UIAlertAction(title: retryTitle, style: .default) { [weak self] _ in
            self?.resetFlowSubject.onNext(())
        }
        alert.addAction(retryAction)

        let cancelTitle = configuration.strings.alertCancelOption()
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { [weak self] _ in
            self?.contract?.cancel()
        }
        alert.addAction(cancelAction)

        return alert
    }

    // MARK: - Private

    @LazyInjected
    private var configuration: Authentication.Configuration

    @LazyInjected
    private var authenticationUseCase: AuthenticationUseCase

    internal var resetFlowSubject = PublishSubject<Void>()

    private var loadingRelay = BehaviorRelay<Bool>(value: false)
    private var alertRelay = PublishRelay<UIAlertController>()
    private let disposeBag = DisposeBag()
}
