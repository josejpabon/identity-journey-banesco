//
//  Created by Backbase R&D B.V. on 3/31/20.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit
import Resolver
import BackbaseIdentity
import BackbaseDesignSystem

internal final class PasscodeLoginView: UIView {
    weak var viewController: RenderingViewController?

    func bind(viewModel: PasscodeLoginViewModel) {
        passcodeInputView.becomeFirstResponder()

        let output = viewModel.bind(
            passcode: passcodeInputView.rx.text.asControlProperty(),
            cancel: cancelButton.rx.tap.asObservable(),
            troubleshoot: troubleshootButton.rx.tap.asObservable()
        )

        output.alert.emit(onNext: { [weak self]  alert in
            self?.viewController?.present(alert, animated: true)
        })
        .disposed(by: disposeBag)

        disposeBag.insert(
            output.image.drive(imageView.rx.image),
            output.title.drive(titleLabel.rx.text),
            output.subtitle.drive(subtitleLabel.rx.text),
            output.loading.drive(loadingIndicator.rx.isAnimating),
            output.loading.drive(onNext: { [weak self] loading in
                guard let self = self else { return }
                if loading {
                    self.troubleshootButton.isHidden = true
                } else {
                    self.configuration.passcode.design.troubleshootButton(self.troubleshootButton)
                }
            }),
            output.loading.map({ !$0 }).drive(cancelButton.rx.isEnabled),
            output.loading.map({ !$0 }).drive(passcodeInputView.rx.isUserInteractionEnabled),
            output.cancelButtonIcon.drive(cancelButton.rx.image()),
            output.troubleshootButtonTitle.drive(troubleshootButton.rx.title())
        )
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubviews(
            backgroundView,
            imageView,
            titleLabel,
            subtitleLabel,
            passcodeInputView,
            loadingIndicator,
            cancelButton,
            troubleshootButton
        )

        backgroundView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        let safeArea = safeAreaLayoutGuide

        imageView.snp.makeConstraints { make in
            make.height.equalTo(DesignSystem.shared.sizer.sm * 9)
            make.top.equalTo(safeArea).offset(DesignSystem.shared.spacer.sm * 6)
            make.leading.trailing.equalTo(safeArea).inset(DesignSystem.shared.spacer.xl)
        }

        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(imageView.snp.bottom).offset(DesignSystem.shared.spacer.xl)
            make.leading.trailing.equalTo(safeArea).inset(DesignSystem.shared.spacer.md)
        }

        subtitleLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(safeArea).inset(DesignSystem.shared.spacer.xl)
            make.top.equalTo(titleLabel.snp.bottom).offset(DesignSystem.shared.spacer.sm)
        }

        passcodeInputView.snp.makeConstraints { make in
            make.top.equalTo(subtitleLabel.snp.bottom).offset(DesignSystem.shared.spacer.xl)
            make.leading.trailing.equalTo(safeArea).inset(DesignSystem.shared.spacer.xl)
        }

        loadingIndicator.snp.makeConstraints { make in
            make.center.equalTo(troubleshootButton)
        }

        troubleshootButton.snp.makeConstraints { make in
            make.top.equalTo(passcodeInputView.snp.bottom).offset(DesignSystem.shared.spacer.md)
            make.centerX.equalTo(safeArea)
        }

        cancelButton.snp.makeConstraints { make in
            make.top.equalTo(safeArea).inset(DesignSystem.shared.spacer.sm)
            make.leading.equalTo(safeArea).inset(DesignSystem.shared.spacer.md)
            make.height.equalTo(DesignSystem.shared.sizer.sm*6)
        }

        applyStyles()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError()
    }

    private lazy var backgroundView = Authentication.Design.BackgroundView(frame: frame)

    lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.accessibilityIdentifier = "passcode.imageView"
        return view
    }()

    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "passcode.titleLabel"
        return label
    }()

    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "passcode.subtitleLabel"
        return label
    }()

    lazy var passcodeInputView: Authentication.Design.OTPInput = {
        let view = Authentication.Design.OTPInput(length: configuration.passcode.length)
        view.accessibilityIdentifier = "passcode.passcodeInputView"
        view.isSecureTextEntry = true
        return view
    }()

    private lazy var loadingIndicator = UIActivityIndicatorView(style: .white)

    private lazy var cancelButton: Button = {
        let button = Button()
        button.accessibilityIdentifier = "passcode.cancelButton"
        return button
    }()

    private lazy var troubleshootButton: Button = {
        let button = Button()
        button.accessibilityIdentifier = "passcode.troubleshootButton"
        return button
    }()

    private func applyStyles() {
        configuration.passcode.design.background(backgroundView)
        configuration.passcode.design.image(imageView)
        configuration.passcode.design.title(titleLabel)
        configuration.passcode.design.subtitle(subtitleLabel)
        configuration.passcode.design.input(passcodeInputView)
        configuration.passcode.design.loading(loadingIndicator)
        configuration.passcode.design.cancelButton(cancelButton)
        configuration.passcode.design.troubleshootButton(troubleshootButton)
    }

    @LazyInjected
    private var configuration: Authentication.Configuration

    private var viewModel: PasscodeLoginViewModel?
    private let disposeBag = DisposeBag()
}
