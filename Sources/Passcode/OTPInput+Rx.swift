//
//  Created by Backbase R&D B.V. on 29/4/20.
//

import UIKit
import RxSwift
import RxCocoa

extension Reactive where Base: Authentication.Design.OTPInput {
    /// Reactive wrapper for `text` property.
    var text: ControlProperty<String?> {
        return base.rx.controlProperty(
            editingEvents: .valueChanged,
            getter: { view in
                return view.text
            },
            setter: { view, text  in
                view.text = text
            }
        )
    }
}
