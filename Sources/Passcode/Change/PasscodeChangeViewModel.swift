//
//  Created by Backbase R&D B.V. on 14/4/20.
//

import UIKit
import RxSwift
import RxCocoa
import Resolver
import BackbaseIdentity

internal final class PasscodeChangeViewModel: AuthenticatorViewModel {
    weak var contract: BBIDPasscodeAuthenticatorContract?

    required init(contract: BBIDPasscodeAuthenticatorContract?) {
        self.contract = contract
    }

    func bind(passcode: ControlProperty<String?>, cancel: Observable<Void>) -> (
        cancelButtonIcon: Driver<UIImage?>,
        title: Driver<String>,
        subtitle: Driver<String>,
        loading: Driver<Bool>,
        errorAlert: Observable<UIAlertController>
    ) {
        passcode
            .unwrapped()
            .subscribe(onNext: { [weak self] pass in
                self?.processPasscode(current: pass, control: passcode)
            })
            .disposed(by: disposeBag)

        resetFlowSubject
            .subscribe(onNext: { [weak self] in
                passcode.onNext(nil)
                self?.reset()
            })
            .disposed(by: disposeBag)

        cancel
            .subscribe(onNext: { [weak self] in
                self?.contract?.cancel()
            })
            .disposed(by: disposeBag)

        return (
            cancelButtonIcon: .just(configuration.passcode.cancelButtonIcon),
            title: titleRelay.asDriver(),
            subtitle: subtitleRelay.asDriver(),
            loading: loadingRelay.asDriver(),
            errorAlert: errorAlertSubject.asObservable()
        )
    }

    func processPasscode(current: String, control: ControlProperty<String?>) {
        // all: old, new, and confirmed passcodes are entered, attempt change
        if let old = oldPasscode, let new = newPasscode {
            attemptChange(old: old, new: new, confirmed: current)
            return
        }

        // only old and new passcodes entered, move to confirmation
        if oldPasscode != nil {
            moveToConfirmation(withNewPasscode: current)
            control.onNext(nil)
            return
        }

        // only old passcode is entered, move to new passcode
        moveToNew(withOldPasscode: current)
        control.onNext(nil)
    }

    func attemptChange(old: String, new: String, confirmed: String) {
        if new == confirmed {
            loadingRelay.accept(true)
            contract?.changePasscode(old, newPasscode: new)
        } else {
            errorAlertSubject.onNext(errorAlertController(for: .passcodeMismatch))
        }
    }

    func moveToNew(withOldPasscode passcode: String) {
        oldPasscode = passcode
        titleRelay.accept(configuration.passcode.strings.changeStep2Title())
        subtitleRelay.accept(configuration.passcode.strings.changeStep2Subtitle())
    }

    func moveToConfirmation(withNewPasscode passcode: String) {
        newPasscode = passcode
        titleRelay.accept(configuration.passcode.strings.changeStep3Title())
        subtitleRelay.accept(configuration.passcode.strings.changeStep3Subtitle())
    }

    func reset() {
        oldPasscode = nil
        newPasscode = nil
        titleRelay.accept(configuration.passcode.strings.changeStep1Title())
        subtitleRelay.accept(configuration.passcode.strings.changeStep1Subtitle())
        loadingRelay.accept(false)
    }

    func errorAlertController(for error: Authentication.Error) -> UIAlertController {
        let title = configuration.strings.alertTitle(error)()
        let message = configuration.strings.alertMessage(error)()
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let retryTitle = configuration.strings.alertRetryOption()
        let retryAction = UIAlertAction(title: retryTitle, style: .default) { [weak self] _ in
            self?.resetFlowSubject.onNext(())
        }
        alert.addAction(retryAction)

        return alert
    }

    func didSucceed() {
        loadingRelay.accept(false)
        contract?.finish()
    }

    func didFail(with error: Authentication.Error) {
        loadingRelay.accept(false)
        errorAlertSubject.onNext(errorAlertController(for: error))
    }

    // MARK: - Private

    @LazyInjected
    private var configuration: Authentication.Configuration

    private var oldPasscode: String?
    private var newPasscode: String?

    internal var resetFlowSubject = PublishSubject<Void>()

    private lazy var titleRelay = BehaviorRelay<String>(value: configuration.passcode.strings.changeStep1Title())
    private lazy var subtitleRelay = BehaviorRelay<String>(value: configuration.passcode.strings.changeStep1Subtitle())
    private var loadingRelay = BehaviorRelay<Bool>(value: false)

    private var errorAlertSubject = PublishSubject<UIAlertController>()

    private let disposeBag = DisposeBag()
}
