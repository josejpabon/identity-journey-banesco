//
//  Created by Backbase R&D B.V. on 14/4/20.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit
import Resolver
import BackbaseIdentity
import BackbaseDesignSystem

internal final class PasscodeChangeView: UIView {
    weak var viewController: RenderingViewController?

    func bind(viewModel: PasscodeChangeViewModel) {
        passcodeInputView.becomeFirstResponder()

        let output = viewModel.bind(
            passcode: passcodeInputView.rx.text.asControlProperty(),
            cancel: cancelButton.rx.tap.asObservable()
        )

        output.errorAlert.subscribe(onNext: { [weak self]  alert in
            self?.viewController?.present(alert, animated: true)
        })
        .disposed(by: disposeBag)

        disposeBag.insert(
            output.cancelButtonIcon.drive(cancelButton.rx.image()),
            output.title.drive(titleLabel.rx.text),
            output.subtitle.drive(subtitleLabel.rx.text),
            output.loading.drive(loadingIndicator.rx.isAnimating),
            output.loading.map({ !$0 }).drive(cancelButton.rx.isEnabled),
            output.loading.map({ !$0 }).drive(passcodeInputView.rx.isUserInteractionEnabled)
        )
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubviews(backgroundView, titleLabel, subtitleLabel, passcodeInputView, loadingIndicator, cancelButton)

        backgroundView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        let safeArea = safeAreaLayoutGuide

        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(safeArea).inset(DesignSystem.shared.spacer.sm * 10)
            make.leading.trailing.equalTo(safeArea).inset(DesignSystem.shared.spacer.md)
        }

        subtitleLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(safeArea).inset(DesignSystem.shared.spacer.sm * 6)
            make.top.equalTo(titleLabel.snp.bottom).offset(DesignSystem.shared.spacer.sm)
        }

        passcodeInputView.snp.makeConstraints { make in
            make.top.equalTo(subtitleLabel.snp.bottom).offset(DesignSystem.shared.spacer.xl)
            make.leading.trailing.equalTo(safeArea).inset(DesignSystem.shared.spacer.xl)
        }

        loadingIndicator.snp.makeConstraints { make in
            make.top.equalTo(passcodeInputView.snp.bottom).offset(DesignSystem.shared.spacer.xl)
            make.centerX.equalTo(safeArea)
        }

        cancelButton.snp.makeConstraints { make in
            make.top.equalTo(safeArea).inset(DesignSystem.shared.spacer.sm)
            make.leading.equalTo(safeArea).inset(DesignSystem.shared.spacer.md)
            make.height.equalTo(DesignSystem.shared.sizer.sm*6)
        }

        applyStyles()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError()
    }

    private lazy var backgroundView = Authentication.Design.BackgroundView(frame: frame)

    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "passcode.titleLabel"
        return label
    }()

    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.accessibilityIdentifier = "passcode.subtitleLabel"
        return label
    }()

    lazy var passcodeInputView: Authentication.Design.OTPInput = {
        let view = Authentication.Design.OTPInput(length: configuration.passcode.length)
        view.accessibilityIdentifier = "passcode.passcodeInputView"
        view.isSecureTextEntry = true
        return view
    }()

    private lazy var loadingIndicator = UIActivityIndicatorView(style: .white)

    private func applyStyles() {
        configuration.passcode.design.background(backgroundView)
        configuration.passcode.design.title(titleLabel)
        configuration.passcode.design.subtitle(subtitleLabel)
        configuration.passcode.design.input(passcodeInputView)
        configuration.passcode.design.loading(loadingIndicator)
        configuration.passcode.design.cancelButton(cancelButton)
    }

    private lazy var cancelButton: Button = {
        let button = Button()
        button.accessibilityIdentifier = "passcode.cancelButton"
        return button
    }()

    @LazyInjected
    private var configuration: Authentication.Configuration

    private var viewModel: PasscodeRegistrationViewModel?
    private let disposeBag = DisposeBag()
}
