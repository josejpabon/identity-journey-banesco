//
//  Created by Backbase R&D B.V. on 27/08/2020.
//

import UIKit

public extension Passcode {
    struct Router {
        public typealias ConfirmButtonHandler = () -> Void
        
        public var didTapTroubleshoot: (UINavigationController) -> (@escaping ConfirmButtonHandler) -> Void
    }
}
