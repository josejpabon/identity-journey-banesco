//
//  Created by Backbase R&D B.V. on 15/5/2020.
//

import Foundation

public extension Passcode {
    /// Passcode strings.
    struct Strings {
        /// Create a new strings object with default values.
        public init() {
            // no code required.
        }

        /// authentication.passcode.registration.labels.step1Title
        public var registrationStep1Title = localized(key: "registration.labels.step1Title")

        /// authentication.passcode.registration.labels.step1Subtitle
        public var registrationStep1Subtitle = localized(key: "registration.labels.step1Subtitle")

        /// authentication.passcode.registration.labels.step2Title
        public var registrationStep2Title = localized(key: "registration.labels.step2Title")

        /// authentication.passcode.registration.labels.step2Subtitle
        public var registrationStep2Subtitle = localized(key: "registration.labels.step2Subtitle")

        /// authentication.passcode.registration.buttons.reset
        public var registrationResetButtonTitle = localized(key: "registration.buttons.reset")

        /// authentication.passcode.change.labels.step1Title
        public var changeStep1Title = localized(key: "change.labels.step1Title")

        /// authentication.passcode.change.labels.step1Subtitle
        public var changeStep1Subtitle = localized(key: "change.labels.step1Subtitle")

        /// authentication.passcode.change.labels.step2Title
        public var changeStep2Title = localized(key: "change.labels.step2Title")

        /// authentication.passcode.change.labels.step2Subtitle
        public var changeStep2Subtitle = localized(key: "change.labels.step2Subtitle")

        /// authentication.passcode.change.labels.step3Title
        public var changeStep3Title = localized(key: "change.labels.step3Title")

        /// authentication.passcode.change.labels.step3Subtitle
        public var changeStep3Subtitle = localized(key: "change.labels.step3Subtitle")

        /// authentication.passcode.login.labels.title
        public var loginTitle = localized(key: "login.labels.title")

        /// authentication.passcode.login.labels.subtitle
        public var loginSubtitle = localized(key: "login.labels.subtitle")

        /// authentication.passcode.login.buttons.troubleshoot
        public var loginTroubleshootButtonTitle = localized(key: "login.buttons.troubleshoot")

        /// authentication.passcode.login.alerts.troubleshoot.title
        public var loginTroubleshootAlertTitle = localized(key: "login.alerts.troubleshoot.title")

        /// authentication.passcode.login.alerts.troubleshoot.message
        public var loginTroubleshootAlertMessage = localized(key: "login.alerts.troubleshoot.message")

        /// authentication.passcode.login.alerts.troubleshoot.options.confirm
        public var loginTroubleshootAlertConfirmOption = localized(key: "login.alerts.troubleshoot.options.confirm")

        private static func localized(key: String) -> LocalizedString {
            let prefix = "authentication.passcode."
            return LocalizedString(key: prefix + key, in: .authentication)
        }
    }
}
