//
//  Created by Backbase R&D B.V. on 3/27/20.
//

import UIKit
import Backbase
import BackbaseIdentity

/// Authentication namespace used for grouping passcode level configuration.
public struct Passcode {
    /// Build a passcode view controller from a `Renderable`.
    /// - Parameter item: `Renderable`.
    /// - Returns: view controller building closure.
    public static func build(item: Renderable) -> (UINavigationController) -> UIViewController {
        return { navigationController in
            navigationController.viewControllers = [RenderingViewController(withRenderable: item)]
            return navigationController
        }
    }
    
    internal static var authenticator: BBIDPasscodeAuthenticator = {
         do {
             return try BBIDPasscodeAuthenticator(viewClass: PasscodeContainer.self)
         } catch {
             fatalError("Failed to create BBIDPasscodeAuthenticator from PasscodeContainer: \(error.localizedDescription)")
         }
    }()
}
