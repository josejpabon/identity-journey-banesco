//
//  Created by Backbase R&D B.V. on 29/4/20.
//

import UIKit

public extension Passcode {
    /// Passcode design-related configurations.
    struct Design {
        internal init(journeyDesign: Authentication.Design) {
            self.journeyDesign = journeyDesign
        }

        /// Applied to all `BackgroundView` views.
        public lazy var background = journeyDesign.styles.background

        /// Applied to all image views in passcode screens.
        public lazy var image = journeyDesign.styles.image

        /// Applied to all title labels in passcode screens.
        public lazy var title = journeyDesign.styles.title

        /// Applied to all subtitle labels in biometric screens.
        public lazy var subtitle = journeyDesign.styles.subtitle

        /// Applied to all OTP inputs in passcode screens.
        public lazy var input = journeyDesign.styles.otpInput

        /// Applied to loading indicator in passcode screens.
        public lazy var loading = journeyDesign.styles.loading

        /// Applied to reset button in passcode screens.
        public lazy var resetButton = journeyDesign.styles.button(.plain)

        /// Applied to cancel button in passcode screens.
        public lazy var cancelButton = journeyDesign.styles.button(.plain)

        /// Applied to troubleshoot button in passcode screens.
        public lazy var troubleshootButton = journeyDesign.styles.button(.plain)

        // MARK: - Private

        private let journeyDesign: Authentication.Design
    }
}
