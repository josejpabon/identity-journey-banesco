//
//  Created by Backbase R&D B.V. on 3/31/20.
//

import UIKit
import RxSwift
import RxCocoa
import Resolver
import BackbaseIdentity

internal final class PasscodeRegistrationViewModel: AuthenticatorViewModel {
    weak var contract: BBIDPasscodeAuthenticatorContract?

    required init(contract: BBIDPasscodeAuthenticatorContract?) {
        self.contract = contract
    }

    func bind(
        passcode: ControlProperty<String?>,
        cancel: Observable<Void>,
        resetFlow: Observable<Void>
    ) -> (
        title: Driver<String>,
        subtitle: Driver<String>,
        cancelButtonIcon: Driver<UIImage?>,
        resetButtonTitle: Driver<String>,
        resetButtonHidden: Driver<Bool>,
        loading: Driver<Bool>,
        errorAlert: Observable<UIAlertController>,
        dismiss: Signal<Void>
    ) {
        passcode
            .unwrapped()
            .subscribe(onNext: { [weak self] pass in
                if let initial = self?.initialPasscode {
                    self?.attemptRegistration(initial: initial, confirmed: pass)
                } else {
                    passcode.onNext(nil)
                    self?.moveToConfirmation(withInitialPasscode: pass)
                }
            })
            .disposed(by: disposeBag)

        passcodeSubject
            .subscribe(onNext: { [weak self] confirmedPasscode in
                self?.contract?.passcodeDidEnter(confirmedPasscode)
            })
            .disposed(by: disposeBag)

        retryConfirmationSubject
            .subscribe(onNext: {
                passcode.onNext(nil)
            })
            .disposed(by: disposeBag)

        resetFlow
            .subscribe(onNext: { [weak self] in
                passcode.onNext(nil)
                self?.reset()
            })
            .disposed(by: disposeBag)

        cancel
            .subscribe(onNext: { [weak self] in
                self?.contract?.cancel()
            })
            .disposed(by: disposeBag)

        return (
            title: titleRelay.asDriver(),
            subtitle: subtitleRelay.asDriver(),
            cancelButtonIcon: .just(configuration.passcode.cancelButtonIcon),
            resetButtonTitle: .just(configuration.passcode.strings.registrationResetButtonTitle()),
            resetButtonHidden: resetButtonHiddenRelay.asDriver(),
            loading: loadingRelay.asDriver(),
            errorAlert: errorAlertSubject.asObservable(),
            dismiss: dismissRelay.asSignal()
        )
    }

    func attemptRegistration(initial: String, confirmed: String) {
        if confirmed == initial {
            passcodeSubject.onNext(confirmed)
            loadingRelay.accept(true)
        } else {
            errorAlertSubject.onNext(errorAlertController(for: .passcodeMismatch))
        }
    }

    func moveToConfirmation(withInitialPasscode passcode: String) {
        initialPasscode = passcode
        titleRelay.accept(configuration.passcode.strings.registrationStep2Title())
        subtitleRelay.accept(step2Subtitle)
        resetButtonHiddenRelay.accept(false)
    }

    func reset() {
        initialPasscode = nil
        titleRelay.accept(configuration.passcode.strings.registrationStep1Title())
        subtitleRelay.accept(step1Subtitle)
        resetButtonHiddenRelay.accept(true)
        loadingRelay.accept(false)
    }

    func errorAlertController(for error: Authentication.Error) -> UIAlertController {
        let title = configuration.strings.alertTitle(error)()
        let message = configuration.strings.alertMessage(error)()
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let retryTitle = configuration.strings.alertRetryOption()
        let retryAction = UIAlertAction(title: retryTitle, style: .default) { [weak self] _ in
            switch error {
            case .incompleteEnrollment:
                self?.authenticationUseCase.logOut { _ in
                    self?.dismissRelay.accept(())
                }
            default:
                self?.retryConfirmationSubject.onNext(())
            }
        }
        alert.addAction(retryAction)

        return alert
    }

    func didSucceed() {
        loadingRelay.accept(false)
        contract?.finish()
    }

    func didFail(with error: Authentication.Error) {
        loadingRelay.accept(false)
        errorAlertSubject.onNext(errorAlertController(for: error))
    }

    var step1Subtitle: String {
        let localized = configuration.passcode.strings.registrationStep1Subtitle()
        return String(format: localized, configuration.passcode.length)
    }

    var step2Subtitle: String {
        let localized = configuration.passcode.strings.registrationStep2Subtitle()
        return String(format: localized, configuration.passcode.length)
    }

    // MARK: - Private

    @LazyInjected
    private var configuration: Authentication.Configuration

    @LazyInjected
    private var authenticationUseCase: AuthenticationUseCase

    private var initialPasscode: String?

    private lazy var titleRelay = BehaviorRelay<String>(value: configuration.passcode.strings.registrationStep1Title())
    private lazy var subtitleRelay = BehaviorRelay<String>(value: step1Subtitle)

    internal var retryConfirmationSubject = PublishSubject<Void>()
    internal var passcodeSubject = PublishSubject<String>()

    private var resetButtonHiddenRelay = BehaviorRelay<Bool>(value: true)
    private var loadingRelay = BehaviorRelay<Bool>(value: false)
    private var errorAlertSubject = PublishSubject<UIAlertController>()
    private var dismissRelay = PublishRelay<Void>()
    private let disposeBag = DisposeBag()
}
