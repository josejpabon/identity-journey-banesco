//
//  Created by Backbase R&D B.V. on 3/27/20.
//

import UIKit
import SnapKit
import BackbaseIdentity
import Resolver

internal final class PasscodeContainer: UIView, RenderableView, BBIDPasscodeAuthenticatorView {
    func promptForPasscodeSelection() {
        let viewModel = PasscodeRegistrationViewModel(contract: contract)
        activeViewModel = viewModel

        let view = PasscodeRegistrationView()
        view.bind(viewModel: viewModel)
        view.viewController = viewController
        visibleView = view
    }

    func promptForPasscode() {
        let viewModel = PasscodeLoginViewModel(contract: contract)
        activeViewModel = viewModel

        if let navigationController = viewController?.navigationController {
            viewModel.didTapTroubleshoot = configuration.passcode.router.didTapTroubleshoot(navigationController)
        }
        
        let view = PasscodeLoginView()
        view.bind(viewModel: viewModel)
        view.viewController = viewController
        visibleView = view
    }

    func promptForPasscodeChange() {
        let viewModel = PasscodeChangeViewModel(contract: contract)
        activeViewModel = viewModel

        let view = PasscodeChangeView()
        view.bind(viewModel: viewModel)
        view.viewController = viewController
        visibleView = view
    }

    func authenticatorDidSucceed() {
        activeViewModel?.didSucceed()
    }

    func authenticatorDidFail(with error: Error) {
        activeViewModel?.didFail(with: .init(passcode: error))
    }

    // MARK: - Private

    private weak var contract: BBIDPasscodeAuthenticatorContract?
    private var activeViewModel: AuthenticatorViewModel?

    // MARK: - Rendering

    static func initialize(with contractImpl: NativeContract, container: UIView) -> (UIView & NativeView)? {
        let view = Self()
        view.contract = contractImpl as? BBIDPasscodeAuthenticatorContract
        container.addSubview(view)
        view.snp.makeConstraints { $0.edges.equalToSuperview() }
        return view
    }

    var visibleView: UIView? {
        willSet {
            visibleView?.removeFromSuperview()
        }
        didSet {
            if let view = visibleView {
                addSubview(view)
                view.snp.makeConstraints { $0.edges.equalToSuperview() }
            }
        }
    }
    
    @LazyInjected
    private var configuration: Authentication.Configuration
}
