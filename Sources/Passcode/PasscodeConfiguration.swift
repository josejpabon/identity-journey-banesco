//
//  Created by Backbase R&D B.V. on 3/27/20.
//

import UIKit
import Resolver

public extension Passcode {
    /// Authentication journey passcode configurations.
    struct Configuration {
        internal init(journeyDesign: Authentication.Design) {
            self.journeyDesign = journeyDesign
        }

        /// Passcode length (default: 5)
        /// - Note: This is just a UI configuration. Passcode length should be configured from the backend as well.
        public var length = 5

        /// Brand image (default: `UIImage.named("PasscodeLogo", in: .authentication, .design)`)
        public var loginImage = UIImage.named("PasscodeLogo", in: .authentication, .design)

        /// Cancel button icon (default: `UIImage.named("ic_close", in: .authentication, .design)`)
        public var cancelButtonIcon = UIImage.named("ic_close", in: .authentication, .design)

        /// Passcode design configurations.
        public lazy var design = Design(journeyDesign: journeyDesign)

        /// Passcode strings.
        public var strings = Strings()
        
        /// Router configuration for Passcode screen
        public var router = Router(didTapTroubleshoot: { navigationController in
            return { confirm in
                var configuration: Authentication.Configuration = Resolver.resolve()
                
                let alert = UIAlertController(
                    title: configuration.passcode.strings.loginTroubleshootAlertTitle(),
                    message: configuration.passcode.strings.loginTroubleshootAlertMessage(),
                    preferredStyle: .alert
                )
                
                let confirmTitle = configuration.passcode.strings.loginTroubleshootAlertConfirmOption()
                let confirmAction = UIAlertAction(title: confirmTitle, style: .default) { _ in confirm() }
                alert.addAction(confirmAction)
                
                let cancelTitle = configuration.strings.alertCancelOption()
                let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
                alert.addAction(cancelAction)
                
                navigationController.present(alert, animated: true, completion: nil)
            }
        })

        // MARK: - Private

        private let journeyDesign: Authentication.Design
    }
}
