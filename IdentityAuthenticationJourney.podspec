Pod::Spec.new do |s|
  s.name = 'IdentityAuthenticationJourney'
  s.version = '1.0.0'
  s.summary = 'IdentityAuthenticationJourney'
  s.description = s.summary
  s.license = 'Backbase License'
  s.homepage = 'http://www.backbase.com/home'
  s.author = 'Backbase B.V.'

  s.platform = :ios
  s.ios.deployment_target = '12.0'

  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.source = { git: 'https://josejpabon@bitbucket.org/josejpabon/identity-authentication-journey-ios-master-d5307e570f8.git' }
  s.source_files = 'Sources/**/*'
  s.exclude_files = 'Sources/Info.plist'

  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.vendored_frameworks = 'IdentityAuthenticationJourney.xcframework'

  # ――― Assets ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.ios.resource_bundle = { 
    "IdentityAuthenticationJourneyAssets": [
      "Assets/*.xcassets",
      "Assets/*.lproj/*.strings"
    ]
  }

  # ――― Dependencies ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.dependency 'SnapKit', '~> 5'
  s.dependency 'BackbaseDesignSystem', '~> 1.0'
  s.dependency 'BackbaseIdentity', '~> 1.4'
  s.dependency 'Resolver', '~> 1'
  s.dependency 'RxCocoa', '~> 5'
  s.dependency 'RxSwift', '~> 5'
end
