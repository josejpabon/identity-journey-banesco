/*
  Created by Backbase R&D B.V. on 14/5/2020.
*/

// MARK: - Error Alerts
"authentication.alerts.notConnected.title" = "No internet connection";
"authentication.alerts.notConnected.message" = "Please check your internet connection and try again.";

"authentication.alerts.invalidCredentials.title" = "Incorrect username or password";
"authentication.alerts.invalidCredentials.message" = "Please try again.";

"authentication.alerts.invalidPasscode.title" = "Incorrect passcode";
"authentication.alerts.invalidPasscode.message" = "You entered an invalid passcode, Please try again.";

"authentication.alerts.sessionExpired.title" = "Session expired";
"authentication.alerts.sessionExpired.message" = "Please log in again.";

"authentication.alerts.accountLocked.title" = "You’re temporarily blocked";
"authentication.alerts.accountLocked.message" = "It looks like you reached the maximum attemtps and for security reasons you’ve been temporarily blocked. Please try again.";

"authentication.alerts.cancelledByUser.title" = "";
"authentication.alerts.cancelledByUser.message" = "";

"authentication.alerts.incompleteEnrollment.title" = "Setup incomplete";
"authentication.alerts.incompleteEnrollment.message" = "Something went wrong during registration. Please try again.";

"authentication.alerts.passcodeMismatch.title" = "Passcodes do not match";
"authentication.alerts.passcodeMismatch.message" = "Please try again.";

"authentication.alerts.biometricUsageDenied.title" = "Biometrics access required.";
"authentication.alerts.biometricUsageDenied.message" = "Your privacy settings are preventing us from accessing your biometrics. Fix this from Settings and try again.";

"authentication.alerts.biometricLockout.title" = "Biometric sensor locked out";
"authentication.alerts.biometricLockout.message" = "Biometric sensor was locked out due to several wrong attempts. Lock the device, unlock it, and try again.";

"authentication.alerts.invalidEmail.title" = "Invalid e-mail";
"authentication.alerts.invalidEmail.message" = "Please enter a valid address and try again.";

"authentication.alerts.unknown.title" = "Error";
"authentication.alerts.unknown.message" = "Something went wrong.";

"authentication.alerts.options.confirm" = "OK";
"authentication.alerts.options.retry" = "Retry";
"authentication.alerts.options.cancel" = "Cancel";
"authentication.alerts.options.settings" = "Settings";

// MARK: - Login
"authentication.login.labels.title" = "Welcome back";

"authentication.login.labels.subtitle.faceID" = "Log in with Face ID";
"authentication.login.labels.subtitle.touchID" = "Log in with Touch ID";
"authentication.login.labels.subtitle.lockedOut" = "Enable Biometrics";
"authentication.login.labels.subtitle.passcode" = "Log in with Passcode";

"authentication.login.buttons.biometric.faceID" = "Log in with Face ID";
"authentication.login.buttons.biometric.touchID" = "Log in with Touch ID";
"authentication.login.buttons.biometric.lockedOut" = "Enable Biometrics";
"authentication.login.buttons.passcode" = "Log in with Passcode";

// MARK: - Register
"authentication.register.labels.title" = "Let’s get started";
"authentication.register.labels.subtitle" = "Enter your personal banking credentials to get started";

"authentication.register.inputs.username.placeholder" = "Username";
"authentication.register.inputs.username.emptyError" = "*Username is required";

"authentication.register.inputs.password.placeholder" = "Password";
"authentication.register.inputs.password.emptyError" = "*Password is required";

"authentication.register.buttons.forgotUsername" = "Forgot username?";
"authentication.register.buttons.login" = "Log in";

// MARK: - Passcode
"authentication.passcode.registration.labels.step1Title" = "Create passcode";
"authentication.passcode.registration.labels.step1Subtitle" = "Create a %d-digit code to use for logging in to your banking app";
"authentication.passcode.registration.labels.step2Title" = "Confirm passcode";
"authentication.passcode.registration.labels.step2Subtitle" = "Confirm your newly created %d-digit code";
"authentication.passcode.registration.buttons.reset" = "Reset";

"authentication.passcode.change.labels.step1Title" = "Change passcode";
"authentication.passcode.change.labels.step1Subtitle" = "Please enter your current passcode";

"authentication.passcode.change.labels.step2Title" = "New passcode";
"authentication.passcode.change.labels.step2Subtitle" = "Please enter your new passcode";

"authentication.passcode.change.labels.step3Title" = "Confirm new passcode";
"authentication.passcode.change.labels.step3Subtitle" = "Please re-enter your new passcode for verification";

"authentication.passcode.login.labels.title" = "Welcome back";
"authentication.passcode.login.labels.subtitle" = "Login with passcode";
"authentication.passcode.login.buttons.troubleshoot" = "Forgot passcode?";

"authentication.passcode.login.alerts.troubleshoot.title" = "Forgot passcode?";
"authentication.passcode.login.alerts.troubleshoot.message" = "This action will reset your application. You will be able to define a new passcode. Would you like to continue?";
"authentication.passcode.login.alerts.troubleshoot.options.confirm" = "Confirm";

// MARK: - Biometrics
"authentication.biometrics.registration.labels.title.faceID" = "Enable Face ID";
"authentication.biometrics.registration.labels.title.touchID" = "Enable Touch ID";

"authentication.biometrics.registration.labels.subtitle.faceID" = "Log in using Face ID instead of your password for easy account access";
"authentication.biometrics.registration.labels.subtitle.touchID" = "Log in using Touch ID instead of your password for easy account access";

"authentication.biometrics.registration.buttons.prompt.faceID" = "Enable Face ID";
"authentication.biometrics.registration.buttons.prompt.touchID" = "Enable Touch ID";

"authentication.biometrics.registration.buttons.cancel" = "Maybe later";

// MARK: - Input Required
"authentication.inputRequired.labels.emailTitle" = "Forgot username";
"authentication.inputRequired.labels.emailSubtitle" = "Please enter your e-mail address to receive instructions";
"authentication.inputRequired.inputs.email.placeholder" = "e-mail address";

"authentication.inputRequired.labels.unknownTitle" = "Input required";
"authentication.inputRequired.labels.unknownSubtitle" = "Please provide the required input";

"authentication.inputRequired.buttons.email.submit" = "Confirm e-mail address";
"authentication.inputRequired.buttons.unknown.submit" = "Submit";

// MARK: - Email Sent
"authentication.emailSent.labels.title" = "On its way";
"authentication.emailSent.labels.subtitle" = "If there is an account associated with this e-mail address, you will shortly receive an e-mail with your username";
"authentication.emailSent.buttons.dismiss" = "Done";

// MARK: - Setup Complete
"authentication.setupComplete.labels.title" = "Well done!";
"authentication.setupComplete.labels.subtitle" = "Your device is registered successfully and now you can use the app";
"authentication.setupComplete.buttons.dismiss" = "Let's get started!";
